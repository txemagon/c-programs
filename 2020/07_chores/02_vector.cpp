#include <stdio.h>
#include <stdlib.h>
/* Leer un vector de la forma:
    (1, 2, 3)
    [1, 2, 3]
    */

void
leer_vector (double *var1, double *var2, double *var3) {

    printf ("Vector: ");
    scanf (" %*1[[()] %lf , %lf , %lf %*1[])]", var1, var2, var3);
}



void
imprimir_vector (double x, double y, double z) {
    printf (" [ %.2lf, %.2lf, %.2lf ]\n", x, y, z);
}



int
main (int argc, char *argv[])
{
    double x, y, z;

    leer_vector (&x, &y, &z);
    imprimir_vector (x, y, z);


    return EXIT_SUCCESS;
}
