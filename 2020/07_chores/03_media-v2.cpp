#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>



void
error ()
{
    fprintf (stderr, "Has metido un número mal.\n");
    exit (EXIT_FAILURE);
}

void
ver_media (double media) {
    printf ("===============\n"
            "  Media: %.2lf\n"
            "===============\n"
            , media);
    printf ("\n");
}

int
main (int argc, char *argv[])
{
    unsigned veces = 10;
    double entrada,
           suma = 0;

    for (unsigned i=0; i<veces; i++){
        printf ("Número %u: ", i+1);
        __fpurge (stdin);
        if (scanf (" %lf", &entrada) == 0)
            error ();
        suma += entrada;
    }


    ver_media ( suma / veces );

    return EXIT_SUCCESS;
}
