#include <stdio.h>
#include <stdlib.h>

#define N 0x20

int
main (int argc, char *argv[])
{
    char palabra[N];

    /*
     * Conjunto de selección.
     */

    printf ("Palabra: ");
    scanf (" %[abcl]", palabra);

    printf ("%s\n", palabra);

    /*
     * Conjunto de selección limitado a 4 letras.
     */

    printf ("Palabra de 4 letras: ");
    scanf (" %4[abcl]", palabra);

    printf ("%s\n", palabra);

    /*
     * Conjunto de selección con rango.
     */

    printf ("Número hexadecimal: ");
    scanf (" %[0-9a-fA-F]", palabra);


    printf ("%s\n", palabra);



    /*
     * Conjunto de selección inverso.
     */

    printf ("Palabra: ");
    scanf (" %[!0-9]", palabra);

    printf ("%s\n", palabra);


    /*
     * Línea entera.
     */

    printf ("Palabra: ");
    scanf (" %[!\n]", palabra);

    printf ("%s\n", palabra);


    /* Sacar 1 guión o una barra sin asignar */
    scanf (" %*1[-/]");
    /* Sacar 1 guión o una barra sin asignar */

    unsigned dia, anio;
    char mes[20];
    printf ("firma: ");
    scanf (" Madrid, a %u de %s de %u", &dia, mes, &anio);


    return EXIT_SUCCESS;
}
