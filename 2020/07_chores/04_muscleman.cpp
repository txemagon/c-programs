#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
    unsigned l;

    printf ("Lado: ");
    scanf (" %u", &l);

    for (unsigned fila=0; fila<l; fila++) {
        for (unsigned col=0; col<l; col++)
            printf ("☀");
        printf ("\n");
    }

    return EXIT_SUCCESS;
}
