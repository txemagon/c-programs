
// Comentario de una línea
/* Comentario de múltiples líneas */

int a;           // Variable global.
                 // Declarada fuera de todas las funciones.
                 // Se puede usar desde esta línea en adelante.
                 // No se deben usar de manera habitual.
                 // VG por defecto = 0

void             // No devuelve nada: void = vacío
haz_algo ()      // Usamos la notación underscore
                 // En camelcase: hazAlgo (no usar)
{
    return;      // Cuando se ejecuta vuelve al lugar
                 // desde donde le han llamado.
                 // Puede devolver un valor o no.
                 // Es innecesario en las funciones void.
                 // Cuando una f void llega el final
                 // ejecuta un return por sí sola.
}

int              // main devuelve un entero
main ()          // Declarar la función main
{
    int c;       // Variable local.
                 // Se definen dentro de {}
                 // Sólo se conocen en la función.
                 // Al terminar la función su valor desaparece.
                 // Por defecto ? (rubbish)


    2 + a;       // Las instrucciones terminan en ;
                 // Esto es una expresión.
                 // Todo tiene un Valor de Retorno, pero...
                 // ¿Qué estamos haciendo con le VR?

    return 0;    // Devuelve un valor a la función que llamó.
                 // main => bash
                 // 0    => todo ok
                 // != 0 => wrong
}
