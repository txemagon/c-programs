#ifndef __ANSI_H__
#define __ANSI_H__



/* ANSI CONSTANTS */
#define ESC        "\x1B"

#define CLEAR_SCR  "2J"
#define CURSOR_ON  "?25h"
#define CURSOR_OFF "?25l"



/* ANSI FUNCTIONS */

// String return
#define ANS(fn)  ESC fn

// Printing
#define ANSI(cad)                     \
    printf ( "\x1B[" cad );           \
    fflush (stdout);

#define ANS_MV(x,y)                   \
    printf ("\x1B[%i;%if",            \
            y + 1, x + 1);            \
    fflush (stdout);

#endif //__ANSI_H__
