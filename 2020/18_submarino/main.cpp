#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "ansi.h"
#include "window.h"

/*
 *
 * Struct (empaquetar variables)
 * Múltiples ficheros
 * Makefiles
 * Ficheros
 *
 * */


struct TVec {
    int x;
    int y;
};

// struct TMovil {
//     int xb;
//     int yb;  // = 12; Imposible inicializar no static const
//     int vxb;
//     int vyb; //  = 0;  No confundir la inicialización de la estructura
//              //         con la de la variable.
// };


struct TMovil {
    struct TVec pos;
    struct TVec vel;
};



/* GAME LOGIC */
int
finished (){
    return 0;
}

void
data_init (struct TMovil *vessel)
{
    vessel->pos.x = 1;
    vessel->pos.y = random () % MX_W;
    vessel->vel.x = random () % 2 + 1;
    vessel->vel.y = 0;
}

void
update_ph (struct TMovil *vessel)
{
        vessel->pos.x += vessel->vel.x;

        if (vessel->pos.x > MX_W - 2)
            data_init (vessel);
}

/* MAIN LOOP */
int
main (int argc, char *argv[])
{
    struct TMovil canoa = { {1, 12}, {1, 0} };

    srand (time (NULL));
    data_init (&canoa);

    ANSI (CURSOR_OFF);
    title ();
    sleep (2);
    while (!finished ()) {
        clear_scr ();
        frame (MX_W, MX_H);
        update_ph (&canoa);
        print ( canoa.pos.x, canoa.pos.y, "🚣" );
        usleep ( 100000 );
    }

    ANSI (CURSOR_ON);

    return EXIT_SUCCESS;
}
