#include "ansi.h"
#include "window.h"

#include <stdio.h>

void
clear_scr () {
    ANSI (CLEAR_SCR);
}


void
print ( int x, int y, const char* img )
{
    x = x % (MX_W - 1) + XF + 1;  // 1: Frame border
    y = y % (MX_H - 1) + YF + 1;

    ANS_MV (x,y);
    printf ("%s", img);
    fflush (stdout);
}

void
frame (int w, int h) {


    ANS_MV (XF, YF);

    /* First row */
    printf ("╔");
    for (int i=0; i<w; i++)
        printf ("═");
    printf ("╗\n");

    /* Intermediate rows */
    for (int r=0; r<h; r++) {
        ANS_MV (XF, YF+r+1)
        printf ("║");
        ANS_MV (XF+MX_W+1, YF+r+1)
        printf ("║\n");
    }

    /* Last row */

    ANS_MV (XF, YF+MX_H+1)
    printf ("╚");
    for (int i=0; i<w; i++)
        printf ("═");
    printf ("╝\n");

}

void title () {
    FILE *pf;
    pf = fopen ("artwork/title.dat", "r+");

    while ( !feof (pf) )
        putchar (fgetc (pf));

    fclose (pf);
}
