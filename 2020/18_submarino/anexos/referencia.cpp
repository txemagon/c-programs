#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct TEmpleado {
    char *nombre;
    double sueldo;
};



void
muestra (struct TEmpleado e)
{
    printf ("%s [%.2lf]\n", e.nombre, e.sueldo);
}


struct TEmpleado
rellena ()
{
    struct TEmpleado e;

    printf ("Nombre: ");
    scanf  (" %ms", &e.nombre);
    printf ("Sueldo: ");
    scanf  (" %lf", &e.sueldo);

    return e;
}

// Orig se pasa por referencia por velocidad pero nos
// obligan los buenos modales a poner const.
void
copia (struct TEmpleado *dest, const struct TEmpleado *orig)
{
    (*dest).sueldo = orig->sueldo;
    dest->nombre = (char *) malloc (strlen (orig->nombre) + 1);
    strcpy (dest->nombre, orig->nombre);
}

int
main (int argc, char *argv[])
{

    struct TEmpleado e0, e1;

    e0 = rellena ();

    copia (&e1, &e0);

    muestra (e0);
    muestra (e1);

    strcpy (e1.nombre, "Manolo");
    e1.sueldo = 3000;

    muestra (e0);
    muestra (e1);



    free (e0.nombre);
    free (e1.nombre);

    return EXIT_SUCCESS;
}
