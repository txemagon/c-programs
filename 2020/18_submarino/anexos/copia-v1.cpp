#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct TEmpleado {
    char *nombre;
    double sueldo;
};

int
main (int argc, char *argv[])
{

    struct TEmpleado e0, e1;

    printf ("Nombre: ");
    scanf  (" %ms", &e0.nombre);
    printf ("Sueldo: ");
    scanf  (" %lf", &e0.sueldo);

    // Wrong
    // e1 = e0;

    e1.sueldo = e0.sueldo;
    e1.nombre = (char *) malloc (strlen (e0.nombre) + 1);
    strcpy (e1.nombre, e0.nombre);

    printf ("%s [%.2lf]\n", e0.nombre, e0.sueldo);
    printf ("%s [%.2lf]\n", e1.nombre, e1.sueldo);

    strcpy (e1.nombre, "Manolo");
    e1.sueldo = 3000;

    printf ("%s [%.2lf]\n", e0.nombre, e0.sueldo);
    printf ("%s [%.2lf]\n", e1.nombre, e1.sueldo);


    free (e0.nombre);
    free (e1.nombre);

    return EXIT_SUCCESS;
}
