#include <stdio.h>
#include <stdlib.h>

#define N     4
#define ROW   5
#define COL   4

int main () {

    int sueldo0,
        sueldo1,
        sueldo2,
        sueldo3;


    /* La inicialización de listas sólo se puede
     * hacer en la declaración. */

    /* N es opcional si hay inicialización */
    int sueldo[N] = { 900, 1900, 2999, 1457};

    int *sueldo_max;    // El tipo de datos sería int *

    /* ROW es opcional si hay inicialación */
    int tablero[ROW][COL] = {
        {1, 0, 2, 4},   // Valores de la fila 0
        {1, 0, 2, 4},   // Valores de la fila 1
        {1, 0, 2, 4},   // Valores de la fila 2
        {1, 0, 2, 4},   // Valores de la fila 3
        {1, 0, 2, 4}    // Valores de la fila 4
    };

    /* Una vez declarado, sólo se puede asignar elemento a elemento. */
    sueldo[0] = sueldo[1] = sueldo[2] = sueldo[3] = 1400;
    tablero[0][3] = 5;

    sueldo == &sueldo[0];     // => true

    sueldo_max = &sueldo[1];  // Guardo la dirección de sueldo[1]

    printf ("%lu\n", sizeof (sueldo));      // => 16 = 4 ints * 4 bytes/int
    printf ("%lu\n", sizeof (sueldo_max));  // =>  8 => Todas dir son 8 bytes.

    return EXIT_SUCCESS;
}
