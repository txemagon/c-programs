#ifndef __WINDOW_H__
#define __WINDOW_H__

/* WINDOWS DEFINITIONS */
#define XF     12
#define YF      2
#define MX_W   40
#define MX_H   15


#ifdef __cplusplus
extern "C" {
#endif
    void clear_scr ();
    void frame (int w, int h);
    void print (int x, int y, const char *img);
    void title ();

#ifdef __cplusplus
}
#endif

#endif //__WINDOW_H__

