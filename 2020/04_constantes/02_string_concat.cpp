#include <stdio.h>
#include <stdlib.h>

#define NUM     4

// Stringify
#define ELVALOR(x)  printf ("El valor de " #x " es: %i\n", x);

// String concatenation
#define NUEVAVAR(x) char variable_ ## x[] = #x;

int
main (int argc, char *argv[])
{
    int a = NUM;
    // Objetivo:
    // char variable_a[] = "a";

    NUEVAVAR(a);

    ELVALOR(a);
    ELVALOR(NUM);

    printf ("%s\n", variable_a);

    return EXIT_SUCCESS;
}
