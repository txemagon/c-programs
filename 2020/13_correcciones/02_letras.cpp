#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
    unsigned l = 4;
    char caracter = 'a';

    for (unsigned car=0; car<l; car++)
        for (unsigned letra=0; letra<l; letra++)
            printf ("%c", caracter + car);

    return EXIT_SUCCESS;
}
