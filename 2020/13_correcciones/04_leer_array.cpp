#include <stdio.h>
#include <stdlib.h>

#define N 10

int
main (int argc, char *argv[])
{
    int numero[N];

    for (int i=0; i<N; i++) {
        printf ("Número %i: ", i);
        scanf (" %i", &numero[i]);
    }

    for (int i=0; i<N; i++)
        printf ("Número %i: %i\n", i, numero[i]);


    return EXIT_SUCCESS;
}
