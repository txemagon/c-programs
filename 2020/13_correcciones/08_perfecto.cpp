#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
    unsigned suma,
             candidato;


    for (candidato=1; candidato<65000; candidato++) {
        suma = 0;
        for (unsigned prueba=candidato/2; prueba>0; prueba--)
            if (candidato % prueba == 0)
                suma += prueba;

        printf ("%u\t%u\n", candidato, suma);
    }

    return EXIT_SUCCESS;
}
