#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main (int argc, char *argv[])
{

    char *palabra;

    printf ("Nombre: ");
    scanf ("%ms", &palabra);

    printf ("Tu nombre: %s\n", palabra);

    free (palabra);

    // Ergo es mejor usar scanf con la %m

    return EXIT_SUCCESS;
}
