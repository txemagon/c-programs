#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>

#define MAX        0x10   /* Máximo de letras en una palabra */
#define MAX_INTENT   10U  /* Número máximo de errores */

#ifndef NDBG
#define DEBUG(...)                           \
    fprintf (stderr, "\x1B[32m");            \
    fprintf (stderr, "%s\n", __VA_ARGS__);   \
    fprintf (stderr, "\x1B[0m");
#else
#define DEBUG(...)
#endif

const char *palabra[] = {
    "pergamino",
    "maceta",
    "televisor",
    "valora",
    "telefono",
    "bicicleta",
    "mueble",
    "padel",
    "estacion",
    "boxeo",
    "cacharro",
    "teclado",
    "pelota",
    "automovil",
    "caravana",
    NULL
};

unsigned npalabras = sizeof (palabra) / sizeof (char *) - 1;


void
imprimir_todas () {
    for (int i=0; palabra[i]!=NULL; i++)
        printf ("%s\n", palabra[i]);
}

void
imprimir (unsigned pos) {
    printf (
            "%s\n",
            palabra[pos % npalabras]
            );
}

void title () {
    system ("clear");
    system ("toilet -fpagga --gay '   AHORCADO   '");
    printf ("\n\n\n");
}


void
presenta (unsigned lon, char palabra[MAX]) {

    DEBUG(palabra);
    printf ("\t\t");
    for (unsigned i=0; i<lon; i++) {
        if (palabra[i])
            printf ("%c", palabra[i]);
        else
            printf ("_");

        printf (" ");
    }

    printf ("\n\n\n");
}

bool
ha_perdido (unsigned errores, unsigned vidas){
    return errores >= vidas;
}

bool
ha_ganado (const char *adivinado, const char *palabra) {
    // Ignorar mayúsculas y minúsculas.
    return strcasecmp (adivinado, palabra) == 0;
}

unsigned
aciertos (unsigned turnos, unsigned  errores) {
    return turnos - errores;
}

unsigned
esta (char letra, const char *palabra) {
    unsigned veces = 0;

    while (*palabra != '\0') {
        if (*palabra == letra)
            veces++;
        palabra++;
    }

    return veces;
}

void
inserta (char letra, char adivinado[MAX], const char *original) {
    letra = tolower (letra);

    for (int i=0; original[i] != '\0'; i++)
        if (original[i] == letra)
            adivinado[i] = letra;
}
int
main (int argc, char *argv[])
{
    /*  DECLARACIÓN DE VARIABLES */
    unsigned pos;
    char     adivinado[MAX];
    size_t   lon;            // size_t es un alias de unsigned generalmente.
                             // lon: Número de carácteres de la palabra elegida.
    unsigned turnos   = 0,
             errores  = 0;

    char nueva_letra;


    /* INICIALIZACIÓN */
    bzero (adivinado, sizeof (adivinado));

    srand (time (NULL));
    pos = rand () % npalabras;
    lon = strlen (palabra[pos]);

    /* BUCLE DE JUEGO */
    do {
        title ();
        printf ("Turno: %u\n", turnos);
        printf ("Error: %u\n", errores);
        DEBUG(palabra[pos]);
        presenta (lon, adivinado);
        printf ("\n\nLetra: ");
        scanf  (" %c", &nueva_letra);

        turnos ++;


         if (esta (nueva_letra, palabra[pos]) )
             inserta (nueva_letra, adivinado, palabra[pos]);
         else
             errores++;

    // Si no ha ganado ni ha perdido: sigue jugando.
    } while (
            !ha_perdido (errores, MAX_INTENT) &&
            !ha_ganado (adivinado, palabra[pos]));

    presenta (lon, adivinado);

    return EXIT_SUCCESS;
}
