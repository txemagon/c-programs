#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#define MAX_PAL  10

int
main (int argc, char *argv[])
{
    char **lista;
    int n_words;

    /* Inicialización */
    printf ("¿How many words to input?: ");
    scanf (" %i", &n_words);
    lista = (char **) malloc ( n_words * sizeof (char *) );
    bzero ((void *) lista, sizeof (lista));

    /* ENTRADA DE DATOS */
    for (int i=0; i<n_words; i++) {
        printf ("Palabra %i: ", i+1);
        scanf (" %ms", &lista[i]);
    }

    /* SALIDA DE DATOS */
    for (int i=0; i<n_words; i++)
        printf ("%i.- %s\n", i+1, lista[i]);
    printf ("\n");

    /* HOUSEKEEPING */
    for (int i=0; i<n_words; i++)
        free (lista[i]);

    free (lista);

    return EXIT_SUCCESS;
}
