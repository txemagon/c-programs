#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 0x20

struct TEmpleado {
    char nombre[MAX];   // Campos
    double sueldo;
    char phone[MAX];
} pepe;                 // Variable global pepe

int
main (int argc, char *argv[])
{
    struct TEmpleado fer = {"Fernando", 1000, "963487"};

    fer.sueldo = 1200;
    strcpy (fer.nombre, "Don Fernando");

    printf ("%s: %.2lf\n", fer.nombre, fer.sueldo);

    return EXIT_SUCCESS;
}
