#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct TPrueba {
    int a;
    double b;
    char n[20];
    char *p;
};

void
paso (const struct TPrueba *TPrueba j) {

    printf ("a: %i\n",  (*j).a);
    printf ("b: %lf\n", j->b);
    printf ("n: %s\n",  j->n);
    printf ("p: %s\n",  j->p);

}

int
main (int argc, char *argv[])
{
    struct TPrueba y = { 2, 3.4, "pepelu", NULL };

    y.p = (char *) malloc (20);
    strcpy (y.p, "pepelu");

    paso (&y);
    return EXIT_SUCCESS;
}
