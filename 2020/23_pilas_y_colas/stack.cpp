#include "stack.h"

void
init (struct TStack *stack) {
    bzero (stack, sizeof (*stack));
    stack->status = normal;
}


bool
push (struct TStack *stack, data_type new_input)
{
    stack->status = normal;

    if (stack->summit < MAXSTACK) {
        stack->data[stack->summit++] = new_input;
        return true;
    }

    stack->status = full;

    return false;
}

data_type
pop (struct TStack *stack)
{
    stack->status = normal;

    if (stack->summit == 0) {
        stack->status = empty;
        return 0;
    }

    data_type ret = stack->data[--stack->summit];

    return ret;
}
