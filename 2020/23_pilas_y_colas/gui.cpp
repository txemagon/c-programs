#include "gui.h"
#include "ansi.h"

#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <unistd.h>

#define N 10

#if DATA_TYPE == DOUBLE || DATA_TYPE == FLOAT
#define A2N(data)  atof (data)
#else
#define A2N(data)  atoi (data)
#endif

void
title ()
{
    system ("clear");
    system ("toilet -fpagga --gay STACK");
    printf ("\n\n");
}

/*
 * Returns the amount of read items
 */
unsigned
ask (data_type *new_input)
{
    unsigned read;
    char buffer[N];

    ANS_MV (XPRMPT, YPRMPT);
    printf ("Number: ");
    read = scanf ("%[0-9]", buffer);
    __fpurge (stdin);

    if (read)
        *new_input = A2N (buffer);

    return read;
}

void show (data_type n)
{

    ANS_MV (XPRMPT, (YPRMPT-2));
    printf ("Output: %i", n);
}

void
print (struct TStack stack)
{
    for (unsigned i=0; i<stack.summit; i++)
        printf ("%3i: %u\n", i + 1, stack.data[i]);
    printf ("\n");
}


