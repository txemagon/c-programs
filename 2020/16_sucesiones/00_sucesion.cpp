#include <stdio.h>
#include <stdlib.h>

#define MAX 20

int
main (int argc, char *argv[])
{

    // Dos sucesiones distintas
    printf ("Forma 1:\n\n");
    for (int i=0; i<MAX; i++)
        printf ("%i ", i * (i + 1) );
    printf ("\n");

    // Reflexión
    printf ("Forma 2:\n\n");
    for (int i=1; i<1000000; i*= i+1)
        printf ("%i ", i );
    printf ("\n");

    return EXIT_SUCCESS;
}
