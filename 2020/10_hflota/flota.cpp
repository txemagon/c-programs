#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <strings.h>

#define DIM             8

#define AGUA            0
#define BARCO           1
#define PECIO           2

#define DISPARO         1

#define R_AGUA          0
#define R_TOCADO        1

#ifndef NDBUG
#define DEBUG(...)                    \
    fprintf (stderr, "\x1b[32m");     \
    fprintf (stderr, __VA_ARGS__ );   \
    fprintf (stderr, "\n" );          \
    fprintf (stderr, "\x1b[0m");      \
    __fpurge (stdin);                 \
    getchar ();
#else
#define DEBUG(...)
#endif

unsigned jugador (unsigned turno) { return turno % 2; }

void
cabecera (unsigned turno) {
    system ("clear");
    system ("toilet -fpagga --gay HUNDIR LA FLOTA");
    printf ("\n");
    printf ("Turno %u\t\tJugador %u\n", turno + 1, jugador (turno) + 1);
    printf ("\n\n");
}

unsigned
barcos_quedan ()
{
    return 1;
}

unsigned
queden_barcos () {
    return barcos_quedan ();
}

bool
fuera_de_rango (unsigned dimension, unsigned limite) {
    // Esto sería lo que hubiéramos puesto, pero
    // return dimension < 0 || dimension >= limite;
    // dimension no puede ser menor que 0 porque es unsigned.
    return dimension >= limite;
}



bool
fuera_de_mapa (unsigned fila, unsigned col)
{
    return
        fuera_de_rango (fila, DIM) || fuera_de_rango (col, DIM);
}



bool
ya_habia_disparado_en (unsigned disparo[DIM][DIM], unsigned fila, unsigned col) {
    return disparo[fila][col] != 0;
}




void
eliminar_barcos ()
{

}



void
imprimir (unsigned tablero[DIM][DIM]) {

    for (int f=0; f<DIM; f++) {
        printf ("\t");
        for (int c=0; c<DIM; c++)
            switch (tablero[f][c]) {
                case AGUA:
                    printf ("░");
                    break;
                case BARCO:
                    printf ("█");
                    break;
                case PECIO:
                    printf ("▚");
                    break;
            }

        printf ("\n");
    }

    printf ("\n");
}




unsigned
procesar_disparo (
        unsigned fila,
        unsigned col,
        unsigned tablero[DIM][DIM] )
{
    unsigned resultado = tablero[fila][col] == 1;

    // Si hay barco activo
    if (tablero[fila][col] == BARCO)
        tablero[fila][col] = PECIO;

    // Devolver el resultado del disparo
    return resultado;
}



void
preguntar_coordenadas (unsigned *fila, unsigned *col, unsigned disparo[DIM][DIM]) {
    do {
        printf ("Fila: ");
        scanf ( "%u", fila);
        printf ("Columna: ");
        scanf ( "%u", col);
    } while (fuera_de_mapa (*fila, *col) || ya_habia_disparado_en (disparo, *fila, *col));
}


void
apuntar_disparo (unsigned disparo[DIM][DIM], unsigned fila, unsigned col)
{
    disparo[fila][col] = DISPARO;
}

void
procesar_coordenadas (
        unsigned disparo[DIM][DIM],
        unsigned *fila,
        unsigned *col)
{
    preguntar_coordenadas (fila, col, disparo );
    apuntar_disparo (disparo, *fila, *col);
}

int
main (int argc, char *argv[])
{

    /* DECLARACIÓN DE VARIABLES */
    unsigned turno = 0;
    unsigned fila, col;
    unsigned tablero[2][DIM][DIM] = {
        /* INICIALIZACIÓN */
        {
            { 0, 0, 0, 0, 0, 0, 0, 0 },
            { 0, 0, 0, 0, 1, 0, 0, 0 },
            { 0, 0, 0, 1, 1, 0, 0, 0 },
            { 0, 0, 0, 0, 1, 0, 0, 0 },
            { 0, 0, 0, 0, 1, 0, 0, 0 },
            { 0, 0, 0, 0, 1, 0, 0, 0 },
            { 0, 0, 0, 0, 1, 0, 0, 0 },
            { 0, 0, 0, 0, 0, 0, 0, 0 }
        },

        {
            { 0, 0, 0, 1, 1, 0, 0, 0 },
            { 0, 0, 1, 0, 0, 1, 0, 0 },
            { 0, 0, 0, 0, 0, 1, 0, 0 },
            { 0, 0, 0, 0, 1, 0, 0, 0 },
            { 0, 0, 0, 1, 0, 0, 0, 0 },
            { 0, 0, 1, 0, 0, 0, 0, 0 },
            { 0, 0, 1, 1, 1, 1, 0, 0 },
            { 0, 0, 0, 0, 0, 0, 0, 0 }
        }
    };
    unsigned disparos[2][DIM][DIM];

    bzero (disparos, sizeof (disparos));

    // Meter los barcos

    /* BUCLE DE JUEGO */
    while (queden_barcos ()) {
        cabecera (turno);
        imprimir (tablero[jugador (turno)] );
        procesar_coordenadas (disparos[jugador (turno)], &fila, &col);
        DEBUG( "Coord: [%u, %u]", fila, col);
        turno++;
        procesar_disparo (fila, col, tablero[ jugador (turno) ]);
    }

    return EXIT_SUCCESS;
}
