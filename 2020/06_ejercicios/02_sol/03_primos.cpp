/*
 * =====================================================================================
 *
 *       Filename:  primos.cpp 1.0 13/01/21 10:55:46
 *
 *    Description:  Calculo de los 100 primeros números primos.
 *                  Parte II: La iteración sobre posibles_primos.
 *        txemagon / imasen (), txema.gonz@gmail.com txemagon
 *
 *    GNU General Public License <https://www.gnu.org/licenses/>.
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>


#define BUSCADOS    100

void
titulo ()
{
    system ("clear");
    system ("toilet -fpagga --gay PRIMOS");
    printf ("\n\n");

}





void
poner_resultado ( unsigned primo[BUSCADOS])  // | 1 | -> ... Y en cómo se recibe.
{
    for (unsigned i=0; i<BUSCADOS; i++)
        printf ("%u  ", primo[i]);

    printf ("\n\n");
}




int
main (int argc, char *argv[])
{
    /* DECLARACIÓN DE VARIABLES */
    unsigned posible_primo,
             tiene_divisores = 0,
             numero_de_primo = 0;  // Tenemos que saber qué número de primo se ha encontrado para guardarlo en la variable primo[X]

    unsigned primo[BUSCADOS];



    /* ENTRADA Y SALIDA DE DATOS */
    titulo ();


    /* CÁLCULOS */
    for (  unsigned posible_primo=2; numero_de_primo<BUSCADOS; posible_primo++) {

        /* Buscamos a ver si tiene divisores */
        tiene_divisores = 0;   // Preparamos la bandera para probar a otro posible_primo.
        for (unsigned posible_div=posible_primo-1; posible_div>1; posible_div--)
            if (posible_primo % posible_div == 0)
                tiene_divisores = 1;

        /* Y si los tiene: YAY! Hemos encontrado un primo más!!! */
        if (!tiene_divisores) { // Acuérdate: La ! es una negación lógica.
            primo[numero_de_primo] = posible_primo;
            numero_de_primo++;  // Apuntamos que hemos encontrado un primo más.
        }
    }


    /* SALIDA DE DATOS */
    poner_resultado (primo);  // Fijate en cómo se pasa la variable -> | 1 |


    return EXIT_SUCCESS;
}
