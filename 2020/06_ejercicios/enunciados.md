# Ejercicios Post Navidad

## Ejercicio 1

Hoy vais a intentar crear un programa mirando los apuntes, páginas de Internet y, finalmente,
si os atascáis, la solución comentada que subiré. Pero, recordad, si tenéis que mirar es preferible
que volváis a empezar de cero. Si os atascáis mucho, entonces es preferible que vayáis leyendo la solución
poniendo anotaciones en las partes que comprendéis.

En la primera parte tenéis que hacer un programa que pide un número entero y lo almacena en la variable
entera `posible_primo`

Seguidamente, escribir un bucle `for` que pruebe a dividir a `posible_primo` por todos los números menores
que él y mayores que 1. Poner una variable de tipo bandera a 1 si se encuentre algún número que divide a
`posible_primo` ( que el resto de la división es 0 ).

Para finalizar, imprimir que el número es primo si la variable bandera no está levantada (está a 0) o que
no lo es si sí está levantada.

Espero que ayer leyerais las correcciones detenidamente. Me consta que algunos sí porque preguntaron dudas.
Espero que lo encontrarais todo lo suficientemente paso a paso. Como necesito que tengáis asimilado el contenido
para seguir a partir de ahí, a la vuelta os preguntaré por él.

## Ejercicio 2

El objetivo de hoy es soltarse con los arrays. Ya habíamos visto en clase que un array se puede ver de dos formas.

La metafórica: se están creando un montón de variables numeradas.

Así por ejemplo:

```c
int A[3];
```

crearía las variables A[0], A[1] y A[2]

```
           A[0]A[1]A[2]
          +---+---+---+
          |   |   |   |
          +---+---+---+
```

cada una de 4 bytes por ser de tipo `int`.

La real: Se hace una reserva de memoria de 12 bytes y se crea una variable `A` que contiene la dirección de
memoria del primer byte.

```
           A[0]A[1]A[2]
+---+     +---+---+---+
| o-+---->|   |   |   |
+---+     +---+---+---+
  A
```

En ese caso `A[2]` se leerá: _desde la dirección a la que apunta A sáltate 2 enteros (8 bytes)_ .

Bueno pues para hacer el ejercicio queremos guardar en un array los 100 primeros números primos.

Para ello vamos a coger el ejercicio anterior y vamos a hacer que posible primo se vaya incrementando
de 1 en 1 desde el 2 y por cada primo que encontremos vamos a guardarlo en una de esas "variables numeradas".



## Ejercicio 3

Haz un seguimiento de las variables del programa anterior corregido linea a linea en la parte de los bucles for
anidados. Ayúdate del gdb si lo consideras necesario.

## Ejercicio 4

Usando el ejercicio 2 como base confecciona un programa que imprima la suma de los 100 primeros números primos.


Para practicar con los bucles anidados (uno dentro de otro) os voy a poner ejercicios para tres días. Iré subiendo
las soluciones comentadas. Podéis seguir preguntando las dudas por correo como hasta ahora.

## Ejercicio 5

Se trata de confeccionar un programa que pregunte el lado `l` al usuario y que dibuje en pantalla un cuadrado.
Acuérdate de que no se puede posicionar el cursor en una coordenada específica mas hay que dibujarlo línea a línea.

Ejemplo:
```
Sea l=5

*****
*****
*****
*****
*****

```

## Ejercicio 6

Usa dentro de los bucles anidadados (uno para dibujar cada línea y en cada línea uno para dibujar cada columna) una
estructura condicional que elija si pintar un `*` o un ` ` (espacio).

Ejemplo:

```
Sea l=5

*****
*   *
*   *
*   *
*****

```

## Ejercicio 7

Usa la estructura condicional para dibujar asteriscos sólo en la diagonal principal del cuadrado.

Ejemplo:

```
Sea l=5

*    (CR)
 *   (CR)
  *  (CR)
   * (CR)
    *(CR)

```

Te indico el retorno de carro (`\n`) como (CR) en el dibujo.

## Ejercicio 8

Ahora vamos a hacer lo mismo para pintar la diagonal secundaria. Para plantear el ejercicio
píntalo en un papel con el ejemplo de l=5 y apunta en qué coordenadas has puesto el `*`.
Repite el ejemplo con `l=6`.

Intenta averiguar qué condición cumplen las coordenadas en las que has puesto el `*`.

Acuérdate de que nosotros, los informáticos, numeramos todo desde el 0.

Ejemplo:

```
Sea l=5

    *(CR)
   * (CR)
  *  (CR)
 *   (CR)
*    (CR)

```


## Ejercicio 9

Vamos a unir todo lo anterior.

```
Sea l=5

*****(CR)
** **(CR)
* * *(CR)
** **(CR)
*****(CR)

Sea l=7

*******
**   **
* * * *
*  *  *
* * * *
**   **
*******
```

Todos los ejercicios anteriores son variaciones del cuadrado. En principio deberías de poder hacerlos en un dia o dia y medio.

Para que sigas practicando Te dejo ejercicios de triángulos. El secreto de los mismos es darse cuenta de que el número de
columnas en cada fila depende del número de fila que estés pintando.

## Ejercicio 10

Vamos a pintar un triángulo rectánculo isósceles conocida la altura.

Ejemplo:

```
sea h=5

*(CR)
**(CR)
***(CR)
****(CR)
*****(CR)
```


## Ejercicio 11


Este ejercicio realmente pinta un cuadrado aunque parezca un triángulo.

```
sea h=5

    *(CR)
   **(CR)
  ***(CR)
 ****(CR)
*****(CR)
```

## Ejercicio 12

Vamos a unir los dos anteriores.

```
sea h=5

    **(CR)
   ****(CR)
  ******(CR)
 ********(CR)
**********(CR)

```

Pista: En cada fila tienes que pintar todas las columnas del cuadrado del ejercicio 11 y el triángulo del ejercicio 10. Para eso, dentro del bucle
de las filas tienes que anidar en serie (uno después de otro) los bucles de las columnas de los dos ejercicios anteriores.

## Ejercicio 13

Vamos a invertir un triángulo. Piensa ahora la relación que hay entre las columnas que hay en cada fila y la fila que estás pintando.

```
sea h=5

*****(CR)
****(CR)
***(CR)
**(CR)
*(CR)
```


## Ejercicio 14

El triángulo de Batalla. Ahora vamos a hacer algo parecido al ejercicio 12, pero ahora nos van a dar la base en vez de la altura. Es un ejercicio envenenado.
Mira estos dós ejemplos:

```
sea b=8
    **
   ****
  ******
*********
```

Este ejemplo lo podrías resolver como el ejercicio previo.
Pero mira este otro caso:

```
sea b=9
     *
    ***
   *****
  *******
**********
```

Éste no se podría resolver así.


En principio deberías tener terminado hasta aquí el martes a última hora.


Para terminar de coger soltura vamos a poner ejercicios en los que haya que anidar más bucles.

## Ejercicio 15

Vamos a concatenar cuadrados.

Ejemplo:

```
sea l=3

***   ***(CR)
***   ***(CR)
***   ***(CR)
```

```
sea l=4

****    ****    (CR)
****    ****    (CR)
****    ****    (CR)
****    ****    (CR)
```

```
sea l=5

*****     *****     *****(CR)
*****     *****     *****(CR)
*****     *****     *****(CR)
*****     *****     *****(CR)
*****     *****     *****(CR)
```

## Ejercicios 16

Vamos a hacer un damero.

Ejemplo:

```
sea l=5

*****     *****     *****(CR)
*****     *****     *****(CR)
*****     *****     *****(CR)
*****     *****     *****(CR)
*****     *****     *****(CR)
     *****     *****     (CR)
     *****     *****     (CR)
     *****     *****     (CR)
     *****     *****     (CR)
     *****     *****     (CR)
*****     *****     *****(CR)
*****     *****     *****(CR)
*****     *****     *****(CR)
*****     *****     *****(CR)
*****     *****     *****(CR)
     *****     *****     (CR)
     *****     *****     (CR)
     *****     *****     (CR)
     *****     *****     (CR)
     *****     *****     (CR)
*****     *****     *****(CR)
*****     *****     *****(CR)
*****     *****     *****(CR)
*****     *****     *****(CR)
*****     *****     *****(CR)
```

### Ejercicio 17

Vamos a concatenar cuadrados en los que sólo pintamos una diagonal, pero vamos a alternar la diagonal.

```
sea l=5

*        **        **    (CR)
 *      *  *      *  *   (CR)
  *    *    *    *    *  (CR)
   *  *      *  *      * (CR)
    **        **        *(CR)
```


O sea, que además de que el lado es l (5) también hay l (5) cuadrados.


## Ejercicio 18

Vamos a variar el ejercicio anterior para no imprimir la primera columna en los cuadrados anexos.
Numeramos la columna para que lo puedas ver claramente.

```
sea l=5

012341234123412341234
*       *       *    (CR)
 *     * *     * *   (CR)
  *   *   *   *   *  (CR)
   * *     * *     * (CR)
    *       *       *(CR)
```

Los primeros os costarán más que los siguientes. Espero que los disfrutéis.
