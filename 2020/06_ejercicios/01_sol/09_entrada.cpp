/*
 * =====================================================================================
 *
 *       Filename:  entrada.cpp 1.0 13/01/21 10:55:46
 *
 *    Description:  Simplificamos poner el resultado.
 *        txemagon / imasen (), txema.gonz@gmail.com txemagon
 *
 *    GNU General Public License <https://www.gnu.org/licenses/>.
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#define MIN         2
#define MAX      1000


void
titulo ()
{
    system ("clear");
    system ("toilet -fpagga --gay PRIMOS");
    printf ("\n\n");

}





void
poner_resultado ( unsigned posible_primo, unsigned tiene_divisores)
{
        printf ( "\n\n\t%i %s es primo.\n\n",
                posible_primo,
                /* Repasa el uso del operador condicional */
                tiene_divisores ? "no" : ""
               );
}





unsigned
pedir_numero ()
{
    unsigned posible_primo;
    printf ("Número [%i - %i]: ", MIN, MAX);
    scanf ("%u", &posible_primo);


    return posible_primo;
}




int
main (int argc, char *argv[])
{
    /* DECLARACIÓN DE VARIABLES */
    unsigned posible_primo,
             tiene_divisores = 0;



    /* ENTRADA Y SALIDA DE DATOS */
    do {
        titulo ();
        posible_primo = pedir_numero ();
    } while (posible_primo < MIN || posible_primo > MAX);



    /* CÁLCULOS */
    for (unsigned posible_div=posible_primo-1; posible_div>1; posible_div--)
        if (posible_primo % posible_div == 0)
            tiene_divisores = 1;


    /* SALIDA DE DATOS */
    poner_resultado (posible_primo, tiene_divisores);


    return EXIT_SUCCESS;
}
