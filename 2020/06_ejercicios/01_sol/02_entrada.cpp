/*
 * =====================================================================================
 *
 *       Filename:  01_entrada.cpp 1.0 13/01/21 10:55:46
 *
 *    Description: Vamos a ver qué otras instrucciones de entrada salida podemos usar.
 *        txemagon / imasen (), txema.gonz@gmail.com txemagon
 *
 *    GNU General Public License <https://www.gnu.org/licenses/>.
 * =====================================================================================
 */

int
main (int argc, char *argv[])
{
    /* DECLARACIÓN DE VARIABLES */
    int posible_primo;

    /* ENTRADA Y SALIDA DE DATOS */
    // Una alternativa a printf y scanf es usar la familia de
    // funciones put y get, pero están pensadas para trabajar
    // con caracteres o cadenas (de caracteres)
    puts ("Introduce un número entre 1 y 1000: ");
    scanf ("%i", &posible_primo);

    /* CÁLCULOS */
    // Este bloque lo rellenaremos a medida que vayamos
    // realizando el ejercicio.

    /* SALIDA DE DATOS */
    printf ("Has introducido el número %i\n", posible_primo);
    /*
     * Fíjate en el trabajo de %i. Imagínate que posible_primo
     * vale 7. Para poder ver en la pantalla ese dibujo será
     * necesario mandar el código ASCII que tiene ese dibujo.
     * Luego %i va a mandar a pantalla un 55, o si lo prefieres
     * ver en hexadecimal 0x37.
     *
     * La cosa se complicaría si posible_primo valieses 47, en
     * ese caso %i tendría que mandar dos números a pantalla:
     * uno para el 4 => 0x34 y otro para el 7 => 0x37. O sea
     * que acabaría enviando: 0x3437 (que sinceramente ahora mismo
     * de cabeza no sé cuánto es en decimal).
     * */

    return EXIT_SUCCESS;
}

