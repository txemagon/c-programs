/*
 * =====================================================================================
 *
 *       Filename:  03_entrada.cpp 1.0 13/01/21 10:55:46
 *
 *    Description:  Ahora el objetivo es proteger la entrada para que los números
 *                  estén comprendidos en un rango.
 *        txemagon / imasen (), txema.gonz@gmail.com txemagon
 *
 *    GNU General Public License <https://www.gnu.org/licenses/>.
 * =====================================================================================
 */

int
main (int argc, char *argv[])
{
    /* DECLARACIÓN DE VARIABLES */
    unsigned posible_primo;     // Como los números son positivos vamos a poner el calificativo
                                // unsigned delante de int para decirle que el primer bit es de
                                // magnitud y no de signo. Quedando: unsigned int posible_primo;
                                // Como int es el tipo de datos por defecto lo podemos retirar
                                // de la declaración.

    /* ENTRADA Y SALIDA DE DATOS */
    printf ("Número [1 - 1000]: ");
    scanf ("%u", &posible_primo);    // Fíjate que hemos cambiado %i
                                     // Otro dato interesante es el operado & que aparece delante
                                     // de la variable. Se lee: la dirección de.
                                     //
                                     // En este caso el dato que se está comunicando a scanf
                                     // es una dirección de memoria. El sitio en el que tiene
                                     // que escribir el dato proporcionado por el usuario.
                                     //
                                     // Cuando un parámetro va precedido por un & (ampersand mal
                                     // llamado o más correctamente and per se et - per se et),
                                     // entonces se dice que la variable se pasa por referencia.
                                     // En el resto de los casos se dice que se pasa por valor.

    /* CÁLCULOS */

    /* SALIDA DE DATOS */
    printf ("Has introducido el número %i\n", posible_primo);

    return EXIT_SUCCESS;
}

