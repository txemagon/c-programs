/*
 * =====================================================================================
 *
 *       Filename:  03_entrada.cpp 1.0 13/01/21 10:55:46
 *
 *    Description:  Ahora el objetivo es proteger la entrada para que los números
 *                  estén comprendidos en un rango. (II)
 *        txemagon / imasen (), txema.gonz@gmail.com txemagon
 *
 *    GNU General Public License <https://www.gnu.org/licenses/>.
 * =====================================================================================
 */

int
main (int argc, char *argv[])
{
    /* DECLARACIÓN DE VARIABLES */
    unsigned posible_primo;


    /* ENTRADA Y SALIDA DE DATOS */
    do {
        printf ("Número [1 - 1000]: ");
        scanf ("%u", &posible_primo);
    } while (posible_primo <1 || posible_primo > 1000);

    /*
     * Necesitamos el bucle do - while de entre todos los que
     * tenemos disponibles porque necesitamos que el bloque se
     * ejecute una vez sí o sí.
     *
     */

    /* CÁLCULOS */


    /* SALIDA DE DATOS */
    printf ("Has introducido el número %i\n", posible_primo);

    return EXIT_SUCCESS;
}

