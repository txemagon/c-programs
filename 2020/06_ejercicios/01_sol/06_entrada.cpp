/*
 * =====================================================================================
 *
 *       Filename:  entrada.cpp 1.0 13/01/21 10:55:46
 *
 *    Description:  Todo el código que cumpla una única función lo vamos a a islar
 *                  en funciones. Vamos a empezar por el título.
 *        txemagon / imasen (), txema.gonz@gmail.com txemagon
 *
 *    GNU General Public License <https://www.gnu.org/licenses/>.
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#define MIN         1
#define MAX      1000

void /* void significa vacío y es el tipo de dato de las funciones que no devuelven nada. */
titulo ( /* No se necesitan parámetros*/ ) // Las funciones que no necesitan parámetros se llaman procedimientos.
{
    system ("clear");
    system ("toilet -fpagga --gay PRIMOS");
    printf ("\n\n");

}


    int
main (int argc, char *argv[])
{
    /* DECLARACIÓN DE VARIABLES */
    unsigned posible_primo;


    /* ENTRADA Y SALIDA DE DATOS */
    do {
        titulo ();
        printf ("Número [%i - %i]: ", MIN, MAX);
        scanf ("%u", &posible_primo);
    } while (posible_primo < MIN || posible_primo > MAX);

    /* CÁLCULOS */


    /* SALIDA DE DATOS */
    printf ("Has introducido el número %i\n", posible_primo);

    return EXIT_SUCCESS;
}
