/*
 * =====================================================================================
 *
 *       Filename:  entrada.cpp 1.0 13/01/21 10:55:46
 *
 *    Description:  Afrontamos el algoritmo.
 *        txemagon / imasen (), txema.gonz@gmail.com txemagon
 *
 *    GNU General Public License <https://www.gnu.org/licenses/>.
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#define MIN         2
#define MAX      1000


void
titulo ()
{
    system ("clear");
    system ("toilet -fpagga --gay PRIMOS");
    printf ("\n\n");

}





void
poner_resultado (
        unsigned posible_primo,
        unsigned tiene_divisores        /* Necesitamos saber si tiene divisores para saber si el número es primo. */
        )
{
    if (tiene_divisores)              /* Si el resultado de la expresión es 0 se considera falso. */
        printf (
                "\n\n\t"
                "%i %s\n\n",          /* %s se substituye por una cadena de caracteres. */
                posible_primo,
                "no es primo"         /* ésta concretamente. */
               );
    else
        printf (
                "\n\n\t"
                "%i %s\n\n",
                posible_primo,
                "es primo"
               );
}





unsigned
pedir_numero ()
{
    unsigned posible_primo;
    printf ("Número [%i - %i]: ", MIN, MAX);
    scanf ("%u", &posible_primo);


    return posible_primo;
}




int
main (int argc, char *argv[])
{
    /* DECLARACIÓN DE VARIABLES */
    unsigned posible_primo,
             tiene_divisores = 0;  // Aquí nos interesa que 0 se considera falso.
    // Fíjate que elegir bien el nombre de las variables
    // posibilita que el código se pueda leer.


    /* ENTRADA Y SALIDA DE DATOS */
    do {
        titulo ();
        posible_primo = pedir_numero ();
    } while (posible_primo < MIN || posible_primo > MAX);

    /* CÁLCULOS */
    for (unsigned posible_div=posible_primo-1; posible_div>1; posible_div--)
        if (posible_primo % posible_div == 0)
            tiene_divisores = 1;  // Una variable tipo bandera sólo se puede poner. No se puede "bajar".


    /* SALIDA DE DATOS */
    poner_resultado (posible_primo, tiene_divisores);

    return EXIT_SUCCESS;
}
