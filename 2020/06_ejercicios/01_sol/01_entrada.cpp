/*
 * =====================================================================================
 *
 *       Filename:  01_entrada.cpp 1.0 13/01/21 10:55:46
 *
 *    Description:  Gestión de la entrada en un programa.
 *        txemagon / imasen (), txema.gonz@gmail.com txemagon
 *
 *    GNU General Public License <https://www.gnu.org/licenses/>.
 * =====================================================================================
 */

/*
 * Un programa debe de tener separados los bloques
 * de:
 *     1. Declaración de Variables
 *     1. Entrada de Datos
 *     1. Cálculos
 *     1. Salida de Datos
 */

/* Fíjate que hay comentarios multilinea que empiezan por /*
 * y de línea simple //
 */

int
main (int argc, char *argv[])
{
    /* DECLARACIÓN DE VARIABLES */
    int posible_primo;

    /* ENTRADA Y SALIDA DE DATOS */
    /* La entrada y salida básica la hacemos con printf y scanf */
    printf ("Introduce un número entre 1 y 1000: ");
    scanf ("%i", &posible_primo);

    /* %i es un especificador de formato
     * Hay muchos especificadores de formato más
     * que iremos viendo, pero si tienes interés ahora
     * puedes poner en la terminal: man scanf, o si
     * estás viendo esto desde el vim puedes poner el
     * cursor encima de scanf y presionar la K (mayúscula).
     */


    /* CÁLCULOS */
    // Este bloque lo rellenaremos a medida que vayamos
    // realizando el ejercicio.


    return EXIT_SUCCESS;    // EXIT_SUCCESS es una constante definida
                            // en stdlib.h y vale 0
}

