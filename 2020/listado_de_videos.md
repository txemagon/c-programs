# Listado de Vídeos

Hola a todos, os pongo un listado de vídeos para que repaséis los temas que os veáis más flojos.

También podéis mirar estos enunciados y soluciones del año 2019
https://gitlab.com/txemagon/c-programs/-/blob/master/2019/22_ramillete_de_ejercicios/enunciados.md

## Teoría
* Variables estááticas: https://youtu.be/CehQEIxfiKk
* Recursividad: https://youtu.be/thcfQjvOmJE
* Gramática de los tipos de datos: https://youtu.be/wTvBs3sxvqI
* Recorrer un array de caracteres recursivamente: https://youtu.be/aNU9FhmCE80
* Analizar la línea de comandos: https://youtu.be/Kqp5wqIlpIE
* Imprimir los ordinales: https://youtu.be/nN6qRHwES_k
* Parámetros de los punteros a funciones: https://youtu.be/GiM3D1VL3Jg
* Cómo separar un código fuente en múltiples ficheros: https://youtu.be/JeRMY2EsgUc
* Makefile genérico. Explicación: https://youtu.be/Zs5RkJmLMIg
* Ejemplo básico de Pila: https://youtu.be/rRo1GbxezUw
* Ejemplo de pila con reserva dinámica de memoria: https://youtu.be/GZXH3SP5L98
* Ejemplo de cola: https://youtu.be/obO359DNRKo
* Ejemplo de cola circular: https://youtu.be/eQBo1aBpyAo
* Cambiar el tipo de datos de una pila usando typedef: https://youtu.be/9bCehNmqNYg
* El uso de typedef con las estructuras: https://youtu.be/14MSVYvBq0g
* Listas enlazadas: https://youtu.be/P3L10W7pR40
* Preparando el entorno para la POO: https://youtu.be/LZ64FThX8nE
* Programa base con structs para luego hacer POO: https://youtu.be/AE7JynCBKqs
* Pasando de la filosfía de estructuras a clases: https://youtu.be/cf1SF562r2I



## Dudas

* Uso de punteros como parámetros:  https://youtu.be/q2qe77SQciU
* Uso de arrays como parámetros: https://youtu.be/71yEUzigdiM
* Uso de malloc al multiplicar arrays dinámicos: https://youtu.be/8DUautD2t6U
* Metodo de ordenación de la burbuja: https://youtu.be/LCtM-LxjJRI
* Metodo de ordenación por inserción: https://youtu.be/cXczPPShJmw
* Ordeación de una lista de punteros: https://youtu.be/1h4aY5bbb4I
* Ordenación de una lista de palabras: https://youtu.be/8gLS12N7-iw
* Llamadas a funciones usando un array de punteros a funciones. Problema de las áreas: https://youtu.be/yldZiqQKHRk
