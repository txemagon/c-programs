#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
    FILE *pf = NULL;
    char car;

    if (!(pf = fopen ("fortune.txt", "r"))){
        fprintf (stderr, "File not found.\n");
        return EXIT_FAILURE;
    }

    do {
        fscanf (pf, "%c", &car);
        printf ("%c", car);
    } while (!feof (pf));

    fclose (pf);

    return EXIT_SUCCESS;
}
