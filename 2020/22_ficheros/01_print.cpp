#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <strings.h>



void
print (const char *plet){
    for (;*plet!='\0'; plet++)
        printf ("%c", *plet);
}

int
main (int argc, char *argv[])
{
    FILE *pf;
    long size;

    const char *text = NULL;

    // Conectar un stream a un fichero
    pf = fopen ("fortune.txt", "r");
    if (!pf) {
        fprintf (stderr, "File not found.\n");
        return EXIT_FAILURE;
    }

    // Calcular Tamaño del Fichero
    fseek (pf, 0, SEEK_END);
    size = ftell(pf);
    rewind (pf);

    // Cargamos en memoria el fichero
    text = (const char *) malloc (size);
    fread ((void *) text, size, 1, pf);

    print (text);

    fclose (pf);

    return EXIT_SUCCESS;
}
