#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define NBs    2                    /* NIBBLES POR LÍNEA */
#define SPRITE "183C7EDBFF5A8142"

// #define DEBUG

#ifdef DEBUG
#define DBG(x) printf("(%X)", x);
#else
#define DBG(x)
#endif




/* Convertir de ASCII a Hexa */
int convertir(char letra){

    letra = tolower(letra);
    if (letra >= 'a')
        return letra - 'a' + 10;

    return letra - '0';        // Conseguir el siguiente nibble
}



/* Pintar un nibble entero */
void pintar(unsigned char nibble) {
    unsigned char mask = 8;

    DBG( nibble );

    for (int p=0; p<4; p++) {
        if (mask & nibble)
            printf("█");
        else
            printf("-");
        mask >>= 1;
    }
}



int main() {

    char sprite[] = SPRITE;
    unsigned char nibble;

    for (int i=0; sprite[i]!=0; i++) {

        nibble = convertir (sprite[i]);
        pintar (nibble);                 // pintar el nibble

        if ( (i+1) % NBs == 0 )
            printf("\n");                // Imprimir un salto de línea

    }

    return EXIT_SUCCESS;
}

