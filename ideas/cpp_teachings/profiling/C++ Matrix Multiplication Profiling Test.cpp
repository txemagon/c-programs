///
/// MATRIX MULTIPLICATION TEST
///   Test cost of matrix multiplication in two scenarios
///      1. R = A*B (direct)
///      2. Transpose B, then R = A*B (taking into account B is transposed)
///
///   Program does not output anything. It is just created for profiling purposes.
///
///
/// LICENSE
///
///  Copyright(C) 2017 ronaldo / Cheesetea / ByteRealms / Fremos.
///  Contact: ronaldo@cheesetea.com, @FranGallegoBR
///
///  This program is free software: you can redistribute it and/or modify
///  it under the terms of the GNU General Public License as published by
///  the Free Software Foundation, either version 3 of the License, or
///  (at your option) any later version.
///
///  This program is distributed in the hope that it will be useful,
///  but WITHOUT ANY WARRANTY; without even the implied warranty of
///  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///  GNU General Public License for more details.
///
///  You should have received a copy of the GNU General Public License
///  along with this program.  If not, see <http://www.gnu.org/licenses/>.
///
#include <iostream>
#include <ctime>
#include <cstdlib>

// When defined, B matrix will be transposed first,
// then multiplication will take this into account
#define TRANSPOSE

// Constants
//   BEWARE: a big "size" will make matrices take more than your 
//   program stack can handle, giving segmentation faults.
//
const uint32_t size        = 500;      // Size of square matrices
const uint32_t randfactor  = 10000000; // rand() will be divided by this

///
/// Transposes a matrix in-place
///
#ifdef TRANSPOSE
void 
transposeMatrix(uint32_t m[][size]) {
   for(uint16_t i=0; i < size; ++i) {
      for(uint16_t j=i; j < size; ++j) {
         uint32_t t = m[i][j];
         m[i][j] = m[j][i];
         m[j][i] = t;
      }
   }
}

///
/// r = a*b (Matrices)
///  -> Assumes B is given transposed
///
void
multiplyMatricesT(uint32_t r[][size], uint32_t a[][size], uint32_t b[][size]) {
   for(uint16_t i=0; i < size; ++i) {
      for(uint16_t j=0; j < size; ++j) {
         uint32_t s = 0;
         for(uint16_t k=0; k < size; ++k) {
            s += a[i][k] * b[j][k];
         }
         r[i][j] = s;
      }
   }
}
#endif

///
/// r = a*b (Matrices)
///
void
multiplyMatrices(uint32_t r[][size], uint32_t a[][size], uint32_t b[][size]) {
   for(uint16_t i=0; i < size; ++i) {
      for(uint16_t j=0; j < size; ++j) {
         uint32_t s = 0;
         for(uint16_t k=0; k < size; ++k) {
            s += a[i][k] * b[k][j];
         }
         r[i][j] = s;
      }
   }
}

///
/// Populates matrix m in-place randomly
///
void
populateRandMatrix(uint32_t m[][size]) {
   for(uint16_t i=0; i < size; ++i) {
      for(uint16_t j=0; j < size; ++j) {
         m[i][j] = std::rand() / randfactor;
      }
   }
}

///
/// PROGRAM
///
int main(void) {
   uint32_t a[size][size], b[size][size], c[size][size];

   // Populate Matrices a and b randomly
   std::srand(std::time(0));
   populateRandMatrix(a);
   populateRandMatrix(b);

   // Calculate c=a*b
#ifdef TRANSPOSE
   transposeMatrix(b);
   multiplyMatricesT(c, a, b);
#else
   multiplyMatrices(c, a, b);
#endif

   return 0;
}