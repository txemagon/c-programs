# Notes

## Herramientas de Interés

Compiler explorer: https://godbolt.org/
cppinsights:       https://cppinsights.io/


## Punteros a función y Delegados

```bash
float (*pfunc)(int, float) = &afunction;
void (CTestClass::*pmet)(int);

pmet = &CtestClass::method;

CTestClass o;

(o.*pmet)(7);

```

## Placement New

Podemos hacer reservas, pero el problema es llamar al constructor.
No reserves la memoria, pon el objeto aquí:

Ahora no se llama automáticamente a los constructores y hay que hacerlo a mano.

```cpp
g1 = new (puntero) CGameObject;
```

Las clases que asignan y liberan memoria se llaman Arena.

```cpp
class Arena
{
    public:
        Arena(std::size_t size)
        {
            m_nextplace = m_memory = new uint8_t[size];
        }

        ~Arena() {
            delete [] m_memory;
        }

        void *allocate(std::size_t size)
        {
            uint8_t *p = m_nextplace;
            m_nextplace += size;
            return p;

        }

        void deallocate(void *p)
        {

        }
    private:
        uint8_t *m_memory;
        uint8_t *m_nextplace;
};

void *operator new(std::size_t size, Arena &A) {
    return A.allocate (size);
}

void operator delete (void *p, Arena &A) {
    return A.deallocate (p);
}

int
main () {
    Arena myMem(128);



    g1 = new (myMem) CGameObject;  // Ahora el destructor no se llama.

    ...

    g1->~CGameObject();
    myMem.deallocate(g1);
}
```


A delete no se le puede llamar directamente, sólo si hay una excepción en el constructor.

## Patrón Fachada

La comunicación con múltiples componentes se hace a partir de un único componente.

## pimpl y strategy


## Adjetivos


### Override

```cpp
void Clase::algo() const = 0;

void Clase::algo() const override {};
```


### Destructores Virtuales

```cpp
virtual ~Renderer();
```
Si en la clase padre no hubiere destructor,
habrá uno por defecto y no se llamaría al hijo
en caso de polimorfismo.


### enum class

```cpp
enum class TRenderer { STDP, STDP2 };

class Renderer {


    TRenderer m_selected = TRenderer::STDP;
};
```

Obligan a poner el prefijo de clase.

### default

Destructores por defecto;

Vale para añadir virtual al destructor.


### Constructor singleton

```cpp

class CRenderer;
enum class TRenderer { STDP, STDP2 };
class CRendererMan {

    public:
        ~CRendererMan();
        static CRendererMan& p();

        void switchRenderer();

    private:
        CRendererMan();

        CRenderer*    m_renderer = nullptr;
        TRenderer     m_selected = TRenderer::STDP;

};

CRendererMan&
CRendererMan::p() {
    static CRendererMan instance;
    return instance;
}

```


### Destructores Virtuales


```cpp
struct CRenderer {
    virtual ~CRenderer() = default;
};
```

Para que se llame al destructor de la clase hija hace falta poner virtual.cpp
Si no ponemos default hay que implementar el destructor. Así podemos poner virtual sin más.


## Costes de Memoria

### Memoria Básica

```cpp
#include <iostream>
#include <cstdint>

const uint32_t size = 10000000;

uint32_t values[size] = {1};
volatile uint32_t m_total = 0; // Puede ser accedida externamente.

void sum() {
    for (uint32_t i=0; i<size; ++i)>) {
        m_total += values[i];
    }
}

int main() {
    sum();
    std::cout << m_total << "\n";

    return 0;
}
```

Web: Compiler Explorer


Las variables locales, a veces, se almacenan en un registro.

Para ver las diferencias:

```bash
time ./m
valgrind --tool=callgrind --dump-line=yes ./m
callgrind_annotate --auto=yes callgrind.out.5933
```

### Memoria Caché


```bash
valgrind --tool=cachegrind ./m
cg_annotate --auto=yes cachegrind.xxx
```

Data Oriented design and C++: https://youtu.be/rX0ItVEVjHc
Mike Acton: Insomnia Games.

o3 desenrolla el bucle para meterlo en el pipeline (ejecución especular y fuera de orden)
Ojo con los if dentro de los bucles.

## La regla de los 3 (5)

```cpp
unsing TGO = std::vector<GameObject>;  // typedef pero comprobando tipos
std::mov(algo);                        // rvalue semantics

// constexpr es una expresión constante cuyo valor se puede calcular en tiempo de compilación
constexpr float x { 9 };

// En el caso de las funciones, el valor de retorno se intentará calcular en tiempo de compilación para cada llamada.
constexpr float exp (float x, int n)
{
    return n == 0 ? 1 :
        n % 2 == 0 ? exp (x * x, n / 2) :
        exp ( x * x, (n - 1) / 2 ) * x;
}

inline   // Opcional en la declaración de clase ahora: sustituir el código de la función (como una macro) en la llamada.
noexcept // El código no lanza excepciones. Exception warranty

vgo.push_back (GameObject c);  // Llamáda implícita al constructor de copia.

```

La regla de los 3:

> Cuando hace falta:
>
>  o Un constructor de copia
>  o Un operador de asignación
>  o Un destructor específico
>
> HACEN FALTA LOS TRES


La regla de los 5:

> Cuando hace falta:
>
>  o Un constructor de copia
>  o Un constructor de movimiento
>  o Un operador de asignación
>  o Un operador de asignación de movimiento
>  o Un destructor específico
>
> HACEN FALTA LOS CINCO


¿ Por qué referencias en vez de constructores?

No pueden ser null.

```cpp
if (g.spr)   // Hay que validar que no sea nullptr
    spr = new Sprite (*g.spr)
```

## Vector

```cpp
std::vector
```

Características:

1. Se tiene que comportar como un vector.
1. Es una objeto estático.
1. Las celdas se reservan dinámicamente.
1. `push_back` es quien reserva la memoria.
1. La memoria se va doblando cuando se agota.
1. `size()` cantidad de elementos ocupados.
1. `capacity()` cantidad de elementos reservados.
1. `max_size()` máximo número teórico de elementos.
1. `reserve(size_t)` Hace una reserva inicial.
1. `emplace_back()`


## Copy & Swap Idiom

```cpp
#include <cstdio>

struct GameObject {

  GameObject() : a(2) {}
  GameObject(int a) : a(a) {}
  GameObject(const GameObject &other) {
    a = other.a;
  }
  int a;
};

int main()
{
    GameObject go0;
    GameObject go1(2);
    GameObject go2(go1);   // Fíjate cómo se ven estas dos líneas
    GameObject go3 = go2;  // Resulta que el compilador las ve =.
}
```

El compilador ve este código.

```cpp
#include <cstdio>

struct GameObject
{
  inline GameObject()
  : a{2}
  {
  }

  inline GameObject(int a)
  : a{a}
  {
  }

  inline GameObject(const GameObject & other)
  {
    this->a = other.a;
  }

  int a;
};



int main()
{
  GameObject go0 = GameObject();
  GameObject go1 = GameObject(2);
  GameObject go2 = GameObject(go1);
  GameObject go3 = GameObject(go2);
}
```

### Operador de asignación

Es mejor que el operador de asignación use el constructor de copia a que repita código.

```cpp

// Esto sería lo que la lógica nos sugiere
// Si m_s es un puntero a un sprite

GameObject&
operator=(const GameObject &other) {
    *m_s = *other.m_s;

    // Y si m_s es null?
    // y si this y other son el mismo objeto?
}
```
Es mejor que usemos el constructor de copia.

```cpp
GameObject &
operator=(GameObject other) {
    // Intercambiamos los valores de spr y other puesto que other es temporal.
    std::swap(m_s, other.m_s);
}
```

## Copy Elision & Return Value Optimization

RVO & NRVO (Named Return Value Optimization)

```cpp

GameObject
create() {
    GameObject g; // NRVO
                  // La variable se crea en el stack de la función llamante y así se evita copiarla
                  // en la asignación

    return g;
}

GameObject g = create();
```

RVO:
- Ni const ni volatile
- No puede haber if return porque si no no puede optimizar

std::swap(x, y):    A veces hace 3 copias y 3 destroy, pero si puede hace move semantics.

## rvalue references

* rvalue reference
* move constructor
* move assigment operator

static no se pone en la implementación



```cpp

void swap (GameObject a, GameObject b) {
    GameObject tmp(a);   // a es un valor temporal, ergo le puedo robar los punteros.
    a = b;               // b es temporal tb.
    b = tmp;
}

```

A estos robos se le llama move semantics.


```cpp
const uint16_t &ra;      // No se puede hacer

const uint16_t &ra = a;  // Sí se puede hacer una referencia constante a un rvalue
const uint16_t &ra = 5;  // Sí se puede hacer una referencia constante a un rvalue

// El problema de los anteriores es que un método no puede distinguir si es una referncia a un rvalue o a un lvalue


GameObject &&rrf = create(1, 5, 6);
```

Si algo tiene nombre es un rvalue.
5 es un rvalue, pero ra es un lvalue que referencia un rvalue.

```cpp
uint16_t   &&rrf = 5;
GameObject &&rrf = create();

GameObject (const GameObject &other);              // Copy constructor
GameObject (GameObject &&other);                   // Move constructor

Gameobject &operator=(GameObject other);           // Si lo dejo así da ambigous operator overload
Gameobject &operator=(const GameObject &other);
Gameobject &operator=(GameObject &&other);


GameObject::GameObject( GameObject&& other)
    : m_id(gid++), m_s(other.m_s)
{
    other.m_s = nullptr;
}

GameObject &
operator=(const GameObject &other) { // Un rvalue no viene aquí si hay un parámetro sobrecardado rvalue
                                     // Si no la hubiera se le asignaría la referencia constante.
    GameObject g = other;            // Volvemos a llamar al constructor de copia, pero perdemos el exception warranty
    std::swap(m_s, g.m_s)
}

GameObject &
operator=(GameObject &&other) {  // En el operador de asignación hay dos objetos creados, en el constructor de copia se crea uno
    std::swap(m_s, other.m_s)
}

int main() {
    GameObject a;
    GameObject c( static_cast<GameObject &&> a); // Similar a std::move(a)

    c = static_cast<GameObject &&> a;   // Ambigous operator overload con la version 1 del operator=
                                        // El constructor de copia con su parámetro GameObject no puede
                                        // distinguir entre un lvalue o un rvalue
}
```

std::forward() cast condicional. Si viene de un lvalue lo dejo, si viene de rvalue hace el static_cast. Se usa en templates.

## Lambdas

```cpp
// Inside a class
auto lamb = [=] { return a + 2; }
```

`=` captures this by copy.  

Instead, as of c++17

```cpp
// Inside a class
auto lamb = [*this] { return a + 2; }
```

`=` captures the object by copy.  

## About UML

https://www.uml-diagrams.org  

## Move Semantics and perfect forwarding

https://drewcampbell92.medium.com/understanding-move-semantics-and-perfect-forwarding-987cf4dc7e27
https://drewcampbell92.medium.com/understanding-move-semantics-and-perfect-forwarding-part-2-6b8266b6cfa4
https://drewcampbell92.medium.com/understanding-move-semantics-and-perfect-forwarding-part-3-65575d523ff8


> An easy way to identify whether an expression returns an rvalue or an lvalue is to determine
> if you can take address of the value. If you can its an lvalue otherwise it’s an rvalue.

```cpp
const int& a;           // Impossible
      int& a = 5;       // Impossible
const int& a = 5;       // Possible. const lvalue can reference an rvalue
const int& a = b;       // Possible. lvalue is referencing an lvalue
      int& a = b;       // Possible
```

We can define a reference to an rvalue.

```cpp
int &&rvalueref = (x + 1); // rvalueref is an lvalue containing a rvalue reference.
```

rvalues live as long as their reference lives

```cpp
string Hello()
{
    return "The world is a vampire!";
}

string  &&rvalRef = Hello();
          rvalRef = "Hello";

std::cout << rvalRef << std::endl;  // Ouputs "Hello"
                                    // Notice:
                                    // 1. The rvalue was generated in the string
                                    // 2. rvalue has been modified
```

Temporary Objects are created:

1. Returning a value from a function (except RVO and NRVO)
1. Literals
1. Implicit conversions
1. Passing by value to a function

### Move Semantics

```cpp
std::move<go> == static_cast<GameObject &&> go;
```

move semantics is about stealing pointers.

```cpp
MyClass(MyClass&& other) // Move constructor
{

    _buffer = other._buffer;   // Stealing the array pointer.
                               // Notice buffer is a simple variable containing a pointer.
    _length = other._length;

    other._buffer = nullptr;   // Important: Don't leave the original object pointing to its data. Now is ours.
    other._length = 0;
}


MyClass b();              // b is disposable

MyClass a = std::move(b); // Equivalent to the next call
MyClass a(std::move(b));  // std::move forces calling the move constructor
                          // instead of the copy constructor.

```

`b` is going to be stolen in the move constructor.

> Therefore, it is wise to only use `std::move` on lvalues that we know aren’t going to get used anywhere else in
> the code, such as if they are local to the function in which a move constructor is being called.

But if the object point to other objects, such an string:

```cpp
MyClass(MyClass&& other) // Move constructor
{

    _buffer = other._buffer;
    _length = other._length;
    _name   = other._name;      // Warning other._name is an lvalue, therefore
                                // copy constructor is invoked over string instead
                                // of move constructor.

    // Thus, we shall write:
    _name   = std::move(other._name);

    other._buffer = nullptr;
    other._length = 0;
    other._name   = "";

}

```


### Perfect Forwarding. Templates.

Lets have a template function that forwards some parameters.


```cpp
template<typename T>
void outerFunction(T& param)
{
    innerFunction(param);
}
```

Since the function only accepts an lvalue reference, we cannot pass an rvalue.


```cpp
outerFunction(5);  // Error. rvalue.
```

So we'll have to overload the function.

```cpp
template<typename T>
void outerFunction(T&& param)
{
    innerFunction(param); // Problem: the compiler sees param as an lvalue
}
```

#### Template Type deduction

Rules:

1. When a lvalue is passed T is resolved as X&.
1. When a rvalue is passes T is resolved as X.

Example:


```cpp
int a = 5;
outerFunction(a);     // T is int&
outerFunction(5);     // T is int
```

#### Reference Collapsing

That leads as to inavertendly take a reference on a reference,


```cpp
template<typename T>
void funct(T t)
{
    T& k = t;
}

string hello = "Hello";
func(hello);             // T is resolved to string&.
                         // So what T& would be?
```

which will be collapsed by the compiler with the following rules:

1. reference of a lvalue reference        => lvalue reference (X& &   => X& )
1. rvalue reference of a lvalue reference => lvalue reference (X& &&  => X& )
1. lvalue reference of a rvalue reference => lvalue reference (X&& &  => X& )
1. rvalue reference of a rvalue reference => rvalue reference (X&& && => X&&)


### std::forward

1. Convert everything that is not an lvalue reference to an rvalue
1. Leaves lvalue references as they are.

This means that the innerFunction call can forward an rvalue or make a call to the overloaded
lvalue version inside a template.

### Universal references

Due to reference collapsing && not always implies rvalue reference. Thus, when type
deduction is involved, we call the universal references.

### Perfect forwarding

```cpp
class A
{
    public:
        A(std::string b) : b(b) {}

        // Copy constructor
        A(const A& other)
        {
            b = other.b;
            std::cout << "Copy constructor!" << std::endl;
        }

        // Move constructor
        A(A&& other)
        {
            b = std::move(other.b);
            std::cout << "Move Constructor" << std::endl;
        }


    private:
        std::string b;

};


// A template function
template<typenamen T>
void outerFunction(T&& param)
{
    A a(std::forward(param));
}

// Passing an lvalue
A a = A("Hello");
outerFunction(a);

// Passing an rvalue
outerFunction(A("World"));
```

## CRTP class

Curiously Recurring Template Pattern

A class is derived from a template class using itself as a parameter.
Also called F-bound polymorphism

Some uses are:

### static polymorphism

```cpp
template<class T>
struct Base
{
    void interface()
    {
        static_cast<T*>this->implementation();
    }

    static void static_func()
    {
        T::static_sub_func();
    }
};

struct Derived : Base<Derived>
{
    void implementation;
    static void static_sub_func();
};
```

### Object counter

To retrieve statistics of object creation and destruction.

```cpp
template<typename T>
struct counter
{
    static inline int objects_created = 0;
    static inline int objects_alive   = 0;

    counter()
    {
        ++objects_created;
        ++objects_alive;
    }

    counter(const counter&)
    {
        ++objects_created;
        ++objects_alive;
    }

    protected:

    ~counter()
    {
        --objects_alive;
    }

};


class X : counter<X>
{

};

class Y : counter<Y>

```

### Polymorphic chaining

Method chaining = named parameter idiom.

Problem: Method chaining loses concrete class in a hierarchy. As for instance:

```cpp
class Printer
{
    public:
        Printer(ostream& pstream) : m_stream(pstream) {}

        template <typename T>
        Printer &print(T&& t)    { m_stream << t; return *this; }
        Printer &println(T&& t)  { m_stream << t << std::endln; return *this; }
    private:
        ostream& m_stream;
};

// We can chain calls

Printer(myStream).println("Hello").println(500);

// But on a derived class

class coutPrinter : public Printer
{
    coutPrinter() : Printer(cout) {}
    coutprinter& setConsoleColor(Color c)
    {
        return *this;
    }
};

// In this call

coutprinter().println("Hello").setCoutColor(Color.red).println("Red!");
//                            ^
//                            L We got a problem over there since println has returned a Printer&
```

The CRTP pattern can solve this.

```cpp
// Base class
template<typename ConcretePrinter>
class Printer
{
    public:
        Printer(ostream& pstream) : m_stream(pstream) {}

        template<typename T>
        ConcretePrinter& print(T&& t)
        {
            m_stream << t;
            return static_cast<ConcretePrinter &>(*this);
        }

        template <typename T>
        ConcretePrinter& println(T&& t)
        {
            m_stream << t << endl;
            return static_cast<ConcretePrinter&>(*this);
        }

    private:
        ostream &m_stream;
};

// The base class is returning the concrete type.
```

### Polymorphic copy construction

When copying an object by its polymorphic pointer usually we define a virtual copy constructor, but CRTP are also a valid solution.

```cpp
// Base class
class AbstractShape
{
    public:
        virtual ~AbstractShape() = default;
        virtual std::unique_ptr<AbstractShape> clone() const = 0;
};

// This CRTP implements clone for Derived
template<typename Derived>
class Shape : public AbstractShape {
    public:
        std::unique_ptr<AbstractShape> clone const override {
            return std::make_unique<Derived>(static_cast<Derived const&>(*this));
        }

    protected:
        // We make clear Shape class needs to be inherited
        Shape() = default;
        Shape(const Shape&) = default;
        Shape(Shape &&) = default;
};

// Every derived class inherits from Shape instead of from AbstractShape;

class Square : public Shape<Square>{};
class Circle : public Shape<Circle>{};
```

## Forward declaration vs include

> Forward declare in .h, include in .cpp when possible.

As a rule try the forward declaration first. This will reduce compile times etc. If that doesn't compile go
for the `#include`. You have to go for the #include if you need to do any of the following:

1. Access a member or function of the class.
1. Use pointer arithmetic.
1. Use sizeof.
1. Any RTTI information.
1. new/delete, copy etc.
1. Use it by value.
1. Inherit from it.
1. Have it as a member.
1. Instance in a function.

## Ordenamiento topológico

Depende del algoritmo de búsqueda en profundidad.


