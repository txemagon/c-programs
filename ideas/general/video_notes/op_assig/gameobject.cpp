#include "gameobject.h"

GameObject::GameObject ()
{
    m_s = new Sprite;
}

/* Solution 1 */
GameObject&
GameObject::operator= (const GameObject& other) {
    /* Potential drawbacks of the direct approach */
    /* Don't use */
    *m_s = *other.m_s; // 2 Problems onter.m_s == NULL || m_s already ponting something
    // Sprite has inner pointers.

    /* other == this => will erase the sprite */

    if (this != &other) {
        if (m_s) {
            delete m_s;
            m_s = nullptr;
        }

        if (other.m_s)
            m_s = new Sprite {*other.m_s};
    }


    reutrn *this;
}


/* Solution 2: copy and swap idiom */
GameObject&
GameObject::GameObject (GameObject other) {
    std::swap (m_s, other.m_s);

    return *this;
}
