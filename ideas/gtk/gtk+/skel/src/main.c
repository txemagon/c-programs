#include <gtk/gtk.h>


/* DATA STRUCTURES */

typedef struct {
  // Add pointers to widgets to below
  // GtkWidget *w_x;
  // 
} app_widgets;




/* MAIN PROGRAM */

int main(int argc, char *argv[])
{
  /* VARIABLES */
    GtkBuilder      *builder; 
    GtkWidget       *window;
    app_widgets     *widgets = g_slice_new (app_widgets);

  
  
  /* INITIALITATION*/
    gtk_init(&argc, &argv);
  
  // Element loading
    builder = gtk_builder_new_from_file("glade/window_main.glade");

  // Retrieve pointers
    window = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));
    
  //widgets->w_ = GTK_WIDGET (gtk_builder_get_object (builder, ""));
  
  // Sew signals
    gtk_builder_connect_signals(builder, widgets);
  
  // Clean up the builder
    g_object_unref(builder);
  
  
  
  /* START THE APP*/
    gtk_widget_show(window);                
    gtk_main();

  
  /* HOUSEKEEPING */
    g_slice_free (app_widgets, widgets);
  
    return 0;
}



/* SIGNAL HANDLERS */
/*
void x(GtkButton *button, app_widgets *app_wdgts)
{
  
}
 */

void on_window_main_destroy()
{
    gtk_main_quit();
}
