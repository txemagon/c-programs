#include <gtk/gtk.h>


/* DATA STRUCTURES */

typedef struct {
  // Add pointers to widgets to below
  // GtkWidget *w_x;
  GtkWidget *w_dlg_file_choose;
  GtkWidget *w_img_main;
  GtkWidget *w_dlg_about;
} app_widgets;




/* MAIN PROGRAM */

int main(int argc, char *argv[])
{
  /* VARIABLES */
    GtkBuilder      *builder; 
    GtkWidget       *window;
    app_widgets     *widgets = g_slice_new (app_widgets);

  
  
  /* INITIALITATION*/
    gtk_init(&argc, &argv);
  
  // Element loading
    builder = gtk_builder_new_from_file("glade/window_main.glade");

  // Retrieve pointers
    window = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));
    
  //widgets->w_ = GTK_WIDGET (gtk_builder_get_object (builder, ""));
  widgets->w_dlg_file_choose = GTK_WIDGET (gtk_builder_get_object (builder, "dlg_file_choose"));
  widgets->w_img_main  = GTK_WIDGET (gtk_builder_get_object (builder, "img_main"));
  widgets->w_dlg_about = GTK_WIDGET (gtk_builder_get_object (builder, "dlg_about"));
  
  // Sew signals
    gtk_builder_connect_signals(builder, widgets);
  
  // Clean up the builder
    g_object_unref(builder);
  
  
  
  /* START THE APP*/
    gtk_widget_show(window);                
    gtk_main();

  
  /* HOUSEKEEPING */
    g_slice_free (app_widgets, widgets);
  
    return 0;
}



/* SIGNAL HANDLERS */
/*
void x(GtkButton *button, app_widgets *app_wdgts)
{
  
}
 */

void on_menutim_open_activate(GtkButton *button, app_widgets *app_wdgts)
{
  gchar *file_name = NULL;
  
  gtk_widget_show(app_wdgts->w_dlg_file_choose);
  
  if ( gtk_dialog_run (GTK_DIALOG (app_wdgts->w_dlg_file_choose)) == GTK_RESPONSE_OK) {
    file_name = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER(app_wdgts->w_dlg_file_choose));
    if (file_name != NULL)
      gtk_image_set_from_file (GTK_IMAGE(app_wdgts->w_img_main), file_name);
    g_free (file_name);
  }
  gtk_widget_hide (app_wdgts->w_dlg_file_choose);
  
}



void on_menuitm_close_activate(GtkMenuItem *menuitem, app_widgets *app_wdgts)
{
  gtk_main_quit ();
  
}



void on_menuitm_about_activate (GtkMenuItem *menuitem, app_widgets *app_wdgts)
{
  gtk_widget_show (app_wdgts->w_dlg_about);
}

void on_dlg_about_response (GtkDialog *dialog, gint response_id, app_widgets *app_wdgts)
{
  gtk_widget_hide (app_wdgts->w_dlg_about);
}

void on_window_main_destroy()
{
    gtk_main_quit();
}
