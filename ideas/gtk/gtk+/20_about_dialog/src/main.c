#include <gtk/gtk.h>


typedef struct {
  GtkWidget *w_txtvw_main;
  GtkWidget *w_dlg_file_choose;
  GtkTextBuffer *textbuffer_main;
  GtkWidget *w_dlg_about;
} app_widgets;


int main(int argc, char *argv[])
{
    GtkBuilder      *builder; 
    GtkWidget       *window;
    app_widgets     *widgets = g_slice_new (app_widgets);

    gtk_init(&argc, &argv);

    // builder = gtk_builder_new();
    // gtk_builder_add_from_file (builder, "glade/window_main.glade", NULL);
    // Update October 2019: The line below replaces the 2 lines above
    builder = gtk_builder_new_from_file("glade/window_main.glade");

    window = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));
  
    widgets->w_txtvw_main = GTK_WIDGET (gtk_builder_get_object (builder, "txtvw_main"));
    widgets->w_dlg_file_choose = GTK_WIDGET (gtk_builder_get_object (builder, "dlg_file_choose"));
    widgets->textbuffer_main = GTK_TEXT_BUFFER (gtk_builder_get_object (builder, "textbuffer_main"));
    widgets->w_dlg_about = GTK_WIDGET (gtk_builder_get_object (builder, "dlg_about")); 
  
    gtk_builder_connect_signals(builder, widgets);

    g_object_unref(builder);

    gtk_widget_show(window);                
    gtk_main();
  
    g_slice_free (app_widgets, widgets);

    return 0;
}


void on_menuitm_about_activate (GtkMenuItem *menuitem, app_widgets *app_wdgts)
{
  gtk_widget_show (app_wdgts->w_dlg_about);
}


void on_dlg_about_response (GtkDialog *dialog, gint response_id, app_widgets *app_widgts)
{
  gtk_widget_hide (app_widgts->w_dlg_about);
}


void on_menuitm_open_activate (GtkMenuItem *menuitem, app_widgets *app_wdgts)
{
  gchar *file_name      = NULL;
  gchar *file_contents  = NULL;
  gboolean file_success = FALSE;
  
  gtk_widget_show (app_wdgts->w_dlg_file_choose);
  
  if (gtk_dialog_run (GTK_DIALOG (app_wdgts->w_dlg_file_choose)) == GTK_RESPONSE_OK) {
    file_name = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (app_wdgts->w_dlg_file_choose));
    if (file_name != NULL) {
      file_success = g_file_get_contents (file_name, &file_contents, NULL, NULL);
      if (file_success)
        gtk_text_buffer_set_text (app_wdgts->textbuffer_main, file_contents, -1);
      g_free (file_contents);
   } 
      g_free (file_name);
  }
  
  gtk_widget_hide (app_wdgts->w_dlg_file_choose);
}




void on_menuitm_close_activate (GtkMenuItem *menuitem, app_widgets *app_wdgts)
{
  gtk_text_buffer_set_text (app_wdgts->textbuffer_main, "", -1);
}



void on_menuitm_quit_activate (GtkMenuItem *menuitem, app_widgets *app_wdgts)
{
  gtk_main_quit ();
}


// called when window is closed
void on_window_main_destroy()
{
    gtk_main_quit();
}
