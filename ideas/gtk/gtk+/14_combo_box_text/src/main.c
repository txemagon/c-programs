#include <gtk/gtk.h>

typedef struct {
  GtkWidget *w_comboboxtext_options;
  GtkWidget *w_lbl_sel_num;
  GtkWidget *w_lbl_sel_text;
} app_widgets;

int main(int argc, char *argv[])
{
    GtkBuilder      *builder; 
    GtkWidget       *window;
    app_widgets     *widgets = g_slice_new (app_widgets);

    gtk_init(&argc, &argv);

    // builder = gtk_builder_new();
    // gtk_builder_add_from_file (builder, "glade/window_main.glade", NULL);
    // Update October 2019: The line below replaces the 2 lines above
    builder = gtk_builder_new_from_file("glade/window_main.glade");

    window = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));
  
    widgets->w_comboboxtext_options = GTK_WIDGET (gtk_builder_get_object (builder, "comboboxtext_options"));
    widgets->w_lbl_sel_num  = GTK_WIDGET (gtk_builder_get_object (builder, "lbl_sel_num"));
    widgets->w_lbl_sel_text = GTK_WIDGET (gtk_builder_get_object (builder, "lbl_sel_text"));
  
  
  
    gtk_builder_connect_signals(builder, widgets);

    g_object_unref(builder);

    gtk_widget_show(window);                
    gtk_main();
  
    g_slice_free (app_widgets, widgets);

    return 0;
}

void on_btn_show_option_clicked (GtkButton *button, app_widgets *widgets)
{
  gint     item_num = 0;
  gchar   *item_text = 0;
  GString *gstr_sel_text;
  
  gstr_sel_text = g_string_new ("");
  
  // ----- Item Number -----
  item_num = gtk_combo_box_get_active (GTK_COMBO_BOX (widgets->w_comboboxtext_options));
  g_string_printf (gstr_sel_text, "Item: %i", item_num);
  gtk_label_set_text (GTK_LABEL (widgets->w_lbl_sel_num), gstr_sel_text->str);
  g_string_free (gstr_sel_text, TRUE);
  
  
  // ----- Item Text -----
  item_text = gtk_combo_box_text_get_active_text (GTK_COMBO_BOX_TEXT (widgets->w_comboboxtext_options));
  if (item_text == NULL)
    return;
  
  gtk_label_set_text (GTK_LABEL (widgets->w_lbl_sel_text), item_text);
  g_free (item_text);
}

// called when window is closed
void on_window_main_destroy()
{
    gtk_main_quit();
}
