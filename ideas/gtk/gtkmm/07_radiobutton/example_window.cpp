#include "example_window.h"
#include <iostream>

ExampleWindow::ExampleWindow () :
    m_box_top (Gtk::ORIENTATION_VERTICAL),
    m_box_1   (Gtk::ORIENTATION_VERTICAL, 10),
    m_box_2   (Gtk::ORIENTATION_VERTICAL, 10),
    m_rb1     ("button1"),
    m_rb2     ("button2"),
    m_rb3     ("button3"),
    m_button_close ("Close")
{

    /* APPEARANCE */

    // Set title and border of the window
    set_title ("Radio Buttons");
    set_border_width (0);

    // Set rb group
    m_rb2.join_group (m_rb1);
    m_rb3.join_group (m_rb2);

    // Packing
    add (m_box_top);
    m_box_top.pack_start (m_box_1);
    m_box_top.pack_start (m_separator);
    m_box_top.pack_start (m_box_2);

    // Set borders
    m_box_1.set_border_width (10);
    m_box_2.set_border_width (10);

    // Set radio buttons
    m_box_1.pack_start (m_rb1);
    m_box_1.pack_start (m_rb2);
    m_box_1.pack_start (m_rb3);

    //Set the second active
    m_rb2.set_active ();

    // Button
    m_box_2.pack_start (m_button_close);
    m_button_close.set_can_default ();
    m_button_close.grab_default ();


    /* SIGNALS */

    m_button_close.signal_clicked ().connect (sigc::mem_fun (*this,
                &ExampleWindow::on_button_clicked));

    /* SHOW */
    show_all_children ();
}

ExampleWindow::~ExampleWindow ()
{

}


void
ExampleWindow::on_button_clicked ()
{
    hide (); // To close the application
}
