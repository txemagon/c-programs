#include <stdio.h>
#include <stdlib.h>

bool es_borde (int f, int c, int l) {
    return f == 0 || f == l-1 || c == 0 || c == l-1;
}

int main (int argc, char *argv[]) {

    int l;

    printf ("Lado: ");
    scanf ("%i", &l);

    for (int f=0; f<l; f++) {
        for (int c=0; c<l; c++)
            if ( es_borde(f, c, l))
                printf ("*");
            else
                printf (" ");

        printf ("\n");
    }

    printf ("\n");

    return EXIT_SUCCESS;
}
