#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    int l;

    printf ("lado: ");
    scanf ("%i", &l);

    // Pintar cada fila
    for (int f=0; f<l; f++) {

        // Pintar todas las col de a
        for (int c=0; c<l; c++)
            printf ("a");

        // Pintar todas las col de b
        for (int c=0; c<l; c++)
            printf ("b");

        // Pintar un salto de linea
        printf ("\n");

    }

    printf ("\n");

    return EXIT_SUCCESS;
}
