#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

#define MAXPAL  0x100
#define PAL     5

int main (int argc, char *argv[]) {

    /* DECL. VARIABLES */
    unsigned len;
    char buff[MAXPAL];
    char *lista[PAL];

    /* INICIALIZACIÓN */

    /* ENTRADA DE DATOS */
    for (unsigned i=0; i<PAL; i++) {
        printf ("Palabra: ");
        scanf (" %[^\n]", buff);
        __fpurge (stdin);
        len = strlen (buff) + 1;
        lista[i] = (char *) malloc (len);
        strcpy (lista[i], buff);
    }

    /* CÁLCULOS */

    /* SALIDA DE DATOS */
    printf ("\n");
    for (unsigned i=0; i<PAL; i++)
        printf ("%i.- %s\n", i+1, lista[i]);
    printf ("\n");

    /* LIBERACIÓN */
    for (unsigned i=0; i<PAL; i++)
        free (lista[i]);


    return EXIT_SUCCESS;
}
