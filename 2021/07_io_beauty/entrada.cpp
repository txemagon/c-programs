#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

#define   MAX   0x20


int main (int argc, char *argv[]) {

    char nombre[MAX];

    printf ("Nombre: ");
    scanf  (" %s", nombre);
    printf ("Hi, %s\n", nombre);

    __fpurge (stdin);

    printf ("Nombre: ");
    fgets (nombre, MAX, stdin);

    int ultima = strlen (nombre);
    nombre[ultima-1] = '\0';


    printf ("Hi, %s\n", nombre);

    printf ("Nombre (yo=<nombre>): ");
    scanf (" yo = %s", nombre);
    printf ("%s\n", nombre);


    printf ("Dos palabras: ");
    scanf (" %*s %s", nombre);
    printf ("Segunda: %s\n", nombre);

    return EXIT_SUCCESS;
}
