# Memoria de vídeo

## Estructura del hardware

RAM C000-FFFF
CRTC (Cathode Ray Tube Controller) Lee de C000
1 Byte = 4pixels
Refresco de pantalla 50 Hz
Flickering (pintar una nueva pantalla en mitad del scan)


## Formatos de Pixeles


C000  6D    0110 1101 => 01 10 11 01

2bits/pixel
1byte = 4pixel
4colores/pixel

16Kb Memoria de vídeo


### Formato AMSTRAD

#### Modo 1

00  Azul
01  Amarillo
10  Cian
11  Rojo


7 6 5 4 3 2 1 0
0 1 0 1 0 0 1 1

bits
7,3   00  Azul obscuro
6,2   10  Amarillo         (Notación de punta delgada)
5,1   01  Cian
0,0   11  Rojo


Nota: Los azules empiezan por 0. Bit alto
      Cian y rojo ponen a 1 el bit bajo.


## Formato de pantalla

    Normal
Mode 0: 160x200. 16c (4bpp)
Mode 1: 320x200. 4c  (2bpp)
Mode 2: 640x200. 2c  (1bpp)
Mode 3: 160x200. 4c  (2bpp) Not official

Overscan
Mode 0: 192x272. 16c (4bpp)
Mode 1: 384x272. 4c  (2bpp)
Mode 2: 768x272. 2c  (1 bpp)


## Distribución de Vídeo


### Modo 1

25x40 caracteres de 8x8 => 200x320 => 320x200

1byte = 4pixels

Línea: 320 pixeles / 4pixel/byte = 80 bytes
Una pantalla de primeras líneas = 80 bytes x 25 líneas = 2000 bytes

Se pierden 48 bytes = 12 pixels

12x25 = 300 pixeles perdidos en una imagen
12x48 = 1200 bytes perdidos en una imagen

Último byte visible: 320 / 2 = 1 0100 0000 /10 = 140h / 2 = 80 = 50h

 0: 000h
 1: 050h
 2: 0A0h
 3: 0F0h
 4: 140h
 5: 190h
 6: 1E0h
 7: 230h
 8: 280h
 9: 2D0h
10: 320h
...
19: 5F0h
20: 640h
..
24: 780h
25: 7D0h

800h - 7D0h = 30h = 48 bytes perdidos



Se le asigna a cada pantalla de primeras línas: 2kb = 2048 bytes

2048 bytes = 1000 0000 0000 = 800h

C000
C800
D000
D800
...



Memoria lineal


Ejemplo:

```assembly
;todo hacer un ejemplo
```

## Interrupciones de vídeo


┌──────────┐
│          │                                    ┌──────────┐
│          │                                    │          │
│          ├───────────────────────────────────►│   CRTC   │
│  Video   │                                    │          │
│          │                                    └─────▲────┘
├──────────┤                                          │
│          │                                          │
│          │                                          │
│          │                                          │
│          │                                          │
│          │                                          │
│          │         ┌─────┐                 ┌────────▼───────┐
│          │         │     │                 │                │
│          ◄─────────► Z80 ◄─────────────────►   GATE ARRAY   │
│   RAM    │         │     │                 │                │
│          │         └─────┘                 └────────────────┘
│          │
│          │
└──────────┘

Gate Array: Genera interrupciones

CRTC:

Contador de líneas. Al llegar a 52 (= 312 / 6 ) => Notifica al Gate Array (que produce una interrupción)
El procesador puede contar el tiempo con esa interrupción.

76    HALT  ;Párate

50 Hz * 6 = 300 Interrupciones/seg

300 HALT = 1s
3 HALT   = 0.01s

```assembly
;Parpadeo

4000    3E FF         LD    A,#FF
4002    32 00 C0      LD    (#C000),A
4005    76            HALT
4006    76            HALT
4007    76            HALT
4008    3E 00         LD    A,#00
400A    32 00 C0      LD    (#C000),A
400D    76            HALT
400E    76            HALT
400F    76            HALT
4010    18 EE         JR    #4000

```

Piensa por qué no parpadea.
Haz que parpadee.

## Raster Scan

Parpadeo: Flickering

No modificar hasta que no haya pasado el haz de electrones.

Retrazado horizontal
Retrazado Vertical


