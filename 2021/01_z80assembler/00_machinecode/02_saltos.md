## SALTOS INCONDICIONALES

### Relativos

18 F0       jr F0

## SALTOS CONDICIONALES

            jp F00F

### Relativos

20 F0       jr nz, F0
30 F0       jr nc, F0
28 0F       jr z,  0F
38 0F       jr c,  0F

### Absolutos
C2 0F F0    jp nz, F00F   ; No Zero
D2 0F F0    jp nc, F00F   ; No carry
CA 0F F0    jp z,  F00F   ; Zero
DA 0F F0    jp c,  F00F   ; Carry
F2 0F F0    jp p,  F00F   ; Positive
FA 0F F0    jp m,  F00F   ; Negative


## Bucles

DJNZ

      ld  b,15
loop:
      ; Do whatever
      djnz loop

## Ejemplo

4000  21 00 C0      LD   HL , #C000
4003  3E FF         LD    A , #FF
4005  06 10         LD    B , #10
4007  77            LD  (HL), A
4008  23            INC  HL
4009  05            DEC   B
400A  20 FB         JR   NZ , #4000
400C  3D            DEC   A
400D  FA 05 40      JP    M , #4005
4010  18 FE         JR  #4010
