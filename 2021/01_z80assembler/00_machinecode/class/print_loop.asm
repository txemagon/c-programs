PRINT_CHAR	equ	&BB5A
NDIGITS		equ	10


;;;;;;;;;;;;;;;;;;;;;
;; LOOP DEMO
;;;;;;;;;;;;;;;;;;;;;
org &8000



	;; INITIALIZATION
	ld	b,NDIGITS	;; Digit count
	ld	a,(char)	;; Initial '0'

	;; BLOCK
next_digit:
	call	PRINT_CHAR
	inc	a		;; Adv to next digit

	;; INCREMENT
	dec	b		;; Account prints
	
	;; CONDITIONrun
	jr 	nz,next_digit	;; Loop if no zero

	ret

char:
	db 	&30