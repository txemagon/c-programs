org &8000

_main:




	ret


texto:
	db	'H','o','l','a',00



numerazo:				;; int numerazo = 0x400C09F
	dw	&C09F,&0400  		;; 400C09F

user_data:
	db	00,00,00,00		;; int user_data


;; ARRAY o MATRIZ o VECTOR
nombre:					;; (nombre: &9000)
	db    	00,97,00,00,00,'c'	;; char nombre[6]
;;		^      ^       ^
;;		|      |       L--- nombre[5] ;; Notacion de matrices
;;		|      L---nombre[2]
;;		L----- nombre[0]

num:
	db 	03			;; nombre[6] se carga esta variable

;; NOTACION DE PUNTEROS
;;
;; nombre 	= 800E  // *nombre => nombre[0]
;; nombre + 0 	= 800E  // *(nombre + 0) => nombre[0] == 00
;; nombre + 1 	= 800F  // *(nombre + 1) => nombre[1] == 'a' == 97
;; nombre + 2 	= 8010
;; nombre + 3 	= 8011
;; nombre + 4 	= 8012
;; nombre + 5 	= 8013

;; ld hl, nombre 	;; nombre = &800E
;; ld  a,(nombre)	;; a = *nombre