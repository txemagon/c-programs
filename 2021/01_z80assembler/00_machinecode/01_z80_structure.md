# ESTRUCTURA Z80

## Flags
SZ-H-VNC

S: Sign
Z: Zero
H: Half Carry
P/V: Parity / Overflow
N: Add / Subtract
C: Carry

## Registros

### Registros Primarios
A, B, C, D, E, H, L

A: Acumulador
HL: Direcciones de memoria
BC & DE: Auxiliares de HL (LDI, LDIR)

### Registros ALternativos
Copia de los primarios (para aumentar la velocidad con las interrupciones)

### Registros especiales
I, R, IX, IY, SP, PC

IX & IY: Para copiar datos. 
SP: Stack Pointer
PC: Program Counter
