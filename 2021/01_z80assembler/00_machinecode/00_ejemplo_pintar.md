```
4000  21 00 C0      LD    HL,#C000
4003  36 FF         LD    (HL), #FF
4005  23            INC   HL
4006  36 FF         LD    (HL), #FF
4008  23            INC   HL
400A  18 FE         JR   #400A
```

##################
# Bucle infinito #
##################

# Dirección inicial
```
21 00 C0
```
# Pintar
```
36 FF
23
```

# Saltar
```
18 FB
```



################
# Bucle finito #
################

# Dirección inicial
```
21 00 C0
```

# Contar desde 6 en ah
```
3E 06
```
# Pintar
```
36 FF
23
```

# Descontar
```
3D
```

# Saltar si
```
20 FA
```

```asm
4000  21 00 C0    LD HL,#C000
4003  3E 06       LD A,#06
4005  36 FF       LD (HL),#FF
4007  23          INC HL
4008  3D          DEC A
4009  20 FA       JR  NZ,#4005
400B  18 FE       JR  #400B
```



