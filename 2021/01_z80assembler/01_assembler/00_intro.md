# Introducción

Recomiendo ver los vídeos del profesor Retroman, de donde se han extraído estos apuntes.

Escribimos en un Fichero .s

```assembly
.db 0x3E, 0x88, 0x32, 0x00, 0xC0, 0x18, 0xFE
```

```assembly
.db 0x3E, 0x88
.db 0x32, 0x00, 0xC0
.db 0x18, 0xFE
```

CPCTelera
sdasz80: Ensamblador

```bash
$ alias asz80="$CPCT_PATH/tools/sdcc-3.6.8-r9946/bin/sdasz80"
```

```bash
$ asz80 -l -o -s game.s
```

| Fichero | Significado |
|---------|-------------|
|.lst     |Listado con las traducciones.|
|.rel     |Pseudo binario (como el .o) <br/>
           Línea T: Traducción. <br/>
           Línea R: Símbolos Rellocatable.|
|.sym    Tabla de símbolos|


Ahora toca compilar y linkar

```bash
$ alias sdcc="$CPCT_PATH/tools/sdcc-3.6.8-r9946/bin/sdcc"
```

```bash
$ sdcc game.rel
```

Buscaría `_main`. `_` identificadores globales.

Posición de carga + Posición de ejecución.

```assembly
_main::
.db 0x3E, 0x88
.db 0x32, 0x00, 0xC0
.db 0x18, 0xFE
```

es equivalente a:

```assembly
.globl _main
_main:
.db 0x3E, 0x88
.db 0x32, 0x00, 0xC0
.db 0x18, 0xFE
```

Ahora se puede volver a ensamblar y linkar.

Aparecen los ficheros:

```
.lk     Linkado
.ihx    Binario resultante
.map    Símbolo (aparece el módulo crt0)
.noi    Símbolos
```

Compilar para z80 y sin el crt0

```bash
$ sdcc -mz80 --no-std-crt0 game.rel
```

```
Warning
ASlink-Warning_No definition of area __DATA
```
main en 200

```bash
$ sdcc -mz80 --no-std-crt0 --code-loc 0x4000 game.rel
```

Ahora hay que pasar di ihx a binario.


```bash
$ alias hex2bin="$CPCT_PATH/tools/hex2bin-2.0/"
$ hex2bin -p 00 game.ihx # Padding byte 00
```

Ya hay un .bin

Ahora se puede pasar a cinta para que lo cargue el winape:

```bash
$ rm game.cdt && cpc2cdt -r MYLUCKYGAME -l 0x4000 -x 0x4000 game.bin game.cdt
```


## Resumen Multiples ficheros

f1.s -> asz80 -> f1.rel  \
f2.s -> asz80 -> f2.rel  |> sdcc -> IHX -> BIN -> CDT (DSK)
f2.s -> asz80 -> f3.rel  /


includes => para que los otros ficheros sepan qué identificadores hay en otros ficheros.



