;;;;;;;;;;;;;;;
;; MACROS
;;;;;;;;;;;;;;;

; Son substituciones que se realizan antes de ensamblar.
.macro  drawPixel DIR, COL
	ld		a, #COL
	ld		(DIR), a
.endm

;;;;;;;;;;;;;;;
;; Constantes
;;;;;;;;;;;;;;;

; No dejan código.
; Se sustituyen en el proceso de ensamblado

color = 0x88
vmem  = 0xC000
_main::
        drawPixel       vmem, color
	jr 		.				; La dirección de mem antes de traducir esta línea
;;asz80 -l -o -s main.s
