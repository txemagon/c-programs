#include <stdio.h>
#include <stdlib.h>


#define M 10U

typedef unsigned tipo;

tipo get (tipo m[M][M], int f, int c) {
    if (f < 0 || c < 0)
        return 0U;

    if (f >= (int) M || c >= (int) M)
        return 0U;

    return m[f][c];
}

void rellenar (tipo mapa[M][M], tipo dim) {
    mapa[0][0] = 1;
    for (tipo f=1; f<dim; f++)
        for (tipo c=0; c<dim; c++)
            mapa[f][c] = get (mapa, f-1, c-1) + get (mapa, f-1, c);
}


void pintar (tipo mapa[M][M], tipo dim) {
    for (tipo f=0; f<dim; f++) {
        printf ("\t");
        for (tipo c=0; c<dim; c++)
            if (mapa[f][c] != 0)
                printf ("%4u", mapa[f][c]);
        printf ("\n");
    }
    printf ("\n");
}

void title () {

    system ("clear");
    system ("toilet -Fborder -fpagga --metal 'TARTAGLIA'");
    printf ("\n\n");
}


int main (int argc, char *argv[]) {

    tipo tartaglia[M][M];

    title ();

    rellenar (tartaglia, M);
    pintar (tartaglia, M);


    return EXIT_SUCCESS;
}
