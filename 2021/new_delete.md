# Memoria Dinámica en C++


Sin llamar al constructor:

```cpp
int *p = NULL; 		// Declaración del puntero
p = new int;		// Inicialización. - Se puede hacer todo de una.

delete p;		// Equivalente a free, pero llama al destructor.
```

Llamando al constructor:

```cpp
int *p = new int(25);	// Se llama al constructor para asignar valor inicial.

delete p;
```


Para crear un array:

```cpp
int *p = new int[10];	// Alojar espacio para 10 enteros.

delete [] p;		// Free para arrays.
```

Las llamadas a new lanzan una excepción si no hay memoria suficiente, salvo que lo indiquemos.

```cpp
int *p = new(nothrow) int;
```

Si queremos que new no haga malloc, sino que se aloje la variable en un espacio
previamente reservado podemos usar __placement new__.

```cpp
unsigned char buf[sizeof(int)*2] ;	// Celdas byte para que quepan 2 enteros (8 bytes).

// placement new in buf
int *pInt = new (buf) int(3);		// Alojar un 3 entero en las primeras cuatro celdas.
int *qInt = new (buf + sizeof (int)) int(5); 	// Alojar un entero en las siguientes cuatro.


// No se necesita delete porque buf es un array estático.
```
