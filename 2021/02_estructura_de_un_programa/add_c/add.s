	.file	"add.cpp"
	.intel_syntax noprefix
	.text
	.globl	_Z4sumaii
	.type	_Z4sumaii, @function
_Z4sumaii:
.LFB0:
	.cfi_startproc
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	mov	rbp, rsp
	.cfi_def_cfa_register 6
	mov	DWORD PTR [rbp-4], edi
	mov	DWORD PTR [rbp-8], esi
	mov	edx, DWORD PTR [rbp-4]
	mov	eax, DWORD PTR [rbp-8]
	add	eax, edx
	pop	rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	_Z4sumaii, .-_Z4sumaii
	.section	.rodata
.LC0:
	.string	"%s"
	.text
	.globl	main
	.type	main, @function
main:
.LFB1:
	.cfi_startproc
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	mov	rbp, rsp
	.cfi_def_cfa_register 6
	sub	rsp, 48
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rbp-8], rax
	xor	eax, eax
	mov	DWORD PTR [rbp-36], 0
	movabs	rax, 7815279607914982469
	mov	QWORD PTR [rbp-32], rax
	movabs	rax, 2338323821517627764
	mov	QWORD PTR [rbp-24], rax
	mov	WORD PTR [rbp-16], 2608
	mov	BYTE PTR [rbp-14], 0
	mov	esi, 3
	mov	edi, 6
	call	_Z4sumaii
	mov	DWORD PTR [rbp-36], eax
	mov	eax, DWORD PTR [rbp-36]
	add	eax, 48
	mov	BYTE PTR [rbp-16], al
	lea	rax, [rbp-32]
	mov	rsi, rax
	mov	edi, OFFSET FLAT:.LC0
	mov	eax, 0
	call	printf
	mov	eax, 0
	mov	rdx, QWORD PTR [rbp-8]
	xor	rdx, QWORD PTR fs:40
	je	.L5
	call	__stack_chk_fail
.L5:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
