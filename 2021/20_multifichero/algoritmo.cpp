#include <stdio.h>
#include <stdlib.h>

#include "algoritmo.h"

bool check_hztal(unsigned t[N][N], unsigned fila, enum TDato jugador) {
    unsigned contador = 0U;

    if (fila >= N ){
        fprintf(stderr, "check_hztal: Invalid row %u.\n", fila);
        exit( OUT_OF_BOUNDS );
    }

    for (unsigned i=0; i<N; i++)
        if (t[fila][i] == jugador)
            contador++;

    return contador == N;
}

bool check_vtcal(unsigned t[N][N], unsigned col, enum TDato jugador) {
    unsigned contador = 0U;

    if (col >= N ){
        fprintf(stderr, "check_hztal: Invalid col %u.\n", col);
        exit( OUT_OF_BOUNDS );
    }

    for (unsigned i=0; i<N; i++)
        if (t[i][col] == jugador)
            contador++;

    return contador == N;
}

