	.file	"algoritmo.cpp"
	.text
	.section	.rodata
.LC0:
	.string	"check_hztal: Invalid row %u.\n"
	.text
	.globl	_Z11check_hztalPA3_jj5TDato
	.type	_Z11check_hztalPA3_jj5TDato, @function
_Z11check_hztalPA3_jj5TDato:
.LFB15:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movl	%esi, -28(%rbp)
	movl	%edx, -32(%rbp)
	movl	$0, -8(%rbp)
	cmpl	$2, -28(%rbp)
	jbe	.L2
	movq	stderr(%rip), %rax
	movl	-28(%rbp), %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf@PLT
	movl	$3, %edi
	call	exit@PLT
.L2:
	movl	$0, -4(%rbp)
.L5:
	cmpl	$2, -4(%rbp)
	ja	.L3
	movl	-28(%rbp), %edx
	movq	%rdx, %rax
	addq	%rax, %rax
	addq	%rdx, %rax
	salq	$2, %rax
	movq	%rax, %rdx
	movq	-24(%rbp), %rax
	addq	%rax, %rdx
	movl	-4(%rbp), %eax
	movl	(%rdx,%rax,4), %eax
	cmpl	%eax, -32(%rbp)
	jne	.L4
	addl	$1, -8(%rbp)
.L4:
	addl	$1, -4(%rbp)
	jmp	.L5
.L3:
	cmpl	$3, -8(%rbp)
	sete	%al
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15:
	.size	_Z11check_hztalPA3_jj5TDato, .-_Z11check_hztalPA3_jj5TDato
	.section	.rodata
.LC1:
	.string	"check_hztal: Invalid col %u.\n"
	.text
	.globl	_Z11check_vtcalPA3_jj5TDato
	.type	_Z11check_vtcalPA3_jj5TDato, @function
_Z11check_vtcalPA3_jj5TDato:
.LFB16:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movl	%esi, -28(%rbp)
	movl	%edx, -32(%rbp)
	movl	$0, -8(%rbp)
	cmpl	$2, -28(%rbp)
	jbe	.L8
	movq	stderr(%rip), %rax
	movl	-28(%rbp), %edx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf@PLT
	movl	$3, %edi
	call	exit@PLT
.L8:
	movl	$0, -4(%rbp)
.L11:
	cmpl	$2, -4(%rbp)
	ja	.L9
	movl	-4(%rbp), %edx
	movq	%rdx, %rax
	addq	%rax, %rax
	addq	%rdx, %rax
	salq	$2, %rax
	movq	%rax, %rdx
	movq	-24(%rbp), %rax
	addq	%rax, %rdx
	movl	-28(%rbp), %eax
	movl	(%rdx,%rax,4), %eax
	cmpl	%eax, -32(%rbp)
	jne	.L10
	addl	$1, -8(%rbp)
.L10:
	addl	$1, -4(%rbp)
	jmp	.L11
.L9:
	cmpl	$3, -8(%rbp)
	sete	%al
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE16:
	.size	_Z11check_vtcalPA3_jj5TDato, .-_Z11check_vtcalPA3_jj5TDato
	.ident	"GCC: (Ubuntu 10.3.0-1ubuntu1~20.10) 10.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
