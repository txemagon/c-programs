#ifndef __ALGORITMO_H__
#define __ALGORITMO_H__

#include "general.h"


#ifdef __cplusplus
extern "C" {
#endif
  bool check_hztal(unsigned t[N][N], unsigned fila, enum TDato jugador);
  bool check_vtcal(unsigned t[N][N], unsigned col, enum TDato jugador);
#ifdef __cplusplus
}
#endif

#endif
