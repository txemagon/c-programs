#include "pila.h"

#include <strings.h>

void init (struct TStack *ps) {
    bzero (ps, sizeof (struct TStack));
}

bool push (struct TStack *ps, Tipo data) {
    if (ps->summit >= N)
        return false;

    ps->storage[ps->summit++] = data;
    return true;
}

Tipo pop  (struct TStack *ps) {
    if (ps->summit == 0)
        return (Tipo) -1;

    return ps->storage[--ps->summit];
}


