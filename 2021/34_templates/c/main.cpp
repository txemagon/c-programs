#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "pila.h"

void inspect (const struct TStack *p) {
    printf ("\n\n");
    printf ("Cima: %u\n", p->summit);
    printf ("DATA:\n");

    for (unsigned i=0; i<p->summit; i++)
        std::cout << "\t" << p->storage[i]<< "\n";
    printf ("\n\n");

    printf ("Pulsa intro para continuar");
    getchar ();

}

int main (int argc, char *argv[]) {

    double dato;
    struct TStack pila;

    init (&pila);

    push (&pila, 4.1);
    inspect (&pila);
    push (&pila, 8.3);
    inspect (&pila);

    dato = pop (&pila);
    std::cout << "Dato: " << dato <<"\n";
    inspect (&pila);

    push (&pila, 9);
    inspect (&pila);
    push (&pila, 8);
    inspect (&pila);

    return EXIT_SUCCESS;
}
