#ifndef __PILA_H__
#define __PILA_H__

#define N   0x08

typedef double Tipo;

struct TStack {
    Tipo storage[N];
    unsigned summit;
};

#ifdef __cplusplus
extern "C" {
#endif
    void init (struct TStack *ps);
    bool push (struct TStack *ps, Tipo data);
    Tipo pop  (struct TStack *ps);
#ifdef __cplusplus
}
#endif

#endif
