#ifndef __PILA_H__
#define __PILA_H__

#define N 0x08

// #define NDBG


/*   DEFINITION   */

template <class Tipo>
struct TStack {
	private:
	  unsigned summit;
	  Tipo     storage[N];

      void inspect () const;

	public:
	  TStack ();
      bool push (Tipo data);
      Tipo pop  ();


};



/*   IMPLEMENTATION   */




// Strategy 1
/*
	template class TStack <unsigned>;
	template class TStack <double>;
*/


// Strategy 2

#include <stdio.h>
#include <iostream>


template <class Tipo>
TStack<Tipo>::TStack () : summit (0) {};

template <class Tipo>
bool
TStack<Tipo>::push (Tipo data) {
    if (this->summit >= N)
        return false;

    this->storage[this->summit++] = data;

    this->inspect ();

    return true;
}

template <class Tipo>
Tipo
TStack<Tipo>::pop  () {
    if (this->summit == 0)
        return (Tipo) -1;

    this->inspect ();

    return this->storage[--this->summit];
}


template <class Tipo>
void
TStack<Tipo>::inspect () const {
    printf ("\n\n");
    printf ("Cima: %u\n", this->summit);
    printf ("DATA:\n");

    for (unsigned i=0; i<this->summit; i++)
        std::cout << "\t" << this->storage[i]<< "\n";
    printf ("\n\n");

    printf ("Pulsa intro para continuar");
    getchar ();

}


#endif