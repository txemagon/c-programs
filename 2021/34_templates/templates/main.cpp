#include <cstdlib>
#include <iostream>

#include "pila.h"

int main (int argc, char *argv[]) {

    double dato;
    struct TStack<double> pila;


    pila.push (4.1);
    pila.push (8.3);

    dato = pila.pop ();
    std::cout << "Dato: " << dato <<"\n";
    
    pila.push (9);
    pila.push (8);
    

    return EXIT_SUCCESS;
}
