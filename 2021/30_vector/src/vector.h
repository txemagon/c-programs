
#ifndef VECTOR_H
#define VECTOR_H


/**
  * class Vector
  * 
  */

class Vector
{
public:
    Vector();
    Vector(double x, double y, double z);
    Vector(const Vector &src);

    virtual ~Vector();

   

    double x;
    double y;
    double z;



    Vector    operator+    (const Vector &op1 )     const;
    Vector    operator*    (double landa      )     const;
    double    operator*    (const Vector & op1)     const;
    Vector    operator^    (const Vector & op1)     const;


};

#endif // VECTOR_H
