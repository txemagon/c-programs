#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "vector.h"

#define N 2


double preguntar_coord (const char *que) {
    double leido;

    std::cout << que << ": ";
    scanf ( " %lf", &leido );

    return leido;
}

void preguntar (const char *title, Vector &v) {
    double x, y, z;

    std::cout << title << "\n=======" << "\n\n";

    x = preguntar_coord ("x");
    y = preguntar_coord ("y");
    z = preguntar_coord ("z");

    v.x = x;
    v.y = y;
    v.z = z;

    std::cout << "\n\n";
}

void imprimir (const Vector &v) {
    std::cout << "( " << v.x << ", " << v.y << ", " << v.z << " ) ";
}

int main (int argc, char *argv[]) {


    Vector v[N];

    preguntar ("Vector 1", v[0]);
    preguntar ("Vector 2", v[1]);

    imprimir (v[0]);
    std::cout << " * ";
    imprimir (v[1]);
    std::cout << " = ";
    std::cout << v[0] * v[1];

    // imprimir ( Vector (v[0] ^ v[1]) );

    std::cout << "\n\n";


    return EXIT_SUCCESS;
}
