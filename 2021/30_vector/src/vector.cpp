#include "vector.h"

// Constructors/Destructors
//  

Vector::Vector () : x(0), y(0), z(0) {}
Vector::Vector(double x, double y=0, double z=0) : x(x), y(y), z(z) {}

Vector::Vector (const Vector &src) : x(src.x), y(src.y), z(src.z) {}

Vector::~Vector()
{
}

 
/**
 * @return Vector
 * @param  op1
 */
Vector Vector::operator+(const Vector &op1) const
{
    return Vector (
        this->x + op1.x, 
        this->y + op1.y,
        this->z + op1.z
    );
}


/**
 * Escalar por vector
 * @return Vector
 * @param  landa
 */
Vector Vector::operator*(double landa) const
{
    return  Vector ( landa *  this->x, landa * this->y, landa * this->z);
}


/**
 * @return double
 * @param  op1
 */
double Vector::operator*(const Vector & op1) const
{
    return this->x * op1.x + this->y * op1.y + this->z * op1.z;
}


/**
 * Producto vectorial de dos vectores
 * @return Vector
 * @param  op1
 */
Vector Vector::operator^(const Vector & op1) const
{
    return Vector (
       this->y * op1.z - this->z * op1.y,
       this->z * op1.x - this->x * op1.z,
       this->x * op1.y - this->y * op1.x
    );
}



