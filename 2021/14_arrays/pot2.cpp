#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define N 7

int main (int argc, char *argv[]) {

    FILE *pf;
    long int A[N][N];

    pf = fopen ("pot.csv", "w");

    for (int f=0; f<N; f++)
        for (int c=0; c<N; c++)
            A[f][c] = (long int) pow(c, f+1);

    for (int f=0; f<N; f++) {
        for (int c=0; c<N; c++){
            fprintf (pf, "%11li", A[f][c]);
            if (c != N-1)
                fprintf (pf, ",");
        }
        fprintf(pf, "\n");
    }

    fclose (pf);

    return EXIT_SUCCESS;
}
