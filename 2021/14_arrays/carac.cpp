#include <stdio.h>
#include <stdlib.h>

#define N 10

int main (int argc, char *argv[]) {

    int A[N][N];

    for (int f=0; f<N; f++)
        for (int c=0; c<N; c++)
            A[f][c] = 'a' + f;

    for (int f=0; f<N; f++) {
        for (int c=0; c<N; c++)
            printf ("\t%c", A[f][c]);
        printf("\n");
    }

    return EXIT_SUCCESS;
}
