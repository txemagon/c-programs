#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define N 7

int main (int argc, char *argv[]) {

    long int A[N][N];

    for (int f=0; f<N; f++)
        for (int c=0; c<N; c++)
            A[f][c] = (long int) pow(c, f+1);

    for (int f=0; f<N; f++) {
        for (int c=0; c<N; c++)
            printf ("%11li", A[f][c]);
        printf("\n");
    }

    return EXIT_SUCCESS;
}
