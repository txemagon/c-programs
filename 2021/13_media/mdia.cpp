#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    double total = 0, num=0, media;
    int cantidad = -1;


    do {
        total += num;
        cantidad ++;

        printf ("Número: ");
        scanf ("%lf", &num);
    } while (num >= 0);

    media = total / cantidad;

    printf ("Media = %.2lf\n", media);
    printf ("\n");

    return EXIT_SUCCESS;
}
