#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    double total = 0, num, media;
    int cantidad = 0;


    do {
        //1. Pedir un número
        printf ("Número: ");
        scanf ("%lf", &num);
        // 2. Acumular el número
        total += num;
        //3. Incrementar la cuenta de números introducidos.
        cantidad ++;
    } while (num >= 0); //4. Si el número es > 0 volver a 1.

    // 5. Descontar el último número
    total -= num;
    cantidad --;

    // 6. Dividir la suma de numeros entre la cantidad de num introdu
    media = total / cantidad;

    printf ("Media = %.2lf\n", media);
    printf ("\n");

    return EXIT_SUCCESS;
}
