#include "robot.h"


// Constructors/Destructors
//  

Robot::Robot()
{
    this->serial = Robot::production++;
}

Robot::~Robot()
{
}



/**
 * Say "Hello" and robot serial number
 */
void
Robot::greet(std::ostream *out) const
{
    *out << "Hello, I'm a " << "Robot"  << " with serial number: " << this->serial << "\n";
}


/**
 * Get the value of serial
 * Robot serial number
 * @return the value of serial
 */
unsigned int
Robot::get_serial()
{
    return serial;
}