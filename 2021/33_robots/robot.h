
#ifndef ROBOT_H
#define ROBOT_H

#include <iostream>

class Robot
{
public:


                    Robot();
    virtual        ~Robot();

    void            greet(std::ostream *out = &std::cout)  const;
    unsigned int    get_serial ();




private:
    
    inline static   unsigned int    production = 0;         // Total number of robots produced till now.
    
    
                    unsigned int    serial;                 // Robot serial number
};

#endif // ROBOT_H
