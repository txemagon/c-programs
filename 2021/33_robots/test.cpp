#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "robot.h"

#include <iostream>

#define NROBOTS  10U

// VALID CASES

TEST_CASE ("Robots get serial number.") {

    Robot r[NROBOTS];

    for (unsigned i=0; i<NROBOTS; i++)
        REQUIRE (r[i].get_serial () == i);

}


TEST_CASE ("Robots shall greet.") {
    std::stringstream output, expected;

    /* Building output */
    Robot robbie;

    unsigned id = robbie.get_serial ();

    robbie.greet (&output);
    std::string result = output.str ();

    /* Building expectation */
    expected << "Hello, I'm a Robot with serial number: " << id << "\n";

    REQUIRE ( result.compare (expected.str ()) == 0);

}

TEST_CASE ("There are robot subclasses") {
    std::stringstream output, expected;


    /* Building output */
    Cyborg cyborg;

    Robot *robot = &cyborg;

    unsigned id = robot->get_serial ();
    robot->greet (&output);
    std::string result = output.str ();

    /* Building the expectation */
    expected << "Hello, I'm a Cyborg with serial number: " << id << "\n";

    REQUIRE ( result.compare (expected.str ()) == 0);

}

// INVALID CASES

TEST_CASE ("Comprar con un dni inválido") {}

