#include <stdio.h>
#include <stdlib.h>

enum TOp { sum, rst, mlt, dvd, TOT_OP };

const char *menu_opt[] = {
    "Suma",
    "Resta",
    "Multiplicación",
    "División",
    NULL
};

void title () {
    system ("clear");
    system ("toilet -fpagga --gay CALCULATOR");
    printf ("\n\n\n");
}

enum TOp menu () {
    unsigned i = 0;
    unsigned elegido;
    const char **p_menu = menu_opt;

    while (*p_menu != NULL) {
        printf ("\t\t%u.- %s\n", ++i, *p_menu);
        p_menu++;
    }

    printf ("\n\n");
    printf ("\tOpción: ");
    scanf ("%u", &elegido);

    return (enum TOp) (elegido - 1);
}

int suma (int op1, int op2) { return op1 + op2; }
int rest (int op1, int op2) { return op1 - op2; }
int mult (int op1, int op2) { return op1 * op2; }
int divi (int op1, int op2) { return op1 / op2; }

int preg_op (const char *pregunta) {
    int result;

    printf ("%s: ", pregunta);
    scanf ("%i", &result);

    return result;
}

int main (int argc, char *argv[]) {

    int a, b, resultado;
    enum TOp chosen;

    title ();

    chosen = menu ();
    a = preg_op ("Operando 1");
    b = preg_op ("Operando 2");

    switch (chosen) {
        case sum:
            resultado = suma (a, b);
            break;
        case rst:
            resultado = rest (a, b);
            break;
        case mlt:
            resultado = mult (a, b);
            break;
        case dvd:
            resultado = divi (a, b);
            break;
        default:
            fprintf (stderr, "Invalid Option case.");
            exit (1);
            break;
    }

    printf ("Resultado = %i\n", resultado);

    return EXIT_SUCCESS;
}
