#include <stdio.h>
#include <stdlib.h>

#define DX 0.0005


double f1 (double x) { return - (x * x) + 3; }
double f2 (double x) { return - (x * x) + 7; }

double integra (
        double (*pf)(double),
        const double LI,
        const double LS
        )
{
    double r = 0;

    for (double x=LI; x<LS; x+=DX)
        r += (*pf) (x);

    r *= DX;

    return r;
}


int main (int argc, char *argv[]) {

    system ("clear");
    system ("toilet -fpagga --gay Integrales");

    printf ("I1(0,3) = %.2lf\n", integra (&f1, 0, 3));
    printf ("I2(0,3) = %.2lf\n", integra (&f2, 0, 3));

    return EXIT_SUCCESS;
}
