#ifndef __EMPLEADO_H__
#define __EMPLEADO_H__

#define SMI 1000

class Empleado {

    char *nombre;
    double sueldo;

    public:
   Empleado ();
   Empleado (const char *nombre, double sueldo = SMI);
   Empleado (Empleado *orig);
};

#endif
