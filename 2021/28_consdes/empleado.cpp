#include <stdlib.h>
#include <string.h>

#include "empleado.h"

Empleado::Empleado () : sueldo(SMI)
{
    // Debería de cargarme este constructor por defecto
}

Empleado::Empleado (const char *nombre, double sueldo): sueldo (sueldo)
{
    unsigned len = strlen (nombre) + 1;

    this->nombre = (char *) malloc (len);

    strcpy (this->nombre, nombre);
}

Empleado &Empleado::operator=(const Empleado &op2) {
    this->sueldo = sueldo;
    this->nombre = (char *) realloc (strlen (op2.nombre) + 1);
    strcpy (this->nombre, op2.nombre);
}


Empleado::~Empleado () {
    free (this->nombre);
}
