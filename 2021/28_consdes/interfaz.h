#ifndef __INTERFAZ_H__
#define __INTERFAZ_H__

#include "empleado.h"

class Interfaz {

    public:
        Empleado preguntar ();
        void imprimir (Empleado);
};

#endif
