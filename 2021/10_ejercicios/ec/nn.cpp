#include <stdio.h>
#include <stdlib.h>


#define MAXNUM    0x10


void ver (unsigned char vector[], const char *nombre) {
    for (int i=0; i<MAXNUM; i++)
        printf ("%s[%2i] = %3i\n",nombre, i, vector[i]);

}

void rellenar(unsigned char *m) {
    for (int i=0; i<MAXNUM; i++)
       *(m + i) = 2 * i + 1;

    /*
    scanf ("%uc", &m[i]);
    scanf ("%uc", m + i);
    */
}

int main (int argc, char *argv[]) {

    unsigned char m[MAXNUM];   // Matriz de números.

    /* Cálculos */
    rellenar (m);

    /* Salida de Datos*/
    ver (m, "m");
    printf ("\n");


    return EXIT_SUCCESS;
}
