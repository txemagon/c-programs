#include <stdio.h>
#include <stdlib.h>

#define MAX 0x10

unsigned long fibo() {
    static unsigned long prev = 1;
    static unsigned long actu = 1;
    unsigned long nuevo = prev + actu;

    prev = actu;
    actu = nuevo;

    return nuevo;
}

int main (int argc, char *argv[]) {

    for (int i=2; i<MAX; i++)
        printf ("%lu ", fibo());
    printf ("\n");

    return EXIT_SUCCESS;
}
