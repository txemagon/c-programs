#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

#define MAX        0x50


// MENU CONSTANTS

#define OPTNUM     6

#define ENG        0
#define SPA        1
#define NLANG      (SPA+1)

#ifndef LANG
#define ACT_LANG   ENG
#else
#define ACT_LANG   LANG
#endif





/************/
/*   MENU   */
/************/

const int lang = ACT_LANG;

const char *menu_opt[][OPTNUM] = {
    {
        "Directory Contents.",
        "Directory Occupation.",
        "Disc Space.",
        "Memory.",
        "Exit",
        NULL
    },
    {
        "Contenido de un directorio.",
        "Ocupación del directorio.",
        "Espacio en disco.",
        "Memoria.",
        "Salir",
        NULL
    }
};

enum TOpt {
    content, occupation, diskspace, memory,
    EXIT
};

enum TPhr { MENU, OPTION, PRESSKEY, DIRECTORY, NPHRASES };
const char *tr[NPHRASES][NLANG] = {
    { "Menu", "Menú"},
    { "Option", "Opción"  },
    { "Press any key to continue", "Pulse una tecla para continuar" },
    { "Directory", "Directorio" }
};

void title () {
    system ("clear");
    system ("toilet -fpagga --gay Men-U");

    printf ("\n\n");

}

unsigned read_menu_opt() {
    unsigned opt;
    scanf ("%u", &opt);
    __fpurge(stdin);

    return opt - 1;
}

unsigned menu() {

    title ();
    printf ("\n");

    // Print menu options
    for (int i=0; menu_opt[lang][i]!=NULL; i++)
        printf ("\t\t%i.- %s\n", i+1, menu_opt[lang][i]);

    printf ("\n");
    printf ("\t%s: ", tr[OPTION][lang]);


    // Ask for options
    return read_menu_opt ();
}





/*****************/
/*   FUNCIONES   */
/*****************/

void wait_to_see () {
    printf ("\n%s.\n", tr[PRESSKEY][lang]);
    getchar ();
}


void ask_param (const char *param_name, char *const where) {

    printf ("\x1B[36m\n");
    printf ("%s: ", param_name);
    fgets (where, MAX, stdin);
    __fpurge (stdin);
    printf ("\x1B[0m\n");


}

void execute_command (const char * command) {
    printf ("\n");
    printf ("\x1B[33m\n");
    system (command);
    printf ("\x1B[0m\n");


    wait_to_see ();
}

void dircommand (const char *exec_command) {

    char directory[MAX];
    char command [0x80];

    ask_param (tr[DIRECTORY][lang], directory);

    /*
    strcpy (command, exec_command);
    strcat (command, directory);
    */

    snprintf (command, 0x80, "%s %s", exec_command, directory);

    execute_command (command);

}

void fn_content() {
    dircommand ("ls ");
}

void fn_occupation () {
    dircommand ("du -sh ");
}

void fn_discspace () {
    execute_command ("df -h");
}

void fn_freemem () {
    execute_command ("free");
}

/*****************/
/*   PRINCIPAL   */
/*****************/
int main (int argc, char *argv[]) {

    unsigned option;

    do {
        option = menu ();

        switch (option) {
            case content:
                fn_content ();
                break;
            case occupation:
                fn_occupation ();
                break;
            case diskspace:
                fn_discspace ();
                break;

            case memory:
                fn_freemem ();
                break;
        }

    } while (option != EXIT);

    return EXIT_SUCCESS;
}
