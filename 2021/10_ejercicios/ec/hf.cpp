#include <stdio.h>
#include <stdlib.h>

#define M 4

const char tablero[M][M] = {
    {1, 0, 0, 0},
    {1, 0, 1, 0},
    {1, 0, 1, 0},
    {1, 0, 1, 1}
};

void mostrar (const char board[M][M], int f, int c){
            printf ("%s   ",
                    /* board[f][c]*/ *(board + f*M + c) ?
                    "🛥" : "≈");
}

void print (const char board[M][M]) {
    for (int f=0; f<M; f++) {
        printf ("\t");
        for (int c=0; c<M; c++)
            mostrar (board, f, c);
        printf ("\n");
    }
}

int main (int argc, char *argv[]) {

    int f, c;

    system ("clear");
    print (tablero);
    printf ("\n");

    printf ("Fila: ");
    scanf ("%i", &f);

    printf ("Columna: ");
    scanf ("%i", &c);

    mostrar (tablero, f, c);

    printf ("\n");

    return EXIT_SUCCESS;
}
