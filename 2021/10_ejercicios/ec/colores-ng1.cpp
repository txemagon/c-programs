#include <stdio.h>
#include <stdlib.h>

#define ROJO      1
#define AMARILLO  2
#define AZUL      4

const char *colores[] = {
           "negro",
           "rojo",
           "amarillo",
           "naranja",
           "azul",
           "morado",
           "verde",
           "blanco"
};

void preguntar_componentes (bool *rojo, bool *amarillo, bool *azul) {
    *rojo     = true;
    *amarillo = false;
    *azul     = true;
}

int main (int argc, char *argv[]) {


    bool rojo, amarillo, azul;
    int color = 0;

    preguntar_componentes (&rojo, &amarillo, &azul);

    if (rojo)
        color |= ROJO;

    if (amarillo)
        color |= AMARILLO;

    if (azul)
        color |= AZUL;

    printf ("%s\n", colores[color]);
    printf ("\n");

    return EXIT_SUCCESS;
}
