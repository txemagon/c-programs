#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

bool preguntar ( const char *frase ) {
    char respuesta;

    printf ("%s (s/N): ", frase);
    scanf (" %c", &respuesta);

    return tolower (respuesta) == 's';
}

int main (int argc, char *argv[]) {

    bool tos    = preguntar ("tos");
    bool maulla = preguntar ("maulla");

    if (maulla)
        if (tos)
            printf ("Tos felina");
        else
            printf ("Gato");
    else
        if (tos)
            printf ("Tosferina");
        else
            printf ("Humano Sano");

    printf ("\n");


    return EXIT_SUCCESS;
}
