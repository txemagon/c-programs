#include <stdio.h>
#include <stdlib.h>

/*
001 = ROJO
010 = AMARILLO
100 = AZUL
101 = MORADO
*/

#define ROJO      1
#define AMARILLO  2
#define AZUL      4

void preguntar_componentes (bool *rojo, bool *amarillo, bool *azul) {
    *rojo     = true;
    *amarillo = false;
    *azul     = true;
}

int main (int argc, char *argv[]) {


    bool rojo, amarillo, azul;
    int color = 0;

    preguntar_componentes (&rojo, &amarillo, &azul);

    if (rojo)
        color = color | ROJO;


    if (amarillo)
        color = color | AMARILLO;

    if (azul)
        color = color | AZUL;

    switch (color) {
        case 0:
            printf ("negro\n");
            break;
        case 1:
            printf ("rojo\n");
            break;
         case 2:
            printf ("amarillo\n");
            break;
         case 3:
            printf ("naranja\n");
            break;
         case 4:
            printf ("azul\n");
            break;
         case 5:
            printf ("morado\n");
            break;
         case 6:
            printf ("verde\n");
            break;
         case 7:
            printf ("blanco\n");
            break;
 
         default:
            printf ("Estás viendo dragones.\n");
    }

    printf ("\n");

    return EXIT_SUCCESS;
}
