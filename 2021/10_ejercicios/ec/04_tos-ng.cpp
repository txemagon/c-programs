#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>


const char *pregunta[] = {
    "tos",
    "maulla",
    NULL
};


const char *solucion[] = {
    "Humano Sano",
    "Tosferina",
    "Gato",
    "Tos felina"
};

bool preguntar ( const char *frase ) {
    char respuesta;

    printf ("%s (s/N): ", frase);
    scanf (" %c", &respuesta);

    return tolower (respuesta) == 's';
}

int main (int argc, char *argv[]) {

    unsigned estado = 0;


    for ( int i = 0, bit_n = 1; pregunta[i] != NULL; bit_n <<= 1, i++ )
        if (preguntar (pregunta[i]))
            estado |= bit_n;

    printf ("%s\n", solucion[estado]);

/*
    switch (estado) {
        case 0:
            printf ("Humano Sano");
            break;
        case 1:
            printf ("Tosferina");
            break;
        case 2:
            printf ("Gato");
            break;
        case 4:
            printf ("Tos felina");
            break;
    }

    printf ("\n");
*/


    return EXIT_SUCCESS;
}
