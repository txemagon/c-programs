#include <stdio.h>
#include <stdlib.h>

#define MAX 0x80

int main (int argc, char *argv[]) {

    int i;
    unsigned long long int f[MAX];   // f: sucesión de fibonacci.



    /* CÁLCULOS */
    f[1] = f[0] = 1;

    for (i=2; i<MAX && f[i]>f[i-1]; i++)
        f[i] = f[i-1] + f[i-2];


    /* SALIDA DE DATOS */
    for (int i=0; i<MAX-1; i++)
        printf ("%llu, ", f[i]);
    printf ("%llu\n", f[MAX-1]);

    printf ("\n");

    printf ("tam: %lu\n", sizeof(f[0]));

    return EXIT_SUCCESS;
}
