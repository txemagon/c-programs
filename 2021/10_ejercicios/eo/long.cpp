#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    int a1;
    long int a2;
    long long int a3;

    printf ("INT SIZES\n\n");
    printf ("%lu\n", sizeof (a1));
    printf ("%lu\n", sizeof (a2));
    printf ("%lu\n", sizeof (a3));



    printf ("\nDOUBLE SIZES\n\n");

    double b1;
    long double b2;

    printf ("%lu\n", sizeof (b1));
    printf ("%lu\n", sizeof (b2));



    return EXIT_SUCCESS;
}
