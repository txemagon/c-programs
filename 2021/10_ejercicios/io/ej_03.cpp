#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    unsigned num;

    printf ("Numero (32-127): ");
    scanf (" %u", &num);

    printf ("%i: %c\n", num, num);

    return EXIT_SUCCESS;
}
