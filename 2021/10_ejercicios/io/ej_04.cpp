#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    char hexa[3];

    printf ("Byte en Hexadecimal: ");
    scanf (" %2[0-9a-fA-F]", hexa);
    hexa[2] = '\0';

    printf ("Numero: %s\n", hexa);

    return EXIT_SUCCESS;
}
