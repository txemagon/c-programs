#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define RUBBISH    400

//#define NDBG

#ifndef NDBG
#define DBG(...) fprintf (stderr, __VA_ARGS__);
#else
#define DBG(...)
#endif

enum TOption {desde, hasta, todo, N_OPT};

void menu (){
    printf ("\x1b[1;1f");

    printf (
            "Borrar:\n"
            "\n"
            "    1. Desde el cursor.\n"
            "    2. Hasta el cursor.\n"
            "    3. Toda la pantalla.\n"
            "\n"
            "Opción: "
            );
}

unsigned ask_option () {
    unsigned option;
    scanf ("%u", &option);

    return option - 1;
}

enum TOption fetch_valid_option () {
    unsigned option;

    do {
        menu ();
        option = ask_option ();
    } while (option > 2);

    return (enum TOption) option;
}

void random_char () {
    unsigned x = rand () % 80;
    unsigned y = rand () % 25;
    unsigned c = rand () % (128 -32) + 32;

    printf ("\x1b[%u;%uf%c", y, x, c);
}

int main (int argc, char *argv[]) {

    srand (time(NULL));

    printf ("\x1b[33m");
    for (unsigned i=0; i<RUBBISH; i++)
        random_char ();
    printf ("\x1b[0m\n");


    enum TOption option = fetch_valid_option ();

    switch (option) {
        case desde:
            printf ("\x1B[0J\n");
            break;
        case hasta:
            printf ("\x1B[1J\n");
            break;
        case todo:
            printf ("\x1B[2J\n");
            break;
    }

    // printf ("\x1b[%uJ", option);
    DBG ("Has elegido la opción %u\n", option);

    return EXIT_SUCCESS;
}
