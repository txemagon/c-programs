#include <stdio.h>
#include <stdlib.h>

#define QUETECAGASDEGRANDE  0X1000

int main (int argc, char *argv[]) {

    char sdelim;
    char ndelim;

    unsigned pos = 0;
    double v[QUETECAGASDEGRANDE];


    /* PREGUNTAR VECTOR */
    printf ("Vector: ");
    scanf (" %c", &sdelim);

    do {
        scanf (" %lf", &v[pos++]);
        scanf (" %c", &ndelim);
    } while (ndelim == ',');


    /* IMPRIMIR VECTOR */
    printf ("\n\t(");
    for (unsigned p=0; p<pos; p++)
        printf (" %.2lf", v[p]);
    printf (" )\n");

    return EXIT_SUCCESS;
}
