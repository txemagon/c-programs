#include <stdio.h>
#include <stdlib.h>

#define QUETECAGASDEGRANDE  0X1000

int main (int argc, char *argv[]) {

    char sdelim;
    char ndelim;

    unsigned pos = 0;
    double v[QUETECAGASDEGRANDE];


    /* PREGUNTAR VECTOR */

    // 1. Preguntar el vector
    printf ("Vector: ");
    // 2. Busco el delimitador de entrada
    scanf (" %c", &sdelim);

    do {
        // 3. Leo un double
        scanf (" %lf", &v[pos++]);
        // 4. Leo la coma o un delimitador de salida
        scanf (" %c", &ndelim);
        // 5. Si es coma repetir desde 3.
    } while (ndelim == ',');


    /* IMPRIMIR VECTOR */
    printf ("\n\t(");
    for (unsigned p=0; p<pos; p++)
        printf (" %.2lf", v[p]);
    printf (" )\n");

    return EXIT_SUCCESS;
}
