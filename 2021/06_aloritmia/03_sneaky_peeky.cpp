#include <stdio.h>
#include <stdlib.h>

int globl = 0xDEADBEEF;

int main() {

     /* DECLARACIÓN DE VARIABLES */
    int op1,      // Operando 1
        op2,      // Operando 2
        result;


    system ("clear");
    system ("toilet -fpagga --gay '    CALCULADORA'");
    printf ("\n\n");


    /* ENTRADA DE DATOS */
    printf("Operando 1: ");
    scanf(" %i", &op1);

    printf("Operando 2: ");
    scanf(" %i", &op2);



    /* CÁLCULOS */
    result = op1 + op2;



    /* SALIDA DE DATOS */
    printf("%i + %i = %i\n", op1, op2, result);


    system ("figlet -f small 'algunos datos'");
    printf ("\n\n");
    printf ("&%p: %i (%lu bytes)\n", &op1, op1, sizeof (op1) );
    printf ("&%p: %i (%lu bytes)\n", &op2, op2, sizeof (op2) );
    printf ("Las direcciones de memoria ocupan: %lu bytes.\n", sizeof (&op1));

    return EXIT_SUCCESS;
}
