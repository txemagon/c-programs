let XC = 450
let YC = 325

let k = 100

let d = 2

function x(coord) { return  coord * k + XC }
function y(coord) { return -coord * k + YC }

function conica (c, z) {
    return c * (z+d) / (z + 2 * d)
}

function line (ctx, initial, end) {
    ctx.moveTo (
        x (conica (initial[0], initial[2])),
        y (conica (initial[1], initial[2]))
    )

    ctx.lineTo (
        x (conica (end[0], end[2])),
        y (conica (end[1], end[2]))
    )

    ctx.stroke ()
}

function draw_figure (ctx) {

  for (let i=0; i<lines.length; i++)
    line (
        ctx,
        points[lines[i][0]],
        points[lines[i][1]]
    )
}

