let XC = 450
let YC = 325

let rn = 20

function x(coord) { return  coord  + XC }
function y(coord) { return -coord  + YC }

function draw_line (ctx, xp, yp, xf, yf, color) {
    let k = 100

    ctx.beginPath ()
    ctx.strokeStyle = color
    ctx.moveTo (x (xp), y (yp) )
    ctx.lineTo (x (xp + k * xf), y (yp + k * yf))
    ctx.stroke ()
}
function draw_sprite (ctx, xn, yn) {

    ctx.beginPath ()
    ctx.fillStyle = "blue"
    ctx.ellipse (x (xn), y (yn), rn, rn, 0, 0, 2 * Math.PI)
    ctx.fill ()
    ctx.strokeStyle = "black"
    ctx.ellipse (x (xn), y (yn), rn+3, rn+3, 0, 0, 2 * Math.PI)
    ctx.stroke ()
}

