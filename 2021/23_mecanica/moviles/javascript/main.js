let ctx


let xn = 0,
    yn = 0,
    vx = 0.2,
    vy = 0.5,
    ax = -0.002,
    ay = -0.002


let ahora = (new Date()).getTime ()

function recalcular (){}


function repintar (xn, yn) {
    ctx.beginPath ()
    ctx.clearRect (0, 0, 900, 650)

    draw_sprite  (ctx, xn, yn)
    draw_line    (ctx, xn, yn, vx, vy, "red")
    draw_line    (ctx, xn, yn, ax, ay, "black")
}

function game_loop () {
    let antes, incremento_t
    antes = ahora
    ahora = (new Date()).getTime ()
    incremento_t = (ahora - antes) / 10

    vx += ax * incremento_t
    vy += ay * incremento_t

    xn += vx * incremento_t
    yn += vy * incremento_t


    repintar (xn, yn)

    setTimeout (game_loop, 10)
}

function main () {
    // Inicialización
    ctx = document.getElementById ("canvas").getContext("2d")
    /*
    let nave = {
        pos: {x: 0,      y:0},
        vel: {x: 0.5,    y:0.2},
        acl: {x: -0.002, y: -0.005}
    }
    nave.acl.x = -0.001
    */

    game_loop ()
}
