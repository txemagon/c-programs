#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define DELAY 0.1

struct TCoord {
    double x;
    double y;
};


struct TMovil {
    struct TCoord pos;
    struct TCoord vel;
    struct TCoord acl;
};

double clock2sec (clock_t lapse) {
    return (double) (lapse) / CLOCKS_PER_SEC;
}

void imprimir (struct TMovil m, clock_t cur_t, clock_t *start_p, clock_t *end_p) {

    double lapse;
    *end_p = clock ();

    if ( (lapse = clock2sec (*end_p - *start_p) ) < DELAY)
     return;

    *start_p = *end_p;

    printf ("\x1B[2J\x1B[H\n");

    printf ("DATA AT %.2lfs\n", clock2sec (cur_t));
    printf ("==============\n");
    printf ("\n");
    printf ("Pos x: %.2lf\n", m.pos.x);
    printf ("Pos y: %.2lf\n", m.pos.y);
    printf ("Vel x: %.2lf\n", m.vel.x);
    printf ("Vel y: %.2lf\n", m.vel.y);
    printf ("Acl x: %.2lf\n", m.acl.x);
    printf ("Acl y: %.2lf\n", m.acl.y);

    printf ("\n");
}

void setup (struct TMovil *nave) {

    nave->pos.x = 0;
    nave->pos.y = 0;
    nave->vel.x = 0.2;
    nave->vel.y = 0.4;
    nave->acl.x = -0.02;
    nave->acl.y = -0.03;

}

double update_time ( clock_t *start_t, clock_t *end_t) {
    double inc_t;

    *end_t = clock ();
    inc_t = clock2sec(*end_t - *start_t);
    *start_t = *end_t;

    return inc_t;
}

void update_kinematic (struct TMovil *nave, double inc_t) {

    nave->vel.x += nave->acl.x * inc_t;
    nave->vel.y += nave->acl.y * inc_t;
    nave->pos.x += nave->vel.x * inc_t;
    nave->pos.y += nave->vel.y * inc_t;

}

int main (int argc, char *argv[]) {
    struct TMovil nave;
    clock_t start_t, end_t;
    clock_t start_p, end_p;
    double inc_t;

    setup (&nave);
    // bzero (&nave, sizeof (struct TMovil));
    start_p = start_t = clock ();
    for (;;) {
        /* Time update */
        inc_t = update_time (&start_t, &end_t);

        /* Kinematic update */
        update_kinematic (&nave, inc_t);
        imprimir (nave, end_t, &start_p, &end_p);
        start_t = clock ();
    }

    return EXIT_SUCCESS;
}
