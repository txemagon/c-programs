#include "general.h"
#include "game.h"

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>


void Game::run () {
#ifdef PRODUCTION
    this->intro ();
#endif

    while (this->menu () == jugar){
        this->game_loop.start ();
    };
}


void Game::intro () {
    char ch;

    FILE *pf = fopen (INTRO_SCREEN, "r");
    if ( ! pf ) {
        fprintf (stderr, "No se encontrón el fichero de título.\n");
        exit (1);
    }

    system ("clear");

    while (!feof (pf))
        if ( (ch = getc (pf)) != EOF)
            printf ("%c", ch);

    printf ("\x1b[H;\n");
    system ("toilet -f pagga '      TANKS   '");
    fclose (pf);
    sleep (1);
}



OpMenu Game::menu () {
    struct TOpcion {
        char texto[20];
        OpMenu valor;
    };

    TOpcion opcion[] = {
        {"Jugar", jugar },
        {"salir", salir }
    };

    system ("clear");
    system ("toilet -f pagga MENU");

    std::cout << "\n";
    std::cout << "\n";

    for (int i=0; i<TOTAL_OPM; i++)
        std::cout << "\t" << opcion[i].valor + 1 << ".- " << opcion[i].texto << "\n";
    std::cout << "\n";

    return (OpMenu) 1;
}

