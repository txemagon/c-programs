#include <stdio.h>
#include <stdlib.h>
#include "game.h"

#include "tank_spr.h"

int
main (int argc, char *argv[])
{
    Game game;

    game.run ();

    for (unsigned i=0; i<4; i++)
        printf ("%s\n", tank_sprite[i]);

    return EXIT_SUCCESS;
}
