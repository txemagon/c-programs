#ifndef __GENERAL_H__
#define __GENERAL_H__

#define HELP_TEXT "\n\
    %s                                                           \n\
    ===========                                                           \n\
                                                                          \n\
        - Convert sprites from a list of strings to sprite game format.   \n\
                                                                          \n\
    USAGE:                                                                \n\
                                                                          \n\
        %s [-i] [-o <fichero_salida>] [<sprite_entrada>]       \n\
                                                                          \n\
    OPTIONS:                                                              \n\
                                                                          \n\
      -i, --invert    Mirror the original sprite.                                   \n\
                                                                          \n\
      -o, --output    Specify output file name.                                     \n\
                                                                          \n\
      -h, --help      Show this help.                                               \n\
                                                                          \n\
"

#endif
