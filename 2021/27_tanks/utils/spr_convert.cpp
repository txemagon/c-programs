#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>

#include "general.h"


#define MAX 0x20

char *command_name;

void usage (FILE *out) {
    fprintf (out, HELP_TEXT, command_name, command_name);

    exit (out == stderr ? 1 : 0);
}

void fetch_command_name (char *cn) {
    command_name = cn + (*cn == '.' ? 2 : 0);
}

int main (int argc, char *argv[]) {


    int invert = 0;
    char output_file [MAX];
    char cmd_proc;
    int option_index = 0;

    struct option long_options[] = {
         {"help",   no_argument,       0, 'h'},
         {"invert", no_argument,       0, 'i'},
         {"outpt",  required_argument, 0, 'o'}
    };

    fetch_command_name (argv[0]);

    printf ("Command name: %s\n", command_name);

    while ( (
                cmd_proc =
                getopt_long (argc, argv, "hio:", long_options, &option_index)

            ) != -1 ) {

        switch (cmd_proc) {
            case 'h':
                usage (stdout);
                break;

            case 'i':
                invert = 1;
                break;

            case 'o':
                strncpy (output_file, optarg, MAX);
                break;

            default:
                abort ();
        }

    }

    printf ("Invertir: %s\n", invert ? "Sí": "No");
    printf ("Output file: %s\n", output_file);

    while (optind < argc)
        printf ("%s\n", argv[optind++]);
;
    // Preguntar que spr queremos convertir o leerlo de la línea de comandos.
    // Cargar en memoria
    // Queremos invertirlo
    // Preguntar el nombre del fichero de salida.

    return EXIT_SUCCESS;
}
