#include <stdio.h>
#include <stdlib.h>


int main (int argc, char *argv[]) {

    char sdelim;
    char ndelim;

    unsigned pos = 0;
    double *v = NULL;


    /* PREGUNTAR VECTOR */
    printf ("Vector: ");
    scanf (" %c", &sdelim);

    do {
        v = (double *) realloc (v, (pos + 1) * sizeof (double));
        scanf (" %lf", &v[pos++]);
        scanf (" %c", &ndelim);
    } while (ndelim == ',');


    /* IMPRIMIR VECTOR */
    printf ("\n\t(");
    for (unsigned p=0; p<pos; p++)
        printf (" %.2lf", v[p]);
    printf (" )\n");

    free (v);

    return EXIT_SUCCESS;
}
