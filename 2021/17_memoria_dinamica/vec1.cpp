#include <stdio.h>
#include <stdlib.h>

/*
 





              ┌─────────────┐                           ┌────────┬───────┬───────┬───────┬──────┬──────┬───────────────┐
              │             │                           │        │       │       │       │      │      │               │
              │      ───────┼──────────────────────────►│        │       │       │       │      │      │               │
              │             │                           │        │       │       │       │      │      │               │
              └─────────────┘                           └────────┴───────┴───────┴───────┴──────┴──────┴───────────────┘


                    V






 * */
int main (int argc, char *argv[]) {

    unsigned nceldas;
    double *v;

    printf ("Dimensión del vector: ");
    scanf (" %u", &nceldas);

    v = (double *) malloc (nceldas * sizeof (double));

    // Rellenar el array
    for (unsigned p=0; p<nceldas; p++) {
        printf ("v[%u] = ", p+1);
        scanf (" %lf", v + p);  // &v[p]
    }


    // Imprimir el array
    for (unsigned p=0; p<nceldas; p++)
        printf (" %.2lf", *(v+p)); // v[p]

    printf ("\n");


    free (v);

    return EXIT_SUCCESS;
}
