#include <stdio.h>
#include <stdlib.h>

#define N 8

int main (int argc, char *argv[]) {

    int *p = NULL;
    int *mover;

    p = (int *) malloc ( N * sizeof (int));
    mover = p;

    p[0] = 9;  // *p=9;
    p[1] = 3;  // *(p+1) = 3;

    *mover = 8;
    mover++;
    *mover = 2;

    printf ("%p: ", p);
    for (int i=0; i<N; i++)
        printf ("%i ", *(p+i));
    printf ("\n");


    free (p);

    return EXIT_SUCCESS;
}
