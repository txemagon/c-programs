#include <stdio.h>
#include <stdlib.h>

/*
p = (void *) 0
p = NULL
*/

int main (int argc, char *argv[]) {

    int *p = NULL;

    p = (int *) malloc (4);

    *p = 7;

    printf ("%p: %i\n", p, *p);

    free (p);

    return EXIT_SUCCESS;
}
