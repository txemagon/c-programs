#include "../code/Geometry/Point.h"

// VALID CASES

TEST_CASE ("Declaring an empty point") {
    
    Geometry::Point p;

    REQUIRE ( p.get_x() == 0 );
    REQUIRE ( p.get_y() == 0 );
}

TEST_CASE ("Declare a normal point") {
    
    Geometry::Point p(2.5, 3.9);

    REQUIRE ( p.get_x() == 2.5 );
    REQUIRE ( p.get_y() == 3.9 );
}

TEST_CASE ("Declare a point with one coordinate") {
    
    Geometry::Point p(2.5);

    REQUIRE ( p.get_x() == 2.5 );
    REQUIRE ( p.get_y() == 0   );
}


TEST_CASE ("Declare a point on top of another") {
    
    Geometry::Point original(2.5, 3.9);
    Geometry::Point copy(original);

    REQUIRE ( copy.get_x() == 2.5 );
    REQUIRE ( copy.get_y() == 3.9 );
}


// INVALID CASES