#include "Point.h"

Geometry::Point::Point() : coord (2, 0) {}
Geometry::Point::Point (double x, double y) 
{
    this->coord.push_back (x);
    this->coord.push_back (y);
}

/**
 * @return double
 */
double
Geometry::Point::get_x() const
{
    return this->coord[0];
}


/**
 * @return double
 */
double
Geometry::Point::get_y() const
{
    return this->coord[1];
}


/**
 * @return double
 */
double
Geometry::Point::operator[](unsigned i) const
{
    return this->coord[i];
}

double &
Geometry::Point::operator[](unsigned i)
{
    
    return this->coord[i];
}
