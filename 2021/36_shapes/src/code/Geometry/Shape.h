#ifndef SHAPE_H
#define SHAPE_H

#include <vector>
#include "Point.h"

namespace Geometry {


    /**
      * class Shape
      * Defines the basic attributes and methods for any Shape
      */

    class Shape
    {
    public:

        Shape();
        virtual ~Shape();
        double get_perimeter();
        double get_area();
        Point &operator[](unsigned i);

    protected:

        double perimeter;
        double area;

        virtual void calc_perimeter() = 0;
        virtual void calc_area() = 0;

    private:
        
        bool tainted;
        std::vector<Point> vertices;

        void untaint();


    }; // class Shape

} // end of package namespace

#endif // SHAPE_H
