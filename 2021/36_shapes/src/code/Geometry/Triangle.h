
#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Shape.h"


namespace Geometry {

class Triangle : virtual public Shape
{
public:
   
    Triangle();
    virtual ~Triangle();
    double get_base() const;
    double get_height() const;



protected:

    void calc_perimeter() override;
    void calc_area() override;

};
} // end of package namespace

#endif // TRIANGLE_H
