
#ifndef POINT_H
#define POINT_H

#include <vector>

namespace Geometry {

    class Point
    {
    public:

        Point();
        Point(double x, double y=0);

        double get_x() const;
        double get_y() const;
        double operator[](unsigned i) const;
        double &operator[](unsigned i);



    protected:

        std::vector<double> coord;


    }; // class Point

} // end of package namespace

#endif // POINT_H
