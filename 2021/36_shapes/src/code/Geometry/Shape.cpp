#include "Shape.h"

using namespace Geometry;

/*   PUBLIC   */
Shape::Shape() {}

Shape::~Shape(){}



double
Shape::get_perimeter()
{
    this->untaint ();
    return this->perimeter;
}


double
Shape::get_area()
{
    this->untaint ();
    return this->area;
}


Point &
Shape::operator[](unsigned i)
{
    return this->vertices[i];
}


/*   PRIVATE   */


void
Shape::untaint()
{
    if (this-> tainted) {
        this->calc_perimeter ();
        this->calc_area ();
       this->tainted = false;
    }
}