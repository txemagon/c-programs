#include <stdio.h>
#include <stdlib.h>

#define FICHERO "contacts.csv"

#define MXNAME 0x100
#define MXPHNE 0x10
#define MXEMIL 0x50

void leer_entrada (FILE *pf, char *nombre, char *phone, char *email) {
    fscanf (pf, " %[^,] %*[,]", nombre);
    fscanf (pf, " %[^,] %*[,]", phone);
    fscanf (pf, " %[^\n]", email);
    fscanf (pf, "%*[\n]");
}

void imprimir (char *nombre, char *phone, char *email) {
    printf ("Nombre: %s\n", nombre);
    printf ("Tel: %s\n", phone);
    printf ("Mail: %s\n", email);
    printf ("\n");
}

int main (int argc, char *argv[]) {

    FILE *pf;
    char nombr[MXNAME];
    char phone[MXPHNE];
    char email[MXEMIL];

    if ( !(pf = fopen (FICHERO, "r")) ){
        fprintf (stderr, "%s no encontrado!!!\n", FICHERO);
        return EXIT_FAILURE;
    }

    while (!feof(pf)) {
        leer_entrada (pf, nombr, phone, email);
        imprimir (nombr, phone, email);
    }

    fclose (pf);

    return EXIT_SUCCESS;
}
