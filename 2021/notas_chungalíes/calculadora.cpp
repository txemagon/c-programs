class Calculadora {
    int    suma (int, int);
    double suma (double, double);
};

// Sobrecarga
int    Calculadora::suma (int op1, int op2)         { return op1 + op2; }
double Calculadora::suma (double op1, double op2)   { return op1 + op2; }

caluladora.suma (2, 3);

// parámetro implícito this (puntero a lo que hay antes del punto)
// int suma (Calculadora *this, int op1, int op2)

// Dos vectores:
v1 + v2

// Se interpeta el + como un método.

Vector Vector::operator+ (Vector op2) {
    Vector resultado;

    resultado.x = this->x + op2.x;
    resultado.y = this->y + op2.y;

    return resultado;
}

// También está el parámetro implicito this.

namespace Interfaz {
   int a;

   a = 2;
};

Interfaz::a = 2;

class MiClase {
    int a;

    int suma (int, int);
};


int MiClase::suma (int op1, int op2){}

MiClase c;
