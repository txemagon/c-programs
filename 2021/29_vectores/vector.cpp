#include "vector.h"

Vector::Vector (double x, double y, double z):
    x(x), y(y), z(z)
{}

Vector Vector::operator+ (const Vector &op) {

    return Vector (
            this->x + op.x,
            this->y + op.y,
            this->z + op.z
            );
}
