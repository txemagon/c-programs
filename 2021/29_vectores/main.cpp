#include <stdio.h>
#include <stdlib.h>

#include "vector.h"

void imprimir (Vector v) {
    printf ("(%.2lf, %.2lf, %.2lf)", v.x, v.y, v.z);
}

int main (int argc, char *argv[]) {

    Vector v0 = Vector (2, 3, 5);
    Vector v1(5, 2, 3);
    Vector resul = v0 + v1;

    imprimir (v0);
    printf (" + ");
    imprimir (v1);
    printf (" = ");
    imprimir (resul);
    printf ("\n");

    return EXIT_SUCCESS;
}
