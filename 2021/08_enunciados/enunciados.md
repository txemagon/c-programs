# Enunciados

## Directos

Aquí tienes unos ejercicios de aplicación directa de lo aprendido en clase.
En ellos no tendrás que pensar complicados algoritmos. Sólo asegurarte de que
comprendes cómo funciona cada elemento. Según los vayas completando tendrás
que memorizar su sintaxis e incorporarla a tu arsenal de programador.

Estos ejercicios, por su simplicidad no admiten excusa para su no realización.
No realizarlos puede poner fin, con cierta probabilidad, a tus expectativas de
seguir la asignatura con normalidad. ¡Ánimo!. No te retrases.

Cada uno de los apartados está indicado con una etiqueta.

### IO. Entrada / Salida

1. Pregunta un número entero al usuario e imprímelo en pantalla.
1. Pregunta un número real al usuario e imprímelo en pantalla.
1. Pregunta un número entero entre 32 y 128 al usuario e imprímelo como carácter y como número.
1. Pregunta al usuario un número hexadecimal, guárdalo en un array de caracteres e imprímelo. No aceptes ningún caracter que no pertenezca al hexadecimal.
1. Borra la pantalla usando una secuencia de escape ANSI.
1. Pregunta dos operandos e imprímelos junto con la suma. `<op1> + <op2> = <resultado>`. Imprime los operandos en azul brillante. y el resultado en verde normal.
1. Guarda la posición del cursor. Imprime `HOLA`. Situa el cursor en la línea de arriba, encima de la `H` de `HOLA`. Borra la pantalla desde el inicio hasta el cursor usando una secuencia de escape ANSI. Vuelve a poner el cursor en la `H` de `HOLA`.
1. Lee una línea entera usando `fgets`. Elimina el `'\n'` final substituyéndolo por un `'\0'`. Usa `strlen` para calcular la longitud de la línea. Imprímela usando `puts`
1. Pregúntale un número al usuario e imprime su valor hexadecimal usando letras mayúsculas.
1. Imprime la dirección de memoria de una variable.

### Expresiones y Operadores
1. Imprime el tamaño de un array de 10 enteros.
1. Calcular la cantidad de elementos que tiene un array dividiendo su tamaño en bytes por lo que ocupa cada elemento.
1. Averigua cuantas veces es efectivo poner `long` en la declaración de `int` y de `double`. Declara variables como `int a`, `long int b`, `long long int c`, etc. Imprime su tamaño.
1. Depura el siguiente fragmento de código incrustándolo en un programa. Averigua como funciona. Piensa qué hacen los números en binario.

```c
int a = 0;
char b = 6;

while (b > 0) {
    b <<= 1;
    b |= (a = a++ % 2) ;
}

```

### EC. Estructuras De Control

#### EA. Estructuras Alternativas

##### IF. if else

1. Pregunta un número al usuario e imprime en pantalla si es par o impar.
1. Pregunta al usuario si quiere instalar algo al usuario. (s/n). Si responde 's' di: "Pues lo instalo.", y si es que 'n': "Pues no lo instalo.".
1. Repite el ejercicio anterior, pero permite que el usuario utilice mayúsculas o minúsculas. Usa la función `tolower` para la comparación.
1. Entérate de si un usuario tiene tos y también de si maúlla y haz un diagnóstico. Puede estar: "sano", "tos ferina", "ser un gato", "tos felina".
1. Pídele al usuario que imagine un color y pregúntale si su color tiene componentes rojo, verde y azul; dato que guardarás en sendas variables booleanas. Usando múltiples `if` anidados averigua su color. Nota: Si tiene rojo, verde y azul es blanco. Si no tiene ninguno es negro. Las demás mezclas asumimos que las sabes.
1. Pide un número real al usuario y di si está en el entorno de 3 más o menos `E`. Siendo `E` una constante simbólica que vale 0.001.

##### SC. switch case

1. Haz un menú que le permita al usuario:
    1. Listar un directorio de su elección. `system (comando);`. Comando es un array de caracteres que contendrá `"ls -lh "` y acontinuación el directorio de elección del usuario (mira el manual del comando `strcat`). En caso de que el usuario no haya introducido uno se listará el directorio actual.
    1. Ver los que ocupa un directorio y un subdirectorio. `system ('du -sh');`
    1. Consultar la cantidad de espacio libre que tiene cada disco. `system ('df -h');`
    1. Mostrar la ocupación de memoria. `system ('free');`


#### EI. Estructuras Iterativas

##### W. while

1. Imprime una array de caracteres que contenga una frase. Hazlo carácter a carácter. Si la letra que vas a imprimir es una 'i', entonces imprime un signo de exclamación.
1. Imprime un array desde el final (`strlen`) hasta el principio.

##### DW. do while

1. Haz que el usuario introduzca un número del 1 al 10. No aceptes una respuesta no válida.
1. Pídele números positivos al usuario. Habrás terminado cuando introduzca un número negativo o nulo. En ese momento muéstrale la media de todos los datos introducidos.

##### F. for
1. Imprime los 10 primeros números naturales.
1. Imprime 10 asteriscos.
1. Imprime 10 veces el carácter que te diga el usuario.
1. Imprime tantas veces como te diga el usuario el carácter que te diga el usuario.
1. Pide 10 números al usuario y muéstrale la media.

### A. Arrays
1. Declara un Array de enteros y llénalo con los 10 primeros números naturales. Imprímelo.
1. Declara un array de enteros y llénalo y pon en cada celda el valor del índice (número de celda contando desde 0) más el valor de la celda anterior.
1. Rellena un array con los 10 primeros números pares.
1. Rellena un array con los 10 primeros números impares.
1. Rellena el array `int A[10][10]`. La primera fila estará entera rellena con el carácter `'0'`, la segunda con el `'1'` y así sucesivamente hasta el `'9'`.
1. Usando la función `pow` rellena el array `int A[3][10];`. La primera fila tendrá los números del `'0'` al `'9'`. La segunda sus cuadrados, y la tercera sus cubos.



### FN. Funciones

1. Desarrolla la función `void title (); ` que borre la pantalla e imprima el título de un programa.
1. Desarrolla la función `int suma (int op1, int op1);` que devuelva la suma de sus dos operandos.
1. Desarrolla la función `void pedir_entero(int *variable);` que pida un entero al usuario y lo guarde en la dirección que detalla `variable`.

  A la vuelta de navidades se te va a examinar de los ejercicios que tengas hechos en tu gitlab. Acuérdate de ir subiéndolos según los vas haciendo. Penaliza mucho subirlos todos al final.

## Algoritmia

Soluciona los siguientes enunciados antes de reyes.

En esta sección tendrás que poner a prueba todas las habilidades aprendidas en las secciones anteriores y
construir algoritmos usando los 5 mandamientos del programador.

1. Asegúrate de comprender el enunciado (lo que se te pide) exactamente y en todos los casos.
1. Hazlo a mano suponiendo que existe la RAM, el teclado, la pantalla y el usuario, pero haz tú de ordenador. Recuerda que no puedes mantener ningún en la cabeza. Todos deben de estar escritos en alguna parte. Repite el proceso varias veces y con distintos datos de entrada.
1. Abstrae los pasos que has seguido y déjalos por escrito en lenguaje natural. Acuérdate de eliminar todos los números y todos los ejemplos. Usa expresiones como: "divido el posible primo por el candidato" en lugar de: "hago 3 / 2".
1. Codifica. Traduce de lenguaje natural a C.
1. Depura con el `gdb` o haz un seguimiento en papel.

Suerte con los siguientes ejercicios.

1. Leer un vector de tamaño variable encerrado entre paréntesis o corchetes. Los elementos estarán separados por comas y serán de tipo real (`double`). (IO, W)
1. Utilizando secuencias de escape ANSI hacer que caiga un asterisco como si fuera un copo de nieve. (IO,F).
1. Lee una línea desde la terminal carácter a carácter usando `getchar`. Si hay un texto entre corchetes o paréntesis dobles, guárdalo en un array e imprímelo.
Usa `ungetc` para devolver un carácter al stream cuando lo desees. (IO. W. IF)
1. Lee 10 números reales e imprímelos justificados con ceros de manera que ocupen como mínimo 6 posiciones la parte entera y 2 la decimal (sin contar el .).
1. Pregúntale el nombre al usuario e imprímelo centrado en pantalla. Para saber el número de columnas estudia el siguiente código: (IO)

    ```c
    #include <sys/ioctl.h>
    #include <stdio.h>
    #include <unistd.h>
    
    int main (int argc, char **argv)
    {
        struct winsize w;
        ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    
        printf ("lines %d\n", w.ws_row);
        printf ("columns %d\n", w.ws_col);
        return 0;  // make sure your main returns int
    }
    ```

1. Imprime el nombre que te dé el usuario recuadrado con el caracter que te especifique el propio usuario. Por ejemplo:

    ```
    *************
    *   MANUEL  *
    *************
    ```
    
    en el caso de que te hubiera dicho `"Manuel"` y `'*'`. Usa `toupper` para la conversión a mayúsculas y `strlen` para calcular la longitud. (IO, F, IF)

1. Rellena el array `int A[5][10]`. La primera fila contendrá los números naturales. En las siguientes filas, cada celda contendrá la suma de la celda superior más la celda superior derecha. La columna derecha del array sólo contendrá será igual a su celda superior.
1. Pregúntale el lado al usuario y dibuja un cuadrado usando espacios en blanco. El borde del cuadrado será dibujado con asteriscos.
