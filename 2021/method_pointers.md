# Method Pointers

Sea la siguiente clase con su método `int sum (int, int);`

```cpp
class MyClass
{
public:

    int sum(int a, int b) {
        return a+b;
    }

};
```

Para declarar un puntero al método haremos:

```cpp
int (MyClass::*methodPtrOne)(int, int);
```

Para asignar un valor al puntero:


```cpp
methodPtrOne = &MyClass::sum;
```

Y para invocar al método:


```cpp
MyClass clsInstance;
int result = (clsInstance.*methodPtrOne)(2,3);
```
