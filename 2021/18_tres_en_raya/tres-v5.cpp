// tres-v5: Comprobar el 3 en raya horizontal

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>


#define N 3  /* BOARD SIZE */

/* ERRORS */
#define OUT_OF_BOUNDS 2

enum TDato { vacio, j1, j2 };
const char *representa[] = {
    "  ",
    "⭕", // 2B55
    "❌", // 274C
};

void pintar (unsigned t[N][N]){
    printf ("\n");
    for (unsigned f=0; f<N; f++) {
        printf ("\t");
        for (unsigned c=0; c<N; c++){
            printf (" %s ", representa[t[f][c]]);
            if (c < N-1)
                printf ("│");
        }
        printf ("\n");

        // Imprimir una linea horizontal
        printf ("\t");
        if (f < N-1)
            for (unsigned c=0; c<N; c++) {
                printf ("────");
                if (c < N-1)
                    printf ("┼");
            }
        printf ("\n");
    }

    printf ("\n");
}

void fingir_datos(unsigned t[N][N]) {
    t[0][2] = t[0][1] = t[0][0] = j1;

    t[1][2] = j2;
}

bool check_hztal(unsigned t[N][N], unsigned fila, enum TDato jugador) {
    unsigned contador = 0U;

    if (fila >=N ){
        fprintf(stderr, "check_hztal: Invalid row %u.\n", fila);
        exit( OUT_OF_BOUNDS );
    }

    for (unsigned i=0; i<N; i++)
        if (t[fila][i] == jugador)
            contador++;

    return contador == N;
}

int main (int argc, char *argv[]) {

    unsigned tablero[N][N];

    /* Inicialización */
    bzero (tablero, sizeof(tablero));
    fingir_datos(tablero);

    for (unsigned f=0U; f<N; f++)
        if (check_hztal (tablero, f, j1))
            printf ("Tres en raya en la fila: %u!\n", f);
        else
            printf ("Fila %u: Libre.\n", f);

    pintar(tablero);

    return EXIT_SUCCESS;
}
