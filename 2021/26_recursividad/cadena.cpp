#include <stdio.h>
#include <stdlib.h>

const char *sent = "dabale arroz a la zorra el abad";

void imprime (const char *pletra) {
    if (*pletra == '\0')
        return;

    imprime (pletra+1);
    printf ("%c", *pletra);

}

int main (int argc, char *argv[]) {

    imprime (sent);

    printf ("\n");

    return EXIT_SUCCESS;
}
