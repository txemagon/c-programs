#include <stdio.h>
#include <stdlib.h>

void imprime (int n) {
    if (n<1)
        return;

    printf ("%i  ", n);

    imprime (n-1);
}


int main (int argc, char *argv[]) {

    imprime (6);

    printf ("\n");

    return EXIT_SUCCESS;
}
