#include <stdio.h>
#include <stdlib.h>

int f (int n) {
    if ( n == 0 )
        return 1;
    return n * f (n-1);
}

int main (int argc, char *argv[]) {

    printf ("6! = %i\n", f(6));

    return EXIT_SUCCESS;
}
