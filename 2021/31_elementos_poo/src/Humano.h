
#ifndef HUMANO_H
#define HUMANO_H

#include "Mamifero.h"

class Humano : virtual public Mamifero
{
public:
    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    Humano();

    /**
     * Empty Destructor
     */
    virtual ~Humano();


};

#endif // HUMANO_H
