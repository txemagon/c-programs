
#ifndef CABALLO_H
#define CABALLO_H

#include "Mamifero.h"

class Caballo : virtual public Mamifero
{
public:
    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    Caballo();

    /**
     * Empty Destructor
     */
    virtual ~Caballo();


};

#endif // CABALLO_H
