
#ifndef MAMIFERO_H
#define MAMIFERO_H

class Mamifero
{
public:
    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    Mamifero();

    /**
     * Empty Destructor
     */
    virtual ~Mamifero();



    /**
     */
    virtual void mover() = 0;

private:
    // Private attributes
    //  

    int n_extremidades;

    // Private attribute accessor methods
    //  


    /**
     * Set the value of n_extremidades
     * @param value the new value of n_extremidades
     */
    void setN_extremidades(int value)
    {
        n_extremidades = value;
    }

    /**
     * Get the value of n_extremidades
     * @return the value of n_extremidades
     */
    int getN_extremidades()
    {
        return n_extremidades;
    }

    void initAttributes();

};

#endif // MAMIFERO_H
