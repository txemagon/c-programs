
#ifndef VACA_H
#define VACA_H

#include "Mamifero.h"

class Vaca : virtual public Mamifero
{
public:
    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    Vaca();

    /**
     * Empty Destructor
     */
    virtual ~Vaca();


};

#endif // VACA_H
