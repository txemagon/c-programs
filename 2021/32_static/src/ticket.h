#include <string>

#ifndef TICKET_H
#define TICKET_H


class Ticket
{
public:

    Ticket() = delete;
    virtual ~Ticket();


    static  const Ticket*       comprar     (const std::string dni);
                  bool          es_valido   (const std::string dni)     const;
                  bool          consumir    (const std::string dni);
                  unsigned int  get_id      ()                          const;



private:

    Ticket (const std::string dni);

    static  unsigned int        ntickets;

            unsigned int        id;
            std::string         dni;
            bool                consumido;

    static  bool                validar_dni   (const std::string dni);
};

#endif // TICKET_H
