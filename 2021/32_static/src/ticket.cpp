#include "ticket.h"

// Constructors/Destructors
//  


Ticket::Ticket  (const std::string dni) : dni (dni), consumido (false)    {
    this->id = Ticket::ntickets++;
}
Ticket::~Ticket ()                                                  {}




/**************/
/*   STATIC   */
/**************/

unsigned int Ticket::ntickets = 0;

bool
Ticket::validar_dni (const std::string dni) { return true; }


const Ticket *
Ticket::comprar (const std::string dni)
{
    if ( ! Ticket::validar_dni (dni) )
        throw;

    return new Ticket (dni);
}






/****************/
/*   INSTANCE   */
/****************/


bool
Ticket::es_valido (const std::string dni) const  { return dni == this->dni; }


bool
Ticket::consumir (const std::string dni)
{

    if (this->consumido)
        throw;

    if ( ! this->es_valido (dni) )
        return false;

    this->consumido = true;

    return true;
}

unsigned int
Ticket::get_id () const { return this->id; }
