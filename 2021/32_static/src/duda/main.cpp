#include <stdio.h>
#include <stdlib.h>

#include "libreria.h"

#define N 3U

void imprimir () {
    printf ("n = %u\n", n++);
}

int main (int argc, char *argv[]) {

    for (unsigned i=0; i<N; i++)
        imprimir ();

    return EXIT_SUCCESS;
}
