#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "ticket.h"



// VALID CASES

TEST_CASE ("Comprar con un dni válido.") {
    const Ticket *ticket = Ticket::comprar ("23379414Y");

    REQUIRE ( ticket != nullptr );
}

TEST_CASE ("Comprar con un dni repetido.") {
    const Ticket *ticket[2];

    ticket[0] = Ticket::comprar ("23379414Y");
    ticket[1] = Ticket::comprar ("23379414Y");

    REQUIRE ( ticket[0] != nullptr   );
    REQUIRE ( ticket[1] != nullptr   );
    REQUIRE ( ticket[0] != ticket[1] );

    REQUIRE ( ticket[0]->get_id () != ticket[1]->get_id () );

}




// INVALID CASES

TEST_CASE ("Comprar con un dni inválido") {}

