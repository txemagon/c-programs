#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#define N 0x10

int  main(int argc, char *argv[]){

    int n = 11;
    int B[N];
    int i=0;
    bzero (B, sizeof (B));

    do {
        B[N-1-i++] = n % 2;
    } while ( n /= 2 );

    for (int i=0; i<N; i++)
        printf ("%s%i", i%4 ? "":" ", B[i]);

    printf ("\n");
    return EXIT_SUCCESS;
}
