#include <stdio.h>
#include <stdlib.h>

/* Función punto de entrada */
int  main(){

    int num;

    printf("Num: ");
    scanf(" %i", &num);
    printf(" 0x%X\n", num);

    return EXIT_SUCCESS;
}
