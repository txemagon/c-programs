#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define INC 0.0001


/* pol Evalua el valor de polinomio
 * de coeficientes coef en el punto x
 */
double pol (double *pol, int grado, double x) {
    int lon = (grado + 1) * sizeof (double);
    double *copia = (double *) malloc ( lon );
    double resultado = 0;
    memcpy (copia, pol, lon);

    for (int i=0; i<grado; i++)
        for (int celda=0; celda<=i; celda++)
            *(copia + celda) *= x;

    for (int i=0; i<=grado; i++)
        resultado += *(copia + i);

    free (copia);

    return resultado;
}

double integral (
        double li, double ls,
        double (*pf)(double) )
{
    double area = 0;
    for (double x=li; x<ls; x+=INC)
        area += INC * (*pf)(x);

    return area;
}

double parabola (double x) {
    double coef[] = {1, 0, 0};
    return pol (coef, 2, x);
}

double e_(double x) {
    return exp(-x);
}

int  main(int argc, char *argv[]){

    printf ("El área es: %lf\n", integral (1, 3, &e_));

    return EXIT_SUCCESS;
}
