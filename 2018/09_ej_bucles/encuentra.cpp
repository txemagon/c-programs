#include <stdio.h>
#include <stdlib.h>

/* Función punto de entrada */
int  main(){
    int A[] = {5, 20, 3, 9, 2, 13, 17, 9};
    int pos = -1;
    int buscado = 13;

    /* Busca el 13 en A */
    for (int i=0; i<sizeof(A)/sizeof(int); i++ )
        if ( A[i] == buscado )
            pos = i;

    if (pos >0)
        printf ("%i está en %i.\n", buscado, pos);
    else
        printf ("Nota!.\n");

    return EXIT_SUCCESS;
}
