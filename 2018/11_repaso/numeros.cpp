#include <stdio.h>
#include <stdlib.h>

#define N 10

/* Función punto de entrada */
int  main(){
    int lista[N];

    /* Entrada de datos */

    /* Calculos */
    for (int i=0; i<N; i++)
        lista[i] = (i + 1) * (i + 1);

    /* Salida de datos */
    /*
    for (int i=0; i<N; i++)
        printf ("%i ", lista[i]);
*/
    for (int i=0; i<N; i++){
        for (int j=0; j<lista[i]; j++)
            printf ("*");
        printf ("\n");
    }

    printf ("\n");

    return EXIT_SUCCESS;
}
