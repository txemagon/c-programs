#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define DIM 2

/* Función punto de entrada */
int  main(){

    double vec[DIM],
           modulo;

    system ("clear");

    printf ("Vector: ");
    scanf (" %lf, %lf", &vec[0], &vec[1]);

    modulo = sqrt( pow(vec[0], 2) + pow(vec[1], 2) );

    printf ("Módulo: %.2lf\n", modulo);

    return EXIT_SUCCESS;
}
