#include <stdio.h>
#include <stdlib.h>

#define OUT "frases.txt"
#define N 10
#define MAX_LIN 0x1000

int  main(int argc, char *argv[]){
    FILE *pf;
    char buffer[MAX_LIN];

    if ( !(pf = fopen ( OUT, "w" )) ){
        fprintf (stderr, "No he podido abrir %s.\n", OUT);
        return EXIT_FAILURE;
    }

    printf ("Dinos, las %i frases más importantes de tu vida.\n", N);
    for (int i=0; i<N; i++) {
        printf ("Frase: ");
        fgets ( buffer, MAX_LIN, stdin);
        fprintf (pf, "%s", buffer);
    }

    fclose (pf);
    return EXIT_SUCCESS;
}
