#include <stdio.h>
#include <stdlib.h>

#define DUMP "fibonacci.dat"

int  main(int argc, char *argv[]){

    FILE *pf;
    int fibo[] = { 1, 1, 2, 3, 5, 8, 13, 21, 34  };
    int n = sizeof (fibo) / sizeof (int);

    if (! (pf = fopen (DUMP, "wb"))) {
        fprintf (stderr , "Ajín.\n");
        return EXIT_FAILURE;
    }

    fwrite (fibo, sizeof (int), n, pf);

    fclose (pf);



    return EXIT_SUCCESS;
}
