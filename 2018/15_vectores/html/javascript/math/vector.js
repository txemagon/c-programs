
Vector.prototype = new Array
Vector.prototype.constructor = Vector
function Vector(x, y){
    if (x instanceof Array){
        this[0] = x[0]
        this[1] = x[1]
    }
    else {
        this[0] = x
        this[1] = y
    }
}

Vector.prototype.module = function () {
    return Math.sqrt(this[0] * this[0] + this[1] * this[1])
}

Vector.prototype.unit = function() {
    var m = this.module()
    return new Vector(this[0] / m, this[1] / m)
}

Vector.add = function(a, b) {
    return new Vector(a[0] + b[0], a[1] + b[1])
}

Vector.prototype.add = function (b) {
    return Vector.add(this, b)
}

Vector.mul = function(k, v) {
    if (typeof(k) == "number" || k instanceof Number)
        return new Vector(k * v[0], k * v[1])
}

Vector.dot = function(v1, v2) {
    return new v1[0] * v2[0] + v1[1] * v2[1]
}

Vector.prototype.dot = function (v1) {
    return Vector.dot(this, v1)
}

Vector.subs = function(a,b) {
    return Vector.add( a, Vector.mul(-1, b) )
}

Vector.prototype.subs = function (b) {
    return Vector.subs(this, b)
}

Vector.prototype.angle = function () {
    var angle = Math.atan2(this[1], this[0])
    if (angle < 0)
        angle = 2 * Math.PI + angle
    return angle
}

Vector.prototype.rotate = function (angle) {
    var m = this.module()
    var a = this.angle()
    return new Vector(m * Math.cos(a + angle), m * Math.sin(a + angle))
}