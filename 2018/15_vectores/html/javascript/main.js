/* Global Resoruces */
var logger 

function keydown(event) {
    if (event.code == "Space") {
        canvas.html.classList.add("Drag")
        canvas.dragging = true
    }
}

function keyup(event) {
    if (event.code == "Space") {
        canvas.html.classList.remove("Drag")
        canvas.dragging = false
    }
}

function main() {
    logger = new Logger("output")
        /* GUI */
    canvas = new Canvas(document.getElementById("lienzo"))
    n = new Nester("groups", canvas)
}