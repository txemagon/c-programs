function Logger (html) {
    this.name = html
    this.html = document.getElementById (html)
}

Logger.prototype.log = function (mssg) {
    this.html.innerHTML += "<span class='Log'>" + (new Date()).getTime() + ": " + mssg + "</span><br/>\n"
}
