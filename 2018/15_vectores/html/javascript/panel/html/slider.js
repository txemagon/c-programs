function SliderState(owner, min, max) {
    this.owner = owner
    this.lo = min
    this.hi = max
    this.owner.range = {
            lo: this.lo,
            hi: this.hi
        }
        /* html node references */
    this.lo_info
    this.hi_info
    this.slider_lo
    this.slider_hi
    this.node = create_node("div", "", {}, this.owner.node)

    this.create_html_slider("min", this.lo * 100)
    this.create_html_slider("max", this.hi * 100)

}

SliderState.prototype.create_html_slider = function(name, value) {
    var node_name = "lo_info"
    var slid_name = "lo"
    if (name == "max") {
        node_name = "hi_info"
        slid_name = "hi"
    }

    function eventer(that, name) {
        return function() {
            var valname = "lo"
            if (name == "max")
                valname = "hi"
            var state = that.get_active_state()
            var nval = {}
            nval.lo = state.lo
            nval.hi = state.hi
            nval[valname] = Number(this.value) / 100
            that.set(nval)
        }

    }


    var n

    create_node("label", name.capitalize() + ": ", {}, this.node)
    this[node_name] = create_node("span", value + " %", {
        id: "value_" + name + "_" + this.id
    }, this.node)
    this["slider_" + slid_name] = n = create_node("input", "", {
        id: "slider_" + name + "_" + this.id,
        type: "range",
        min: "0",
        max: "100",
        step: "0.01",
        value: "" + value,
        class: "Slider"
    }, this.node)
    n.addEventListener("input", eventer(this, name))
    n.addEventListener("change", eventer(this, name))
}

SliderState.prototype.dom = function() {
    return this.node
}


SliderState.prototype.get_active_state = function() {
    return this.owner["range"]
}

/* Rules for changing from one state to another */
SliderState.prototype.check_change_rules = function(new_state, old_state) {
    /* Check for bad values*/
    if (new_state.lo < 0 || new_state.hi > 1)
        return false
    return true
}

/* Trigger actions on state change */
SliderState.prototype.change = function(new_state, old_state) {
    this.owner.refresh()
}

SliderState.prototype.html_change = function(value_pair) {
    this.lo_info.innerHTML = value_pair.lo * 100 + " %"
    this.hi_info.innerHTML = value_pair.hi * 100 + " %"
    this.slider_lo.value = value_pair.lo * 100
    this.slider_hi.value = value_pair.hi * 100
}

SliderState.prototype.set = function(value_pair /* {lo: 0.2, hi:0.5}*/ ) {
    var old_value = this.get_active_state()
    if (!this.check_change_rules(value_pair, old_value))
        return
    this.owner["range"] = value_pair
    this.change(value_pair, old_value)

    this.html_change(value_pair)
}