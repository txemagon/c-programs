CardList.id = 0
function CardList(board, name, controller) {
	this.id = CardList.id++
	this.board = board
	this.name = name
    this.node = create_node ("div", "", {id: "group_" + this.id, class: "ProfileGroup" })
    this.entity = {} /* HTML components representing and holding data as an array of coordinates */
    this.entity_order = []			
    this.curves = {} /* HTMLEntity data processed as Curve HTMLEntitity#name is foreign key */
    this.action_state = {active: {class: ""}}

    this.punch_x = 0
    this.punch_y = 0
}


CardList.prototype.cast = function (mssg, data, sender) {
	data = data || {}
	data.group = this.name.underscore()
	switch (mssg){
		case "delete_group":
		    for (var i in this.entity)
		    	this.entity[i].do_remove()
			break
		case "delete_profile":
		    delete this.entity[data.profile.underscore()]
		    this.entity_order.splice( this.entity_order.findIndex( el => el == data.profile.underscore()) , 1)
			break
		case "profile_added":
			if (data) {
			    this.active_profile = data
			    if ( this.entity_order.findIndex( el => el == data.profile.underscore() ) == -1)
			    	this.entity_order.push (data.profile.underscore())
			    this.board.cast("activate_card", this.name.underscore())
			    this.set(data.profile, "work")
			    this.entity[data.profile].actuate("show", "add")
			}
		    break
		case "work_profile":
			if (data) {
			    this.active_profile = data
			    data.data = this.curves[data.profile]
			    this.board.cast("activate_card", this.name.underscore())
			    this.set(data.profile, "work")
			}
		    break
		case "toggle_view_profile":
			this.entity[data.profile].actuate("show", "toggle")
			break

		case "punch_sooner":
			var i = this.entity_order.findIndex( el => el == data.profile)
			if (i>0) {
				[this.entity_order[i-1], this.entity_order[i]] = [this.entity_order[i], this.entity_order[i-1]]
				this.board.cast("panel_change")
			}
			break

		case "punch_later":
		var i = this.entity_order.findIndex( el => el == data.profile)
			if (i < this.entity_order.length-1) {
				[this.entity_order[i], this.entity_order[i+1]] = [this.entity_order[i+1], this.entity_order[i]]
				this.board.cast("panel_change")
			}
			break
	}

    this.board.cast(mssg, data, sender)

    switch (mssg) {
        case "profile_added":
             this.actuate("active", "add")
             break
        case "work_added":
             this.actuate("active", "add")
             break
        case "delete_profile":
             this.board.cast("panel_update")
		     this.board.cast("canvas_update")
		     break
     }

     this.board.cast("panel_change")

}

CardList.prototype.get_active_profile = function () {
	return this.active_profile || [[]]
}

CardList.prototype.list_entities = function (callback) {
	for (var i=0; i<this.entity_order.length; i++)
		if (this.entity[this.entity_order[i]] && this.entity[this.entity_order[i]] instanceof HTMLEntity)
			callback (this.entity[this.entity_order[i]], this.entity_order[i])
}

CardList.prototype.html_render = function () {
	var n, b
    n = create_node ("h2", this.name.humanize().capitalize(), {}, this.node)
    
    b = create_node ("button", "Load", { class: "first"}, this.node)
    b.addEventListener ("click", () => { this.add_profile ()} )
    b = create_node ("button", "Save", { class: "next"}, this.node)
    b.addEventListener ("click", () => { this.add_profile ()} )
    b = create_node ("button", "Delete", { class: "last"}, this.node)
    b.addEventListener ("click", () => { this.cast ("delete_group")} )

    create_node ("br", "", {}, this.node)
	/*if (this.action_state.active.class.search("Selected") != -1)
		n.classList.add("Selected")
	else
		n.classList.remove("Selected")
	*/
    //punch:  {s: "🖸"		/*"&#x1F5B8"*/, config: {class: "next" , title: "Get Program" } },
    this.list_entities ( (entity, name) => {
        	this.node.appendChild (entity.get_dom ())
        })
    
    b = create_node ("button", "Add Profile", {}, this.node)
    b.addEventListener ("click", () => { this.add_profile ()} )
    create_node ("br", "", {}, this.node)

/*
    create_node ("span", " Orig X:", {}, this.node)
    b = create_node ("input", this.punch_x, { type: "Number", id: this.name.underscore() + "_punch_x", value: this.punch_x }, this.node)
    b.addEventListener ("change", (function(that) { 
    	return function(ev) {
    		that.punch_x = Number (this.value)
    	}
    }) (this) )

	create_node ("span", " Orig Y:", {}, this.node)
    b = create_node ("input", this.punch_y, { type: "Number", id: this.name.underscore() + "_punch_y", value: this.punch_y }, this.node)
    b.addEventListener ("change", (function(that) { 
    	return function(ev) {
    		that.punch_y = Number (this.value)
    	}
    }) (this) )   


    b = create_node ("button", "Punch", {}, this.node)
    b.addEventListener ("click", () => { this.add_profile ()} )
*/
	return this.node
}

CardList.prototype.get_dom = function () {
	prune_dom (this.node)
	return this.html_render()
}

CardList.prototype.unset_all = function(action) {
	Object.keys(this.entity).forEach( (name, i) => {
		this.entity[name].actuate(action, "remove")
	} )
}

CardList.prototype.set = function (name, action) {
	this.unset_all(action)
	this.entity[name].actuate(action, "add")
}

CardList.prototype.add_profile = function (initial_values) {
		var np = (new ProfileEditor(
			result => {
				try {
					var curve = new Curve (result.data, true)
					var name = result.name.underscore()
					this.curves[name] = curve
					this.entity[name] = new HTMLEntity(name, result.data, this.node, this)
					this.cast("profile_added", {
						group: this.name.underscore(), 
						profile: result.name.underscore(), 
						data: this.curves[name]
					})
				} catch (err) { 
					console.log ("Impossible to parse profile.")
				}
			}
			)).start(initial_values)
	
}

CardList.prototype.actuate = ModuleSelect.actuate
