/*
var panel = {
    id: 12,
    title: "Interval 0",
    node: null,
    parent_node: null,
    views: {
        curve: {
            none: {
                label:   0,
                fx_data: 0
            }
            original: {
                label:   0,
                fx_data: 0
            }
        },
        vector: ["none", "tangent", "normal"],
        curve2: ["none", "evolute"]
    },
    actions: ["refine"]
}

var panex = {
    views: {
        pointset:{
            label: 0,
            fx_data: 0
        },
    },
    actions: {}
}
*/

function Board(controller, title, parent_node, html_config) {
    html_config = html_config || {}

    this.id = Board.number++
        this.controller = controller
    this.title = title.humanize().capitalize()
    if (typeof(parent_node) == "string")
        parent_node = document.getElementById(parent_node)
    if (!parent_node)
        throw "Parent node not found on Board " + title + "creation."
    this.parent_node = parent_node

    this.show_add_interval = false
    this.slider = null

    this.node = document.createElement("div")

    for (var attr in html_config)
        this.node.setAttribute(attr, html_config[attr])

    controller.panel.add(controller.title.underscore(), this)
    this.views = {}
    this.actions = []
    this.view_selector = {}
}

Board.number = 0

Board.prototype.get_data_source = function() {
    return this.controller.get_active_data()
}

Board.prototype.add_selector = function(view_name) {
    for (var view_name in this.views)
        this.view_selector[view_name] = new SelectState(this.controller, "active_" + view_name,
            Object.keys(this.views[view_name]), "none", view_name, {})
}

Board.prototype.create_view = function(view_name, view_func) {
    this.views[view_name] = {}
    this.views[view_name].none = {
        label: "none",
        fx_data: []
    }
    this.add_selector(view_name)

    this.controller.canvas_view ["active_" + view_name + "_" + this.controller.title.underscore()] = view_func
}

Board.prototype.add_view = function(view_name, item, fx_data, label) {
    label = label || item.humanize()
    if (!this.views[view_name])
        this.create_view(view_name)
    if (!item)
        return
    this.views[view_name][item] = {
        label: label,
        fx_data: fx_data
    }
    this.view_selector[view_name].set(item)
}

Board.prototype.add_action = function(name, params) {
    var a = new Action(this, name, params)
    this.actions.push(a)
    return a
}

Board.prototype.find = function(name) {
    var nm = name.underscore()
    for (var i=0; this.actions.length; i++)
        if (nm == this.actions[i].name.underscore())
            return this.actions[i]
}

Board.prototype.set_slider = function() {
    this.slider = new SliderState(this.controller, 0, 1)
}

Board.prototype.cast = function(action, result) {
    this.controller.cast(action, result)
}

Board.prototype.update_info = function(info) {
    var point_info = document.getElementById("info_points_" + this.id)
    var distance_info = document.getElementById("info_distance_" + this.id)
    if (!(point_info && distance_info))
        return
    point_info.innerHTML = info.points || 0
    distance_info.innerHTML = info.distance || 0
}

Board.prototype.header = function() {
    var n
    var info = this.controller.info()
    create_node("h2", this.title + " Board:", {}, this.node)
    create_node("label", "Active Profile Info: ", {}, this.node)
    n = create_node("div", "", {}, this.node)
    create_node("label", "Points:", {}, n)
    create_node("span", info.points || 0, {
        id: "info_points_" + this.id
    }, n)
    create_node("br", "", {}, n)
    create_node("label", "Distance:", {}, n)
    create_node("span", info.distance || 0, {
        id: "info_distance_" + this.id
    }, n)
    create_node("br", "", {}, n)
}

Board.prototype.html_render = function() {

    this.header()

    if (this.slider)
        this.node.appendChild(this.slider.dom())

    for (var view_name in this.view_selector)
        this.node.appendChild(this.view_selector[view_name].dom())

    for (var i = 0; i < this.actions.length; i++)
        this.node.appendChild(this.actions[i].dom())

    if (this.show_add_interval) {
        var n = create_node("div", "", {
            class: "AddInterval"
        }, this.node)

        n = create_node("button", "Add Interval", {
            style: "margin-top: 30px;"
        }, n)
        n.addEventListener("click", (function(that) {
            return function() {
                that.controller.cast("interval")
            }
        })(this))
    }

    return this.node
}

Board.prototype.get_dom = function() {
    prune_dom(this.node)
    return this.html_render()
}

Board.prototype.html_add_interval = function() {
    this.show_add_interval = true
}