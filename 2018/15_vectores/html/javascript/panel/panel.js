function Panel(html) {
    this.id = html
    this.html = html_node(html)
    this.board = {}
}

Panel.prototype.add = function(id, board) {
    this.board[id] = board
}

Panel.prototype.refresh = function() {
    prune_dom(this.html)
    for (var i in this.board)
        this.html.appendChild(this.board[i].get_dom())
}