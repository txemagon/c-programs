Profiler.prototype = new Controller
Profiler.prototype.constructor = Profiler
Profiler.prototype.super = Controller

function Profiler(title, panel, nester, initial_set) {
    this.super.call(this, title, panel, nester)
        /* Virtual StateMachine variables */
        // this.active_data = "original"
    this.board.create_view("data", () => {
                this.nester.canvas.view(this.get_active_data().p.p)
            })

    this.board.add_view("data", "original", initial_set)
    this.board.add_action("refine", {
        preset: () => { this.board.view_selector["data"].set("original") },
        intermediate_points: [100, 1, 2],
        stress_factor: [17]
    })

    this.curve = initial_set
    this.boundary = initial_set.focus()

    var n
    var por_x = [0, 1]
    var por_y = [0, 1]
    por_x.listeners = {change: () => {this.por_x = n.argval[0].value }}
    por_y.listeners = {change: () => {this.por_y = n.argval[1].value }}
    n = this.board.add_action("picture", {
        origin_x: por_x,
        origin_y: por_y
    })
    this.por_x = n.argval[0].value = parseInt (this.boundary[0] - 131)
    this.por_y = n.argval[1].value = parseInt (this.boundary[2] - 131)
    this.interval = []
    this.board.html_add_interval()
    
    this.canvas_view["punch_origin"] = () => {
            this.nester.canvas.origin_sign(this.por_x, this.por_y)
    }

    n.argval[0].addEventListener("change", () => {
            this.nester.cast("canvas_change")
    })

    n.argval[1].addEventListener("change", () => {
            this.nester.cast("canvas_change")
    })

    this.board.add_action("Radii",{})  
    
    this.panel.refresh()
}


Profiler.prototype.add_interval = function() {
    this.interval.push(new Interval("Interval " + this.interval.length, this.panel, this.nester, this))
}

Profiler.prototype.get_active_data = function() {
    if (this.active_data == "none" || !this.item.data[this.active_data])
        return {
            p: {
                p: []
            }
        }
    return this.item.data[this.active_data].fx_data
}

Profiler.prototype.total_active_points = function () {
	var c = 0
	for (var i=0; i<this.interval.length; i++)
	    c += this.interval[i].total_active_points ()
	return c
}

Profiler.prototype.info = function () {
	var curve = this.get_active_data()
    if (! (curve && curve.p && curve.p.S && this.interval))
        return {distance: 0, points: 0}
    var distance = curve.p.S[curve.p.S.length-1]
    distance = parseInt (100 * distance) / 1000
    var total_points = 0
    for (var i=0; i<this.interval.length; i++)
        total_points += (this.interval[i].total_active_points() || 0)
    return { distance: distance, points: total_points }
}

Profiler.prototype.get_canvas_views = function () {
    var result = []
    result = result.concat( this.super.prototype.get_canvas_views.apply (this, [this.title.underscore() + "_"]) )
    for (var i=0; i<this.interval.length; i++)
        result = result.concat( this.interval[i].get_canvas_views(this.title.underscore() + "_"))
    return result
}

Profiler.prototype.subroutine = function(xd, yd){
    var progr = "" 
    var p = 0
    for (var i = 0; i < this.interval.length; i++) {
        progr += ";TOOL "+ (Number(this.interval[i].contour_r.argval[0].value) * 10) + " T 10 MT1\n"
        progr += ";CARICO XPIN Y350\n"
         var points = this.interval[i].get_active_contour()
         for (var j = 0; j < points.length; j++, p++)
             progr += ";COLPO X" + round_to(2, points[j][0] - xd) + " Y" + round_to(2, points[j][1] - yd) + "\n"
     }
    return progr
}

Profiler.prototype.picture = function(xmin, ymin) {
            var progr = "data:,"
            progr += this.subroutine(xmin, ymin)
            window.open(encodeURI(progr))
}

Profiler.prototype.cast = function(action, result) {
    var that = this
    switch (action) {
        case "Radii":
            this.radius()
            break
        case "refine":
            this.board.add_view("data", "refined", result)
            this.board.update_info(this.info())
            this.nester.cast("canvas_change")
            break
        case "interval":
            this.add_interval()
            this.panel.refresh()
            break
        case "picture":
            var xd = Number(document.getElementById("picture_action_param_origin_x_" + this.title.underscore()).value)
            var yd = Number(document.getElementById("picture_action_param_origin_y_" + this.title.underscore()).value)
            this.picture(xd, yd)
            break
        case "update_info":
            this.board.update_info(this.info())
        break
    }

}

Profiler.prototype.radius = function() {
    this.nester.canvas.graph(this.get_active_data().r)
}
