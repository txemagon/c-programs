#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define X 0
#define Y 1
#define DIM 2

/* Función punto de entrada */
int  main(){
    double vector[2][DIM],
           modulo[2],
           producto = 0,
           angulo;


    for (int i=0; i<2; i++) {
        printf ("Vector %i: ", i + 1);
        scanf (" %lf, %lf", &vector[i][X], &vector[i][Y]);
    }

    for (int i=0; i<2; i++)
        modulo[i] = sqrt(
                pow(vector[i][X], 2) +
                pow(vector[i][Y], 2)
                );

    for (int c=0; c<DIM; c++)
        producto += vector[0][c] * vector[1][c];

    angulo = acos ( producto / modulo[0] / modulo[1] );
    angulo *= 180 / M_PI;

    printf ("Angulo = %.2lfº\n", angulo);

    return EXIT_SUCCESS;
}
