#ifndef __GUI_H__
#define __GUI_H__

#include <stdio.h>
#include <stdlib.h>

#include "general.h"
#include "queue.h"

#ifdef __cpluscplus
extern "C" {
#endif
void imprimir (struct TCola c);
void titulo ();
#ifdef __cpluscplus
}
#endif

#endif
