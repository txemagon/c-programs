#include <stdio.h>
#include <stdlib.h>

double suma (double op1, double op2) { return op1 + op2; }
double rest (double op1, double op2) { return op1 - op2; }

int main (int argc, char *argv[]) {

    double (*func[2]) (double, double) = { &suma, &rest  };

    return EXIT_SUCCESS;
}
