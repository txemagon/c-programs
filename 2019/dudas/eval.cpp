#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double pol (double x) {
    return x * x;
}
double e_x(double x) {
    return pow (M_E, x);
}

double (*fun[]) (double) = {&pol, &e_x};

int main (int argc, char *argv[]) {


    int op = 1;
    double x = 0.9;


    printf ("e(%.2lf) = %.2lf\n", x, (*fun[op]) (x) );

    return EXIT_SUCCESS;
}
