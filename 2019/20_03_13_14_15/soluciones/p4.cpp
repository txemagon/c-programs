#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define E 0.01

void title () {
    system ("clear");
    system ("toilet -fpagga --gay POLINOMIOS");
    printf ("\n");
    printf ("Multiplicación de Matrices.\n");
    printf ("\n\n");
}

double f (double *polinomio, double grado, double x) {

    double altura = 0,
           potencia = 1;

    for (int p=0; p<grado; p++, potencia*=x)
        altura += polinomio[p] * potencia;

    return altura;
}

bool zero(double *pol, int grado, double li, double ls) {
    return f(pol, grado, li) * f(pol, grado, ls) > 0 ? true : false;
}

int main (int argc, char *argv[]) {
    double buffer, li, ls, *pol = NULL;
    char end[2]; /* Un paréntesis y un \0*/
                 /* Si lo dejamos como char el \0 se escribe en dim. */
    int dim = 0;
    double medio;

    title ();

    printf ("Introduce el Polinomio\n");
    printf ("ej: 3x² + 2x + 1.5 => (1.5 2 3)\tPolinomio: ");
    scanf (" %*[(]");
    do {
        pol = (double *) realloc(pol, (dim+1) * sizeof(double));
        scanf(" %lf", &buffer);
        pol[dim++] = buffer;
    } while (!scanf(" %1[)]", end));

    do {
        title ();
        printf ("Escoge dos valores para los que la función tiene signo distinto.\n");
        printf ("\t\t\t\t\tLímite Inferior: ");
        scanf (" %lf", &li);
        printf ("\t\t\t\t\tLímite Superior: ");
        scanf (" %lf", &ls);
    } while (zero (pol, dim, li, ls));


    do {
        medio = (li + ls) / 2;
        if (zero(pol, dim, medio, ls))
            ls = medio;
        else
            li = medio;
    } while (fabs (ls - li) > E);

    /* Nota. Esto es el método de Newton Raphson. */

    printf ("La función tiene una raíz en %.2lf\n", medio);

    free (pol);

    return EXIT_SUCCESS;
}
