#ifndef __STACK_H__
#define __STACK_H__

#define MAX_SIZE 0x800

struct TStack {
    int *data;
    unsigned summit;
    unsigned size;
    int failed;
};

#ifdef __cplusplus
extern "C"
{
#endif
    void init (struct TStack *s);
    void destroy (const struct TStack *s);
    void push (struct TStack *s, int ndata);
    int pop (struct TStack *s);
#ifdef __cplusplus
}
#endif

#endif
