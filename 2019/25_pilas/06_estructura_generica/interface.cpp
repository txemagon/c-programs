#include "interface.h"

/* FUNCIONES DEL INTERFAZ */

void title () {
    system ("clear");
    system ("toilet -fpagga --gay 'PILA'");
    printf ("\n\n");
}

void show_error (const char *mssg) {
    fprintf (stderr, "%s\n", mssg);
}

void show_stack (struct TStack s) {
    title ();
    for (int i=s.summit-1; i>=0; i--)
        printf ("\t%5.2lf\n", s.data[i]);
    printf ("\n\n");
}

