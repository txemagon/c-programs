#include "stack.h"

/* FUNCIONES DE LA MÁQUINA DE DATOS */
void init (struct TStack *s) {
    s->summit = 0;
    s->failed = 0;
}

void push (struct TStack *s, int ndata) {
    s->failed = 0;
    if (s->summit >= M) {
        s->failed = 1;
        return;
    }
    s->data[s->summit++] = ndata;
}

int pop (struct TStack *s) {
    s->failed = 0;
    if (s->summit <= 0) {
        s->failed = 1;
        return -666;
    }
    return s->data[--s->summit];
}
