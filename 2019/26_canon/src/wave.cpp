#include "wave.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define FREQ 44100
#define VOL   0xFF

#define DO0        16.351597831
#define K_FREQ_INC 1.05946309


const double m_2pi    = 2 * M_PI;             /* Cache this to speed up */
      double m_2pi_fm = 2 * M_PI / FREQ;      /* switch to sample domain */

void set_freq (unsigned *p_freq) {
    if (!*p_freq)
        *p_freq = FREQ;
    m_2pi_fm = m_2pi / *p_freq;
}

WAVEFORMAT create_header (unsigned freq) {
    WAVEFORMAT header;

    header.wFormatTag      = 2;     /* Random Format Type */
    header.nChannels       = 1;
    header.nSamplesPerSec  = freq;
    header.nAvgBytesPerSec = freq;  /* 1 byte per sample  */
    header.nBlockAlign     = 0;     /* still no data      */

    return header;
}

void destroy_wav (struct TWav *w) {
    free (w->data);
    free (w);
}

void append_samples (
        struct TNote n,     /* tone to be generated */
        unsigned bpm,       /* score bmp rate       */
        unsigned freq,      /* sampling frequency   */
        BYTE **data_origin, /* wav data origin      */
        QWORD *p_insp)   /* data insertion point */
{

    /* Render parameters for the new note. */
    unsigned duration;
    /* Duration in samples */
    duration =  4 * 60. / bpm * (double) n.duration.numerator / n.duration.denominator * freq;
    /* For faster applications a lookup table might be required. */
    unsigned tone = n.tone_id; /* piano key number */
    double omega = m_2pi_fm * pow (K_FREQ_INC, n.tone_id) * DO0;


    /* Gather space for new samples */
    *data_origin = (BYTE *) realloc (*data_origin, *p_insp + duration);
    for (int i=0; i<duration; i++, (*p_insp)++)
        *(*data_origin + *p_insp) = VOL * cos (omega * i);

}

struct TWav *render_score (const struct TScore *score, unsigned freq /* sampling freq*/) {
    struct TWav *wav = (struct TWav *) malloc (sizeof (struct TWav));
    wav->data = NULL;
    QWORD insp    = 0; /* Insertion point */
    wav->header = create_header (0);

    set_freq (&freq);
    unsigned len = score->notes.summit;
    unsigned bpm = score->bpm;

    for (unsigned i=0; i<len; i++) {
        struct TNote n = score->notes.data[i];
        append_samples(n, bpm, freq, &wav->data, &insp);
    }

    wav->header.nBlockAlign = insp;

    return wav;
}

void write (const char *filename, struct TWav *sc, unsigned freq) {
}

void play  (const struct TWav *sc, unsigned freq) {
    set_freq (&freq);
    const char *command_tmpl = "aplay -t raw -r %u";
    char command[0x200];
    sprintf (command, command_tmpl, freq);

    FILE *audiostream = popen (command, "w");

    if (!audiostream) {
        fprintf (stderr, "Could not open aplay audio stream to write.\n");
        return;
    }

    //fwrite (&sc->header, sizeof(WAVEFORMAT), 1, audiostream );
    fwrite (sc->data, sc->header.nBlockAlign, 1, audiostream );

    if ( pclose(audiostream) != 0)
        fprintf (stderr, "Error shuting down aplay.\n");
}
