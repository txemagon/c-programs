#ifndef __NOTES_H__
#define __NOTES_H__

/*Note name length*/
#define NL 3U

#include <scales.h>

extern const char * const note_names[];

struct TDuration {
    unsigned numerator;   // Number of bar fractions
    unsigned denominator; // Bar fraction unit.
};

struct TNote {
    struct TDuration duration;
    enum TNoteName name_index;  // European note name.
    unsigned octave;      // Note octave.
    char read_name[NL];   // Name read from score.
    unsigned tone_id;     // Number of piano key associated with this tone.
};

#ifdef __cplusplus
extern "C"
{
#endif
    struct TNote create_note (
            unsigned duration,
            const char note_name[NL],
            unsigned octave,
            struct TScale sc);

    const char *note_name (enum TNoteName index, struct TScale sc);
    const char *note_fig  (unsigned fraction);

#ifdef __cplusplus
}
#endif

#endif
