#ifndef __STACK_H__
#define __STACK_H__

#include "notes.h"

#define MAX_STACK_SIZE 20000
#define STACK_BLK_SIZE 5

struct TStack {
    struct TNote *data;
    unsigned summit;
    unsigned size;
};

#ifdef __cplusplus
extern "C"
{
#endif

    struct TStack create ();
    void destroy (struct TStack *s);
    void push (struct TStack *s, struct TNote data);
    struct TNote pop (struct TStack *s);

#ifdef __cplusplus
}
#endif

#endif
