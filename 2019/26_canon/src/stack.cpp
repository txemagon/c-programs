#include "stack.h"

#include <stdio.h>
#include <stdlib.h>

void allocate (struct TStack *s) {
    if (s->size + STACK_BLK_SIZE > MAX_STACK_SIZE) {
        fprintf (stderr, "FUll stack.\n");
        exit (1);
    }
    s->data = (struct TNote *) realloc (s->data,
            (s->size + STACK_BLK_SIZE) * sizeof (struct TNote));
    s->size += STACK_BLK_SIZE;
}

struct TStack create () {
    struct TStack n;

    n.summit = 0;
    n.size = 0;
    n.data = NULL;
    allocate (&n);
    return n;
}

void destroy (struct TStack *s) {
    free (s->data);
}

void push (struct TStack *s, struct TNote data) {
    while (s->summit >= s->size)
        allocate (s);

    s->data[s->summit++] = data;
}

struct TNote pop (struct TStack *s) {
    struct TNote r;

    if (s->summit > 0)
        r = s->data[--s->summit];
    else
        fprintf (stderr, "Trying to pop notes from an empty stack.\n");

    return r;
}
