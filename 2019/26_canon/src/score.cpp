#include "score.h"
#include "notes.h"

#include <stdio.h>
#include <stdlib.h>

void print_sc (const struct TScore *sc) {
    struct TNote n;
    int lf = 0;           // Line feed flag

    system ("clear");
    printf ("\n");

    printf ("BPM: %u\n", sc->bpm);
    printf ("Scl: %s\n", get_scname (&sc->scale));
    printf ("\n");

    for (unsigned i=0; i<sc->notes.summit; i++, lf++) {
        if (lf > NPL){
            printf ("\n");
            lf = 0;
        }
        n = sc->notes.data[i];
        printf ("  ");

#if NODOTS != 0
        const char *dot = "";
        int duration = n.duration.denominator;
        if (n.duration.numerator == 3) {
            duration /= 2;
            dot = "·";
        }
        printf ("%s%s%s%u ", note_fig(duration), dot,
                note_name(n.name_index, sc->scale), n.octave);
#else
        for (unsigned nd=0; nd<n.duration.numerator; nd++)
            printf ("%s", note_fig (n.duration.denominator));
        printf ("%s%u ", note_name(n.name_index), n.octave);
#endif
        // Not printing piano key number.
        // printf ("(%i)   ", n.tone_id);
    }

    printf ("\n");
    printf ("\n");
}


struct TScore *create_score () {
    struct TScore *s = (struct TScore *) malloc (sizeof (struct TScore));
    s->bpm    = BPM;
    s->scale  = create_scale();

    return s;
}


void destroy_score (struct TScore *sc) {
    free (sc);
}
