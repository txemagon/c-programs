#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 6
#define M 20

void muestra (int a[N]) {
    for (int i=0; i<N; i++)
        printf ("%i ", a[i]);
    printf ("\n");
}

int main (int argc, char *argv[]) {
    int A[N];
    int menor;

    /* Inicialización */
    srand (time (NULL));
    for (int i=0; i<N; i++)
        A[i] = rand () % M + 1;   // Números entre 1 y 20

    muestra (A);
    /* Ordenación de inserción */

    for (int buscando=0; buscando<N-1; buscando++) {
        menor = buscando;
        for (int i=buscando+1; i<N; i++)
            if (A[i] < A[menor])
                menor = i;
        if (menor > buscando) {
            int aux = A[buscando];
            A[buscando] = A[menor];
            A[menor] = aux;
        }
    }
    /* Fin de la ordenación */
    muestra (A);


    return EXIT_SUCCESS;
}
