#include <stdio.h>
#include <stdlib.h>

#define N 6

void muestra (int a[N]) {
    for (int i=0; i<N; i++)
        printf ("%i ", a[i]);
    printf ("\n");
}

int main (int argc, char *argv[]) {
    int A[N] = {2, 5, 3, 7, 4, 1};

    muestra (A);
    /* Ordenación de la burbuja (bubble sort) */
    for (int p=0; p<N-1; p++)
        for (int i=0; i<N-1; i++)
            if (A[i] < A[i+1]) {
                int aux = A[i+1];
                A[i+1] = A[i];
                A[i] = aux;
            }
    /* Fin de la ordenación */
    muestra (A);


    return EXIT_SUCCESS;
}
