#include <stdio.h>
#include <stdlib.h>

#define N 4

int main (int argc, char *argv[]) {
    system ("clear");
    system ("toilet -fpagga --gay PERFECTOS");
    printf ("\n\n");

    for (unsigned cuantos=0, perfecto=2; cuantos<N; perfecto++) {
        unsigned suma = 0;
        for (int div=perfecto/2; div>0 && suma<=perfecto; div--)
            if (perfecto % div == 0)
                suma += div;
        if (perfecto == suma) {
            cuantos++;
            printf (" %u", perfecto);
            fflush (stdout);          /* Igual tardamos un poco en llenar el buffer. */
        }
    }

    printf ("\n\n");

    return EXIT_SUCCESS;
}
