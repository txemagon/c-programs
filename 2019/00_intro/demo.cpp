#include <math.h>
#include "fbd.h"

#define R 100
#define XC 500
#define YC 370

void circle () {
    for (double a=0; a<2*M_PI; a +=.001)
    put (XC + R*cos(a), YC - R*sin(a), 0xFF, 0xFF, 0xFF, 0xFF);
}

int main () {

    open_fb ();
    /* Hacer lo que me dé la gana */
    put ( 500, 350, 0xFF, 0xFF, 0xFF, 0xFF );
    circle ();

    /* Fin de hacer lo que me dé la gana */
    close_fb ();

    return 0;
}
