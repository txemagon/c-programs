# Ejercicios de Programación Orientada a Objetos

## Declaraciones en C++

1. Haz un programa que use la clase `CPersona` con el método público `saluda`. También
tendrá el atributo privado nombre. El programa generará 10 personas que irá guardando en
un Array y hará saludar a las 10 personas. Cada persona tendrá un nombre que se tomará
de un catálogo de nombres:

```c
const char * nombre[] = {
    "Pepemari",
    ....,
    NULL
};
```

Utiliza los inicializadores para rellenar los atributos. El valor por defecto cuando no
se provee apellido será Expósito.

## Herencia

1. Crea un programa que contenga `CHumano` y '`CCaballo`. Apunta en sus atributos características
del tipo: número de extremidades, etc. Dótales de métodos del tipo anda, galopa, relincha.

## Clases no Instanciable.

1. Asegúrate de que no se pueden generar mamíferos poniendo el constructor como `protected`. Haz que todos los mamíferos anden, aunque todos de distinta manera.

## Sobrecarga

1. Permite que halla caballos tipo Pony usando enum class y sobrecargando el constructor de CCaballo.
1. Crea un método mover sin parámetros. Sobrecarga un método mover para los mamíferos que acepte las coordenadas `x` e `y` para un supuesto destino.


## Accedentes y mutadores

1. Crea atributos virtuales sólo lectura para obtener la temperatura del animal en distintas unidades de medida. Implementa los métodos en mamífero pero asegúrate que se pueda acceder a ellos desde cualquier sitio.

## Destructores

1. Crea la clase `CEnterrador` que provea el método entierra. Cuando crees los mamíferos pásale una referencia a su enterrador.
En sus últimos estertores (destructor) haz que llamen al método `entierra`.
Dicho método estará sobrecargado y aceptará `CPersona` o `CCaballo`.
El enterrador pondrá en pantalla si ha enterrado a una persona o a un caballo.
