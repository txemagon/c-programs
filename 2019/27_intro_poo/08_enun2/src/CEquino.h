
#ifndef CEQUINO_H
#define CEQUINO_H

#include "CMamifero.h"

class CEquino : public CMamifero
{
public:
  bool is_herrado ();

  void set_herradura (bool herrado);
  void galopa ();
  void anda () override;

private:
  bool herrado_;

};
#endif // CEQUINO_H
