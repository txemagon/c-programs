#include "CEquino.h"

#include <iostream>

using namespace std;

bool
CEquino::is_herrado ()
{
  return herrado_;
}

void
CEquino::set_herradura (bool herrado)
{
  this->herrado_ = herrado;
}

void
CEquino::galopa ()
{
  cout << "Galopa al viento.\n";
}

void
CEquino::anda ()
{
  cout << "Ando, pero como un caballo.\n";
}
