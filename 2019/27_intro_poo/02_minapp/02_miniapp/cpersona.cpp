#include "cpersona.h"

#include <string.h>
#include <stdio.h>

CPersona::CPersona (unsigned edad, const char *nombre)
{
    this->edad = edad;
    strcpy (this->nombre, nombre);
}


void CPersona::display () {
    printf ("Nombre: %s\n", this->nombre);
    printf ("Edad:   %u\n", this->edad);
}
