#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    unsigned tope;

    printf ("Numero: ");
    scanf (" %u", &tope);

    for (unsigned numero=tope; numero>0; numero--) {
        printf (" Divisores de %u: ", numero);
        for (unsigned d=numero/2; d>0; d--)
            if (! (numero % d))
                printf ("%u ", d);
        printf ("\n");
    }
    printf ("\n");

    return EXIT_SUCCESS;
}
