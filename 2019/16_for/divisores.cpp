#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    unsigned numero;

    printf ("Numero: ");
    scanf (" %u", &numero);

    for (unsigned d=numero/2; d>0; d--)
        if (! (numero % d))
            printf ("%u ", d);

    printf ("\n");

    return EXIT_SUCCESS;
}
