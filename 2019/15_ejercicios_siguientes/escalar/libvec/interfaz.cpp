#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "interfaz.h"

char prog_title[MAXLIN] = "Vector";

const char *marca_grado[] = {"º", ""};

const char *const bases[] = {
    "cartesianas",
    "cilíndricas",
    "esféricas",
    NULL
};

const char * const componentes [][DIM] = {
    {"X", "Y", "Z"},
    {"Ro", "Theta", "Z"},
    {"R", "Theta", "Phi"}
};


/* Funciones internas */
bool hay_que_poner_grado (enum TBase b, int comp) {
    if (b == cart )
        return false;
    if (comp == 0)
        return false;
    if ( b == cil && comp == 2 )
        return false;


    return true;
}

const char *imprimir_grado (enum TBase b, int comp) {/* Devuelve un puntero a º cuando haya que imprimirlo*/
    return hay_que_poner_grado (b, comp) ? marca_grado[0] : marca_grado[1];
}

void print_vector (
        const char *label, double v[DIM],
        enum TBase base ) {
    printf  ( "\n" );
    printf  ( "\n" );
    printf ("Vector %s [%s]:\t", label, bases[base]);

    printf ("(");
    for (int i=0; i<DIM; i++){
        double valor = hay_que_poner_grado (base, i) ? rad2deg (v[i]) : v[i];
        printf (" %.2lf%s", valor, imprimir_grado (base, i));
    }
    printf (")");
    printf  ( "\n" );
}



/* Funciones públicas (API) */
void title () {
    char command[0x80] = "toilet -fpagga --gay ";
    strcat (command, prog_title);
    system ("clear");
    system (command);
    printf ("\n\n");
}

void print_options (const char * const estadio) {
    const char **base = (const char **) bases;

    title ();

    printf ("Elige la base %s:\n", estadio);
    printf ("\n");
    printf ("\n");

    for (int i=0; *base!=NULL; base++, i++)
        printf ("\t%i.- %s\n", i+1, *base);

    printf ("\n");
    printf ("\n");
}

enum TBase ask_option () {
    unsigned eleccion;

    do {
        printf ("                                                       \r");
        printf ("    Opción: ");
        scanf (" %u", &eleccion);
        printf ("\x1B[1A");
    } while (eleccion <= 0 || eleccion > BASES);

    printf ("\n");
    eleccion--;

    return (enum TBase) eleccion;
}

void convert (double dst_vec[DIM], double src_vec[DIM], enum TBase dstbas, enum TBase srcbas) {

    /* Salida Sin Cálculos */
    if (srcbas == dstbas) {
        memcpy(dst_vec, src_vec, DIM * sizeof (double));
        return;
    }

    /* Cálculos */
    switch (srcbas) {
        case cart:
            switch (dstbas) {
                case cil:
                    car2cil (dst_vec, src_vec);
                    break;
                case esf:
                    car2esf (dst_vec, src_vec);
                    break;
                default:
                    fprintf (stderr, "Base de salida desconocida.\n");
                    exit(EXIT_FAILURE);
            }

            break;
        case cil:
            switch (dstbas) {
                case cart:
                    cil2car (dst_vec, src_vec);
                    break;
                case esf:
                    cil2esf (dst_vec, src_vec);
                    break;
                default:
                    fprintf (stderr, "Base de salida desconocida.\n");
                    exit (EXIT_FAILURE);
            }
            break;
        case esf:
            switch (dstbas) {
                case cart:
                    esf2car (dst_vec, src_vec);
                    break;
                case cil:
                    esf2cil (dst_vec, src_vec);
                    break;
                default:
                    fprintf (stderr, "Base de salida desconocida.\n");
                    exit (EXIT_FAILURE);
            }
            break;
        default:
            fprintf (stderr, "Base de entrada desconocida.\n");
            exit (EXIT_FAILURE);
    }


}

void ask_vector (double v[DIM]) {
    enum TBase base;
    double vector[DIM];
    print_options ("");
    base = ask_option ();
    printf ("Componente: ");
    printf ("\n");
    printf ("\n");

    for (int c=0; c<DIM; c++) {
        printf (" %s: ", componentes[base][c]);
        scanf (" %lf", &vector[c]);
    }

    /* Almacenamos internamente los datos en radioanes  */
    if (base == cil || base == esf)
        vector[1] = deg2rad (vector[1]);

    if (base == esf)
        vector[2] = deg2rad (vector[2]);


    convert (v, vector, cart, base);
}

