# Ejercicios sobre Punteros

Se trata de ir haciendo ejercicios de cada categoría.


## Punteros y Arrays

1. Dado un array de enteros relleno (me da igual como aparezca este array), se trata de
imprimirlo de manera ordenada sin ordenarlo. Para ello se generará un segundo array de
punteros a entero que ha de terminar apuntando a todos los números del primer array en
orden creciente. https://youtu.be/1h4aY5bbb4I
1. Dada una lista de palabras, ordernarla usando strcmp. https://youtu.be/8gLS12N7-iw


## Recursividad

1. Confecciona un programa que sume los n primeros números naturales.
1. Partiendo del programa anterior confecciona un programa que haga el factorial de un número.
1. Confecciona un programa que imprima todos los números naturales desde _n_ hasta 1 (ó 0 me es indiferente)
en orden decreciente. Recuerda que tienes que imprimir _n_ y luego imprimir los _n-1_ anteriores.
1. Rehaz el programa anterior para que los imprime en orden creciente. Recuerda que tienes que imprimir
los _n-1_ anteriores y luego imprimir _n_.
1. Imprime un array de caracteres (`char frase[]`) o cadena (`const char *`) de carácter en carácter.
1. Imprime un array de caracteres (`char frase[]`) o cadena (`const char *`) de carácter en carácter en orden inverso.
Una buena frase sería: "dabale arroz a la zorra el abad" porque es un palíndromo.
1. Imprime los divisores de _n_.
1. Haz una función no recursiva `bool es_primo(int n)` que apoyándose en la función recursiva
`int numero_de_divisores (int n)` diga si un número es primo o no.
1. Haz una función que calcule el número `e` con `n` términos.

```math
e(0) = 1 / 0! = 1  \\
e(1) = 1 / 1! + 1 / 0! = 1 / 1! + e(0) = 2  \\
e(2) = 1 / 2! + 1 / 1! + 1 / 0! = 1 / 2! + e(1) = 2.5 \\

```

```math
e(n) = \sum_{0}^{n} \frac{1}{x!}
```

1. Calcula la función e de manera recursiva hasta que la diferencia entre dos pasos sea menor que una constante
definida por ti, `E`.

```math
e(n) - e(n-1) = 1 / n !
```

Haz la prueba de la ecuación anterior fijándote en lo que vale `e(2)` y `e(1)` antes de operar, cuando todavía están las fracciones.

1. Haz una función recursiva que calcule:

```math
s(n) = 1 - x + \frac{x^2}{2} - \frac{x^3}{6} + \frac{x^4}{24} + \dots + (-1)^n \frac{x^n}{n!}
``` 

1. Dado un programa que ya ha preguntado un polinomio y que cuenta con la función `double f (double x)` pregunte
dos valores de `x`: el límite inferior y el límite superior y calcule la raíz de la función con la función recursiva

```c
void cero (double li, double ls);
```

que termine sus llamadas cuando la distancia entre `li` y `ls` sea menor que una constante `E`. La función se llama 
recursivamente a sí misma cambiando el valor de `li` o el de `ls` por el del valor medio entre ambos. La última llamada
imprime el punto medio entre ambos.

1. Haz un programa que pregunte un número al usuario e imprima el ordinal correspondiente usando una función recursiva.

Para ello usa este array:

```c
const char *ordinal[3][10] = {
    { "", "primero", "segundo", "tercero", "cuarto", "quinto", "sexto", "séptimo", "octavo", "noveno" },
    { "", "décimo", "vigésimo", "trigésimo", "cuatrigésimo", "quincuagésimo", "sexagésimo", "septuagésimo", "octogésimo", "nonagésimo" },
    { "", "centésimo", "bicentésimo", "tricentésimo", "cuadrigentésimo", "quingentésimo", "sexgentésino", "septingentésimo", "octingentésimo", "noningentésimo" }
};
```

1. Traca Final. Torres de Hanoi. Dados tres arrays de enteros (o un array con tres filas) y tres punteros a los mismos:

```c
#define N 5

enum TPalo {origen, intermedio, destino};

int palo[3][N];
int cima[3]; // Indica la primera posición libre de cada palo.
int *origen, *intermedio, *destino;
```

Rellena el `palo[origen]` con los números del 5 al 1 que representan los diámetros de unos discos insertos en el palo. Se trata de pasar todos los discos del palo origen al palo destino, pero no se puede apoyar un disco mayor sobre uno más pequeño. La función recursiva 

```c
void pasar_n_discos (int *origen, int *intermedio, int *destino);
```

tiene que pasar los `n-1` primeros discos al palo intermedio (llamada recursiva cambiando los parámetros). Luego el último disco al palo destino (Operación sobre los arrays)  y, finalmente los `n-1` discos del palo intermedio al palo destino (llamada recursiva).

Cada vez que se mueva un disco (operación sobre los arrays) imprime el contenido de los tres palos.

## Punteros a Funciones
1. Dadas dos medidas que pueden ser base y altura de un triángulo o de un rectángulo y, teniendo dos funciones que calculan áreas: `double area_triangulo(double, double)` y `double area_rectangulo(double, double)`. Imprime el área de lo que quiera el usuario usando un puntero que a veces apunte a `area_triangulo` y a veces a `area_rectangulo`. No uses switch. https://youtu.be/yldZiqQKHRk

1. Teniendo varias funciones matemáticas, pregúntale al usuario qué función quiere usar qué `x`, y calcula el valor de la función en esa `x`,
1. Teniendo varias funciones matemáticas, calcula el área (integral) entre un limite inferior y uno superior de la función que elija el usuario.

