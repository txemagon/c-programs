#include <stdio.h>
#include <stdlib.h>

void imprime (int n) {

    /* Condición de contorno */
    if (n<0)
        return;

    imprime (n-1);
    printf ("%i ", n);
}

int main (int argc, char *argv[]) {

    const int n = 9;

    imprime (n);

    printf ("\n");

    return EXIT_SUCCESS;
}
