#include <stdio.h>
#include <stdlib.h>

int factorial (int n){
    if (n <= 0)
        return 1;
    return n * factorial (n-1);

}

int main (int argc, char *argv[]) {

    const int n = 6;

    printf ("factorial (%i) = %i\n", n, factorial (n));

    return EXIT_SUCCESS;
}
