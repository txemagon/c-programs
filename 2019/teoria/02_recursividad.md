# Recursividad

La recursividad consiste en hacer que una función se llame a sí misma. Para evitar entrar en un bucle infinito,
   debemos encontrar una condición de parada.

Para diseñar un algoritmo recursivo, entonces, tenemos que:

1. Definir el problema en función de sí mismo.
1. Encontrar una condición de parada.

La recursividad es la manera más natural de pensar para nuestro cerebro y, aunque en algunas empresas está
prohíbida por su supuesta complejidad, lo único que indica es una baja formación de sus empleados.

La velocidad de los algoritmos recursivos es bárbara, pero su inconveniente es que consumen mucha memoria.
Acuérdate de que por cada llamada  que se hace a una función se empujan en la pila los parámetros se empujan la
dirección de retorno y BP y se crea una nueva pila con sus variables locales para la nueva llamada.

Las únicas variables que pueden compartir las llamadas recursivas son las variables static, precisamente porque no se hayan en la pila.

Veamos un ejemplo, imaginemos que queremos saber la suma de los $`n`$ primeros números naturales. Para distintos valores tendremos:

```math
s(1) = 1 \\
s(2) = 2 + 1 \\
s(3) = 3 + 2 + 1 \\
s(4) = 4 + 3 + 2 + 1 \\
s(5) = 5 + 4 + 3 + 2 + 1 \\
s(6) = 6 + 5 + 4 + 3 + 2 + 1
```

Así que podemos definir el problema en función de sí mismo:

```math
s(6) = 6 + s(5)
```
Y parametrizando,

```math
s(n) = n + s(n-1)
```
¿ Cuándo ha de parar esto ?

Pues por ejemplo cuando $`n = 0, s(0) = 0`$.

