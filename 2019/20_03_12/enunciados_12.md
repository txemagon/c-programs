# Enunciados 12/03

Los enunciados de hoy orbitan en torno a la multiplicación de matrices.  
En esta página hay un dibujo de la operación: http://www.texample.net/tikz/examples/matrix-multiplication/  
También puedes consultar el documento de ayer sobre notación vectorial.  

1. Usando dos matrices estáticas de 3x3 hacer un programa que calcula la matriz resultante de su multiplicación, también de 3x3.
1. Usando dos matrices estáticas, una de 2x4 y otra de 4x3, calcular el producto de las mismas de dimensión 2x3.
1. Generalizando el problema anterior, una matriz será de mxk y otra de kxn, siendo la resultante de dimensión mxn. Has de preguntarle al usuario los valores de m,k y n. Como los datos no son conocidos en tiempo de compilación necesitas usar `malloc`.
