#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#include "ansi.h"


void title () {
    system ("clear");
    system ("toilet -fpagga --gay MATRICES");
    printf ("\n");
    printf ("Multiplicación de Matrices.\n");
    printf ("\n\n");
}

void pedir_datos (double *m, const char *label, int filas, int cols) {
    title ();
    printf ("Introduce los datos de la Matriz %s\n", label);
    printf ("\n");

    for (int f=0; f<filas; f++) {
        for (int c=0; c<cols; c++) {
            printf ("\t\t\t");
            for (int tab=0; tab<c; tab++)
                printf ("\t");
            scanf ("%lf", m + f * cols + c);  /* Aquí es imposible usar la notación de Arrays &m[f][c]. Al no saber el ancho le es imposible calcular el stride. */
            GO_UP(1);
        }
        printf ("\n");
    }
}

void mostrar (double *m, const char *label, int filas, int cols) {

    printf ("Matriz %s:\n", label);
    printf ("\n");
    for (int f=0; f<filas; f++) {
        printf ("\t\t");
        for (int c=0; c<cols; c++)
            printf ("%9.2lf", *(m + f * cols + c)); /* Idem con la notación de Arrays*/
        printf ("\n");
    }
}

int main (int argc, char *argv[]) {

    double *A, *B, *C;
    int M, K, N;   // Mantengo las mayúsculas por analogía con v2-m3.cpp

    /* Parte nueva */
    title ();
    printf ("Filas de A: ");
    scanf ("%u", &M);
    printf ("Columnas de A y Filas de B: ");
    scanf ("%u", &K);
    printf ("Filas de B: ");
    scanf ("%u", &N);


    /* Creamos las celdas de las matrices en el HEAP */
    A = (double *) malloc ( M * K * sizeof (double) );
    B = (double *) malloc ( K * N * sizeof (double) );
    C = (double *) malloc ( M * N * sizeof (double) );
    /* Fin de lo nuevo */

    bzero (C, M * N * sizeof(C));  /* Poner todo a 0 */
    pedir_datos ((double *) A, "A", M, K);   // Dos maneras
    //pedir_datos (&B[0][0], "B", K, N);       // de hacer el casting
    pedir_datos ((double *) B, "B", K, N);

    /* Para calcular la fila i y j de c, sabemos que cij = aik·bkj contrayendo k en una acumulación */
    for (int i=0; i<M; i++)
        for (int j=0; j<N; j++)
            for (int k=0; k<K; k++)
                // C[i][j] += A[i][k] * B[k][j];  /* No puede calcular el stride */
                *(C + i * N + j) += *(A + i * K + k) * *(B + k * N + j);

    title ();
    mostrar (A, "A", M, K);  // Tb se puede usar el operador &
    mostrar (B, "B", K, N);
    mostrar (C, "C", M, N);
    printf ("\n\n");

    /* Nos acordamos de lberar el HEAP */
    free (A);
    free (B);
    free (C);

    return EXIT_SUCCESS;
}
