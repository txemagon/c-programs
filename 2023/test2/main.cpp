#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

#include <iostream>

int main() {
    std::cout << "Hello, World!" << std::endl;
    return 0;
}
