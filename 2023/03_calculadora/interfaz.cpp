#include "interfaz.h"

#include <stdio.h>
#include <stdlib.h>

#define TITULO_MENU "  \
        MENU         \n\
        =====        \n\
                     \n\
"

const char *opciones[] = {
    "Sumar",
    "Restar",
    "Multiplicar",
    "Dividir",
    "Resto",
    (const char *) 0
};



void
titulo() {
    system ("clear");
    system ("toilet -fpagga --gay 'CALCULADORA'");
}

unsigned
menu () {
    unsigned opcion;

    printf ( "%s", TITULO_MENU );

    for( unsigned i=0; opciones[i] != NULL; i++)
        printf("\t\t%u.- %s\n", i+1, opciones[i]);

    printf("\n\n\tTu Opción: ");
    scanf(" %u", &opcion);

    return opcion - 1;
}
