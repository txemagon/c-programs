#include <stdio.h>
#include <stdlib.h>

#include "interfaz.h"

void operando(const char *enun, double *op) {
    printf ("%s: ", enun);
    scanf(" %lf", op);
}

double suma(double op1, double op2)       { return op1 + op2; }
double resta(double op1, double op2)      { return op1 - op2; }
double multiplica(double op1, double op2) { return op1 * op2; }
double divide(double op1, double op2)     { return op1 / op2; }

int main(int argc, char *argv[]) {
    unsigned opcion;
    double op1, op2, res;
    char signo;

    titulo();
    opcion = menu();

    operando("Operando 1", &op1);
    operando("Operando 2", &op2);

    switch (opcion){
        case SUMA:
            signo = '+';
            res = suma(op1, op2);
            break;
        case SUBS:
            signo = '-';
            res = resta(op1, op2);
            break;
        case MULT:
            signo = 'x';
            res = multiplica(op1, op2);
            break;
        case DIVI:
            signo = '/';
            res = divide(op1, op2);
            break;
        default:
            fprintf( stderr, "No sé que coño hacer con tu opción.\n" );
            return EXIT_FAILURE;
            break;
        }

    printf ("%.2lf %c %.2lf = %.2lf\n", op1, signo, op2, res);

    return EXIT_SUCCESS;
}

