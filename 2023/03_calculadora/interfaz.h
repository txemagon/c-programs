#ifndef __INTERFAZ_H__
#define __INTERFAZ_H__

#define SUMA 0
#define SUBS 1
#define MULT 2
#define DIVI 3
#define REST 4    /* Resto */

#ifdef __cplusplus
extern "C"
{
#endif

    void titulo();
    unsigned menu();

#ifdef __cplusplus
}
#endif


#endif
