#include <stdio.h>
#include <stdlib.h>

#define MX  0x10

int main(int argc, char *argv[]) {
    char dato[MX];

    printf("Hex: ");
    scanf(" %3[0-9a-fA-F]", dato);
    printf("Hex = %s\n", dato);

    printf("%3i\n", 73);
    printf("%4i\n", 73);
    printf("%5i\n", 73);

    printf("%03i\n", 73);
    printf("%04i\n", 73);
    printf("%05i\n", 73);

    return EXIT_SUCCESS;
}

