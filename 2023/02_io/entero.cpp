#include <stdio.h>
#include <stdlib.h>

#define MAX 10

int main(int argc, char *argv[]) {
    int lista[MAX];

    /* Entrada de Datos */
    for (int i=0; i<MAX; ){
        int num;
        printf ("Dato %02i: ", i+1);
        scanf (" %i", &num);

        int repetido = 0;
        // Sólo si no está en la lista
        for (int j=0; j<i; j++)
            if (lista[j] == num)
                repetido = 1;

        //    lo incluyo
        if (!repetido){
            lista[i] = num;
            i++;
        }

    }

    /* Salida de Datos */
    printf ("[ ");
    for (int i=0; i<MAX; i++)
        printf ("%04i ", lista[i]);
    printf ("]\n");

    return EXIT_SUCCESS;
}

