#include <stdio.h>
#include <stdlib.h>

#define MAX 0x80

int main(int argc, char *argv[]) {
    char nombre[MAX];

    printf("Nombre: ");
    scanf(" %[^\n]", nombre);
    printf("Hola %s\n", nombre);

    return EXIT_SUCCESS;
}

