#include <stdio.h>
#include <stdlib.h>

#define MAX   0x100

#define PREGUNTA(ntipo,sform,var)                     \
   printf("Mete un %s: ", #ntipo);                     \
   scanf (sform, &var);                               \
   printf("El %s contiene: %i\n", #ntipo, (int) var);


// -----------------------
// Cargar cosas en memoria
// -----------------------

int main(int argc, char *argv[]) {

    char bytes[MAX];
    char un_byte;

    int entero;
    unsigned sinsigno;
    long int largo;
    unsigned long int ess;  // Entero Sin Signo

    int aent[MAX];

    double real;


    PREGUNTA(carácter, " %c", un_byte)
    PREGUNTA(entero, " %i", entero)

    scanf ("%u", &sinsigno)
    scanf ("%li", &largo)
    scanf ("%lu", &ess)

    scanf(" %i", &aent[0])
    scanf(" %i",aent)
    scanf(" %*i %lf", &real);

    scanf(" %s", bytes);


    return EXIT_SUCCESS;
}

