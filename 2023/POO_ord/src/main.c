#include "Mamifero.h"
#include "Humano.h"
#include "Perro.h"

#define N 2

int
main()
{
	Mamifero *pm[N];

	pm[0] = new Humano();
	pm[1] = new Perro();

	for (int i=0; i<N; i++)
		pm[i]->saluda();
}