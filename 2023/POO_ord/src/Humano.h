
#ifndef HUMANO_H
#define HUMANO_H

#include <cstdio>
#include <iostream>
#include "Mamifero.h"

class Humano : virtual public Mamifero
{
public:
    /**
     */
    void saluda() const override
    {
        std::cout << "Hola" << std::endl;
    }


};

#endif // HUMANO_H
