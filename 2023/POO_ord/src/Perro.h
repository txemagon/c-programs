
#ifndef PERRO_H
#define PERRO_H

#include <cstdio>
#include <iostream>
#include "Mamifero.h"

class Perro : virtual public Mamifero
{
public:
    /**
     */
    void saluda() const override
    {
        std::cout << "Toma la pata" << std::endl;
    }


};

#endif // PERRO_H
