#include <stdio.h>
#include <stdlib.h>

#define N 15

int main(int argc, char *argv[]) {
    unsigned long f[N];

    f[1] = f[0] = 1;

    /* Cálculos */
    for (int i=2; i<N; i++)
        f[i] = f[i-1] + f[i-2];

    /* Salida */
    for (int i=0; i<N; i++)
        printf("%lu ", f[i]);

    printf("\n");


    return EXIT_SUCCESS;
}

