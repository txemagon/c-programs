#include <stdio.h>
#include <stdlib.h>


int main(int argc, char *argv[]) {

    const char  *n1 = "George",
                *n2 = "Geoage",
                *p1 = n1,
                *p2 = n2;

    int bandera_iguales = 1;

    for (; *p1 != '\0'; p1++, p2++)
        if (*p1 != *p2)
            bandera_iguales = 0;

    if (bandera_iguales)
        printf("Son iguales\n");
    else
        printf("No son iguales\n");

    return EXIT_SUCCESS;
}

