#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#define N   5

int main(int argc, char *argv[]) {
    unsigned A[N][N];
    bzero(A, sizeof(A)); // memset(A, 0, sizeof(A));

    // Inicializar
    A[0][0] = 1;
    for (int i=1; i<N; i++)
        for (int j=0; j<N; j++)
            A[i][j] = (j == 0) ? 1 : A[i-1][j] + A[i-1][j-1];

    // Imprimir
    for (int i=0; i<N; i++){
        for (int j=0; j<N; j++)
            printf ("%u ", A[i][j]);
        printf("\n");
    }

    return EXIT_SUCCESS;
}

