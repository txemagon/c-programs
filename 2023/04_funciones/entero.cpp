#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>

#define MAX 10


bool esta_repetido(int, int [MAX], int);

int main(int argc, char *argv[]) {
    int lista[MAX];

    /* Entrada de Datos */
    for (int i=0; i<MAX; ){
        int num;  // buffer
        __fpurge(stdin);
        printf ("Dato %02i: ", i+1);
        scanf (" %i", &num);

        //    lo incluyo
        if (!esta_repetido(num, lista, i) ){
            lista[i] = num;
            i++;
        }

    }

    /* Salida de Datos */
    printf ("[ ");
    for (int i=0; i<MAX; i++)
        printf ("%04i ", lista[i]);
    printf ("]\n");

    return EXIT_SUCCESS;
}

bool esta_repetido(int num, int lista[MAX], int hasta) {
    bool repetido = false;
    // Sólo si no está en la lista
    for (int j=0; j<hasta; j++)
        if (lista[j] == num)
            repetido = true;

    return repetido;
}
