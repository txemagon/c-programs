#include <stdio.h>
#include <stdlib.h>

#define NUMLETRAS  ('z' - 'a' + 1)

int main(int argc, char *argv[]) {

    char cebadario[NUMLETRAS];

    /* Calcular */
    for (char i='z'; i>='a'; i--)
        cebadario['z' - i] = i;

    /* Imprimir */
    for (char i='a'; i<='z'; i++)
        printf("%c ", cebadario[i - 'a']);
    printf("\n");

    return EXIT_SUCCESS;
}

