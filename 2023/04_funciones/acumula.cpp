#include <stdio.h>
#include <stdlib.h>

void acumula(int *almacen, int op) {
    *almacen = *almacen + op;
}

int main(int argc, char *argv[]) {

    int store = 5;

    acumula(&store, 9);

    return EXIT_SUCCESS;
}

