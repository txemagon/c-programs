
#ifndef ILIENZO_H
#define ILIENZO_H


class ILienzo
{
public:
    /**
     * @param  pantalla
     */
    virtual void pintate(char * pantalla) const = 0;


};

#endif // ILIENZO_H
