
#ifndef CJUGADOR_H
#define CJUGADOR_H

#include "ILienzo.h"

#include cadena (string)
#include vector



/**
  * class CJugador
  * Representa un Jugador del 3 en Raya
  */

class CJugador : virtual public ILienzo
{
public:
    /**
     * Accedente del Nombre del Jugador
     * @return cadena (string)
     */
    cadena (string) nombre() const
    {
        return this->nombre_;
    }


    /**
     * Mutador de nombre_
     * @param  nuevo_nm Nuevo nombre para el jugador
     */
    void nombre(cadena (string) nuevo_nm)
    {
        this->nombre_ = nuevo_nm;
    }

private:
    // Private attributes
    //  

    cadena (string) nombre_;

};

#endif // CJUGADOR_H
