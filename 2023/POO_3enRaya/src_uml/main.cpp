#include <stdlib.h>

#include "CGestorPartidas.h"

int
main()
{
	const CJugador &ganador;
	CGestorPartidas torneo;		

	ganador = torneo.empezar();

	felicitar(ganador);

	return EXIT_SUCCESS;
}