
#ifndef CGESTORPARTIDAS_H
#define CGESTORPARTIDAS_H

#define NPLAYERS 2

class CGestorPartidas
{
public:
    const CJugador &empezar();

private:
    CJugador jugadores[NPLAYERS];

    void crear_jugadores();

};

#endif // CGESTORPARTIDAS_H
