
#ifndef CSETFICHAS_H
#define CSETFICHAS_H

#include "ILienzo.h"

#define NUM_FICHAS 3

class CSetFichas : virtual public ILienzo
{
private:
    char simbolo;
    SFicha ficha[NUM_FICHAS];

};

#endif // CSETFICHAS_H
