#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

/****************/
/*  ALGORITMO   */
/****************/


// ----------------------------------
// Devuelve el signo de op: -1, 0 , 1
// ----------------------------------
int
signo(int op){
    return (op > 0) - (op < 0);
}

TEST_SUITE("Signo") {
    TEST_CASE("OP > 0") {
        CHECK_EQ(1, signo(3));
        CHECK_EQ(0, signo(0));
        CHECK_EQ(-1, signo(-2));
    }
}


// -------------------------
// Multiplicación a la Russe
// -------------------------
int
multiplica (int op1, int op2) {
    int resultado = 0;
    int s1 = signo (op1),
        s2 = signo (op2);

    op1 = abs(op1);
    op2 = abs(op2);

    while (op2 > 0){
        if (op2 % 2)
            resultado += op1;
        op1 *= 2;
        op2 /= 2;
    }

    return s1 * s2 * resultado;
}


/*************/
/*   TESTS   */
/*************/
TEST_SUITE("Multiplicacion") {

  TEST_CASE("Criterio de signos") {
      CHECK_EQ(4, multiplica(2, 2));
      CHECK_EQ(-35, multiplica(-5, 7));
      CHECK_EQ(-35, multiplica(5, -7));
      CHECK_EQ(35, multiplica(-5,-7));
  }
}

